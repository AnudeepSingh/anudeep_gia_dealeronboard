package com.myh_genesis_spa.utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public abstract interface BlueLinkAppDevice {
	
	/**
	 * Only for Android to scroll to text matches exactly
	 * @param text
	 */
	public abstract void scrollToExact(String text);
	
	/**
	 * Only for Android to scroll to text partially matched
	 * @param text
	 */
	public abstract void scrollTo(String text);
	
	/**
	 * Only for Ios to scroll down the screen
	 */
	public abstract void scrollDown();
	
	public abstract void swipeScrollWheelUp(String scrollWheelXpath, int entriesToScroll);
	
	public abstract void swipeScrollWheelDown(String scrollWheelXpath, int entriesToScroll);
	
	public abstract AppiumDriver<MobileElement> getDriver();
	
	public abstract void sleepFor(long timeInMillis);
	
	public abstract void waitForElementToLoad(MobileElement element);
	
	public abstract void waitForElementToLoadWithProvidedTime(MobileElement element, int limit);
	
	public abstract boolean isElementDisplayed(MobileElement element);
	
	//public abstract void waitFluentlyForElementToLoad(String element, long waitTimeInSeconds, long pollingTimeInSeconds,String strLocater);
	
	public abstract void scrollByText(String text);
	
}
