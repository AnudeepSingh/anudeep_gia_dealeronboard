package com.myh_genesis_spa.utils;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.myh_genesis_spa.utils.LocateElements.locaterType;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class AndroidBlueLinkAppDevice implements BlueLinkAppDevice {

	private AndroidDriver<MobileElement> driver;

	public AndroidBlueLinkAppDevice(AndroidDriver<MobileElement> driver) {
		this.driver = driver; 
	}

	public void scrollToExact(String text) {
		// TODO Auto-generated method stub

	}

	public void scrollTo(String text) {
		// TODO Auto-generated method stub

	}

	public AppiumDriver<MobileElement> getDriver() {
		return driver;
	}

	public void sleepFor(long timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void waitForElementToLoad(MobileElement element) {
		boolean flag = false;

		for (int i = 0; i <= 10; i++) {
			if (!flag) {
				try {
					sleepFor(2000);
					String isVisible = element.getAttribute("visible");
					flag = isVisible.equalsIgnoreCase("true") ? true : false;
					flag = true;
				} catch (Exception e) {
					sleepFor(1000);
					flag = false;
				}
			} else {
				break;
			}

		}
	}

	public void waitForElementToLoadWithProvidedTime(MobileElement element, int limit) {
		boolean flag = false;

		for (int i = 0; i <= limit; i++) {
			if (!flag) {
				try {
					sleepFor(2000);
					String isVisible = element.getAttribute("visible");
					flag = isVisible.equalsIgnoreCase("true") ? true : false;
					flag = true;
				} catch (Exception e) {
					sleepFor(1000);
					flag = false;
				}
			} else {
				break;
			}

		}
	}

	public boolean isElementDisplayed(MobileElement element) {
		try {
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public void scrollDown() {
		// TODO Auto-generated method stub

	}

//	@Override
//	public void waitFluentlyForElementToLoad(String element, long waitTimeInSeconds, long pollingTimeInSeconds,
//			String strLocater) {
//		@SuppressWarnings("deprecation")
//		Wait<AppiumDriver<MobileElement>> wait = new FluentWait<AppiumDriver<MobileElement>>(driver)
//				.withTimeout(waitTimeInSeconds, TimeUnit.SECONDS).pollingEvery(pollingTimeInSeconds, TimeUnit.SECONDS)
//				.ignoring(NoSuchElementException.class).ignoring(TimeoutException.class);
//
//		LocateElements.locaterType lt = LocateElements.locaterType.valueOf(locaterType.class, strLocater.toUpperCase());
//		switch (lt) {
//
//		case XPATH:
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element)));
//
//			break;
//
//		case ID:
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element)));
//
//			break;
//
//		case TAGNAME:
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(element)));
//
//			break;
//
//		case CLASSNAME:
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(element)));
//
//			break;
//
//		case ACCESSIBILITY:
//
//			wait.until(ExpectedConditions.visibilityOf(driver.findElementByAccessibilityId(element)));
//
//			break;
//
//		}
//
//	}

	@Override
	public void swipeScrollWheelUp(String scrollWheelXpath, int entriesToScroll) {
		// TODO Auto-generated method stub

	}

	@Override
	public void swipeScrollWheelDown(String scrollWheelXpath, int entriesToScroll) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scrollByText(String text) {
        // scroll to item
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap scrollObject = new HashMap<>();
        scrollObject.put("predicateString", "name == '"+text+"'");
       //.put("direction", "down");
        js.executeScript("mobile: scroll", scrollObject);

    } 

}
