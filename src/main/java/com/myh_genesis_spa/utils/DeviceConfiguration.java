package com.myh_genesis_spa.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class DeviceConfiguration {
	
	private static DeviceConfiguration deviceConfiguration;
	private Properties deviceConfigProperties;
	
	public static DeviceConfiguration getInstance() throws IOException
	{
		if (deviceConfiguration == null) {
			deviceConfiguration = new DeviceConfiguration();
			
			deviceConfiguration.deviceConfigProperties = readDeviceConfigProperties();
			
		}
		return deviceConfiguration;
	}
	
	private static Properties readDeviceConfigProperties() throws IOException
	{
		InputStream inputStream = null;
		Properties prop = new Properties();
		try {
			String propFileName = "config.properties";
 
			inputStream = deviceConfiguration.getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
		}catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return prop;
	}
	
	public String getOSVersion() {
		if (getDevicePlatform().equals(DeviceType.Android)) {
			return deviceConfiguration.deviceConfigProperties.getProperty("ANDROID_DEVICE_OS_VERSION");
		}else{
			return deviceConfiguration.deviceConfigProperties.getProperty("IOS_DEVICE_OS_VERSION");
		}
	}
	
	public DeviceType getDevicePlatform()
	{
		String platformName = deviceConfiguration.deviceConfigProperties.getProperty("PLATFORM_NAME");
		if (platformName.equalsIgnoreCase("android")) {
			return DeviceType.Android;
		}else {
			return DeviceType.Ios;
		}
	}
	
	public URL getAppiumURL()
	{
		URL url = null;
		try {
			url = new URL(deviceConfiguration.deviceConfigProperties.getProperty("APPIUM_URL"));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return url;
	}
	
	public String getDeviceUDID()
	{
		if (getDevicePlatform().equals(DeviceType.Android)) {
			return deviceConfiguration.deviceConfigProperties.getProperty("ANDROID_DEVICE_UDID");
		}else{
			return deviceConfiguration.deviceConfigProperties.getProperty("IOS_DEVICE_UDID");
		}
	}
	
	public String getAndroidVirtualDevice()
	{
		return deviceConfiguration.deviceConfigProperties.getProperty("AVD_ID");
	}
	
	public String getAppPackage()
	{
		if (getDevicePlatform().equals(DeviceType.Android)) {
			return deviceConfiguration.deviceConfigProperties.getProperty("ANDROID_APP_PACKAGE");
		}else{
			return deviceConfiguration.deviceConfigProperties.getProperty("IOS_APP_PACKAGE");
		}
	}
	
	public String getAppPath()
	{
		if (getDevicePlatform().equals(DeviceType.Android)) {
			return deviceConfiguration.deviceConfigProperties.getProperty("ANDROID_APP_PATH");
		}else{
			return deviceConfiguration.deviceConfigProperties.getProperty("IOS_APP_PATH");
		}
	}
	
	public String getAppActivity()
	{
		if (getDevicePlatform().equals(DeviceType.Android)) {
			return deviceConfiguration.deviceConfigProperties.getProperty("ANDROID_APP_ACTIVITY");
		}
		return null;
	}
	
	public String getDeviceName()
	{
		if (getDevicePlatform().equals(DeviceType.Android)) {
			return deviceConfiguration.deviceConfigProperties.getProperty("ANDROID_DEVICE_NAME");
		}else{
			return deviceConfiguration.deviceConfigProperties.getProperty("IOS_DEVICE_NAME");
		}
	}

}
