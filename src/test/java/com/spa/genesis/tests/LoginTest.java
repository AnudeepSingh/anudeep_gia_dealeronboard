package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.LoginPage_GIA;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LoginTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;

	private static LoginPage_GIA loginPage;

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	/*****************************************************************************************
	 * @Description : Set Up Charge Management Schedule and go back without
	 *              editing/saving schedule
	 * @Steps : Login and go to the Charge Management screen. Go back to the
	 *        dashboard without saving any schedule.
	 *
	 * @author : Neha Verma
	 * @throws IOException
	 *
	 * @Date : 09-July-2019
	 *
	 ******************************************************************************************/

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule")
	public void testInvalidUsername() throws IOException {
		Reporter.log("Login With :" + "hata412nator.com");
		loginPage.enterLoginCredentialPerformLogin("hata412nator.com", "test1234");

		String Popup_MESSAGE = loginPage.getRemoteSuccessMessageText("Please enter");

		System.out.println("Alert Save Message:" + Popup_MESSAGE);

		Assert.assertTrue(Popup_MESSAGE.contains("Please enter your email used for your account."),
				"Enter valid email id alert not appears");
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testInvalidPassword(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		Reporter.log("Login With :" + strEmail);
		loginPage.enterLoginCredentialPerformLogin(strEmail, "test14");

		String Popup_MESSAGE = loginPage.getRemoteSuccessMessageText("Incorrect");
		

		System.out.println("Alert Save Message:" + Popup_MESSAGE);

		Assert.assertTrue(Popup_MESSAGE.contains("Incorrect username or password"),
				"Incorrect Username And Password alert not appears");
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testTermsAndConditionsPage(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		Reporter.log("Login With :" + strEmail);
		loginPage.navigateTermsAndConditionsPage(strEmail, strPassword,strVIN);

		Assert.assertTrue(loginPage.labelTermsAndConditionsDisplayed(), "Terms And Conditions page not displayed");
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testRedirectPrompt(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		Reporter.log("Login With :" + "fe204@mailnesia.com");
		loginPage.enterLoginCredentialPerformLogin("fe204@mailnesia.com", "test12345");

		Assert.assertTrue(loginPage.isMyHyundaiAlertDisplayed(), "MyHyundai App alert not appears");

		loginPage.clickCloseBtnGenesisApp();

		Assert.assertTrue(!loginPage.isMyHyundaiAlertDisplayed(), "MyHyundai App alert not closed");
		Reporter.log("Login With :" + strEmail);
		loginPage.enterLoginCredentialPerformLogin(strEmail, strPassword);

		loginPage.clickOpenBtnGenesisApp();

		Assert.assertTrue(!loginPage.isLabelGenesisDisplayed(), "Genesis App not open");

	}
	
	@Test(description = "Verify Login Page Elements", dataProvider = "TestLoginData")
	public void testLoginPgeElement(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		driver.switchTo().alert().accept();
		loginPage.clickOnLoginButton();
		Assert.assertTrue(loginPage.isImageLogoDisplay(), "GIA app logo not appear");
		Assert.assertTrue(loginPage.isRegisterButtonDisplay(), "Register button not appear");
		Assert.assertTrue(loginPage.isDemoButtonDisplay(), "Demo link not appear");
		Assert.assertTrue(loginPage.isEnableFaceIdDisplay(), "Enable face id radio button logo not appear");
		Assert.assertTrue(loginPage.isKeepMeLoginDisplay(), "Keep me login radio button not appear");

	}
	
	@Test(description = "Verify Login Page Elements", dataProvider = "TestLoginData")
	public void testRegister(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		driver.switchTo().alert().accept();
		loginPage.clickOnLoginButton();
		loginPage.clickRegisterButton();
		Assert.assertTrue(loginPage.isPopupEnterEmailIdDisplay(), "Popup Entaer Email not appear");
		Assert.assertTrue(loginPage.isCancelButtonDisplay(), "Cancel button not appear");
		Assert.assertTrue(loginPage.isSubmitButtonDisplay(), "Submit Button not appear");
		loginPage.clickCancelButton();
		Assert.assertTrue(loginPage.isRegisterButtonDisplay(), "Register button not appear");
		loginPage.clickRegisterButton();
		Assert.assertTrue(loginPage.isPopupEnterEmailIdDisplay(), "Popup Entaer Email not appear");
		loginPage.enterRegisteredEmailId(strEmail);
		loginPage.clickSubmitButton();
		Assert.assertTrue(loginPage.isPopupAlreadyRegisteredDisplay(), "Already login popup not appear");
		Assert.assertTrue(loginPage.isPopupLoginButtonDisplay(), "Login button popup not appear");
		Assert.assertTrue(loginPage.isPopupForgotPasswordButtonDisplay(), "Forgot password button popup not appear");
		Assert.assertTrue(loginPage.isCancelButtonDisplay(), "Cancel button popup not appear");
		loginPage.clickPopupLoginButton();
		Assert.assertTrue(loginPage.isRegisterButtonDisplay(), "Register button not appear");

	}


	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
