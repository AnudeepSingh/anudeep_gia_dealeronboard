package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.auto.framework.base.Log;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.EnterPinPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.DealerLocatorPage;
import com.spa.hyundai.pageObjects.EnterPinPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.MapPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class MapTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static EnterPinPage_GIA enterPinPage;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MapPage_GIA mapPage;

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		driver = empAppDevice.getDriver();
		enterPinPage = new EnterPinPage_GIA(empAppDevice);
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	@Test(description = "Test car finder with invalid and valid pin", dataProvider = "TestLoginData")
	public void testCarFinderWithInvalidValidPin(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);

		dashboardPage.navigateToMapOptionPage();
		mapPage.clickCarFinderOption();

		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "The enter pin popup not appears");

		enterPinPage.enterPin("1222");

		String Incorrect_Pin_MESSAGE = dashboardPage.getCommandsPopupMessageText("Incorrect PIN");

		System.out.println("Incorrect Pin Message:" + Incorrect_Pin_MESSAGE);

		Assert.assertTrue(Incorrect_Pin_MESSAGE.contains("Incorrect PIN"), "The incorrect pin popup not appears");

		dashboardPage.clickOnPopupOKButton();
		mapPage.clickCarFinderOption();
		enterPinPage.enterPin("1234");
		empAppDevice.sleepFor(3000);
	} 

	@Test(description = "Search Gas POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testSearchGasPOI(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT = "93551";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickNearByGas();

		Assert.assertTrue(mapPage.isGasStationPageDisplayed(), "Gas Station page is not displaying.");

		mapPage.isSearchBarDisplay();

		mapPage.enterSearchText(STR_SEARCHTEXT);
		String strAddress = mapPage.getMapLocationAddress();

		String strAddressLabel = mapPage.getMapLocationAddressLabel();
		System.out.println(strAddressLabel);
		Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");

		boolean addressExpandableList = mapPage.verifyFullAddressInExpandableList(STR_SEARCHTEXT, strAddressLabel);

		Assert.assertTrue(addressExpandableList, "Address not present in expendable list");
		String strMessage = mapPage.clickOnSaveIcon();

		System.out.println(strMessage);

		if (strMessage.contains("POI successfully added")) {

			Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
		}

		else {
			Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
					"POI is not already saved.");

		}

		mapPage.clickSendToCar();
		Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");

		mapPage.clickDownArrow();
		mapPage.navigateToFavorites();
		Assert.assertTrue(mapPage.isFavoritesPageDisplayed(), "Favorites page is not displaying.");
		boolean addressExpandableListFav = mapPage.verifyFullAddressInExpandableList(STR_SEARCHTEXT, strAddressLabel);

		Assert.assertTrue(addressExpandableListFav, "Address not present in expendable list");
		String strMessageDelete = mapPage.clickOnSaveIcon();

		System.out.println(strMessageDelete);
		Assert.assertTrue(strMessageDelete.equalsIgnoreCase("POI deleted."), "Address not present in expendable list");

		mapPage.clickSendToCar();
		Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");

	}

	@Test(description = "Search Fav POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testMyPoi(String strVehicleType, String strTestRun, String strEmail, String strPassword, String strPin,
			String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT = "93551";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickPOISearch();

		Assert.assertTrue(mapPage.isPoiSearchPageDisplayed(), "Poi Search page is not displaying.");
		Assert.assertTrue(mapPage.isSearchBarDisplay(), "POI search bar not displayed");

		mapPage.enterSearchText(STR_SEARCHTEXT);
		String strAddress = mapPage.getMapLocationAddress();
		String strAddressLabel = mapPage.getMapLocationAddressLabel();

		System.out.println(strAddress);
		Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");
		System.out.println(strAddressLabel);
		empAppDevice.sleepFor(5000);
		String strMessage = mapPage.clickOnSaveIcon();

		System.out.println(strMessage);

		if (strMessage.contains("POI successfully added")) {

			Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
		}

		else {
			Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
					"POI is not already saved.");

		}
		mapPage.clickSendToCar();
		Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");

		mapPage.clickDownArrow();
		mapPage.navigateToFavorites();
		Assert.assertTrue(mapPage.isFavoritesPageDisplayed(), "Favorites page is not displaying.");
		boolean addressExpandableList = mapPage.verifyFullAddressInExpandableList(STR_SEARCHTEXT, strAddressLabel);

		Assert.assertTrue(addressExpandableList, "Address not present in expendable list");
		String strMessageDelete = mapPage.clickOnSaveIcon();

		System.out.println(strMessageDelete);
		Assert.assertTrue(strMessageDelete.equalsIgnoreCase("POI deleted."), "Address not present in expendable list");

		mapPage.clickSendToCar();
		Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");

	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testDeleteFavPoiFromGasPOI(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT = "20105";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickGotItButton();
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickNearByGas();

		Assert.assertTrue(mapPage.isGasStationPageDisplayed(), "Gas Station page is not displaying.");

		mapPage.isSearchBarDisplay();

		mapPage.enterSearchText(STR_SEARCHTEXT);
		String strAddress = mapPage.getMapLocationAddress();

		String strAddressLabel = mapPage.getMapLocationAddressLabel();
		System.out.println(strAddressLabel);
		Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");

		String strMessage = mapPage.clickOnSaveIcon();

		System.out.println(strMessage);

		if (strMessage.contains("POI successfully added")) {

			Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
		}

		else {
			Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
					"POI is not already saved.");

		}

		mapPage.clickDownArrow();

		String strMessageDelete = mapPage.clickOnSaveIcon();

		System.out.println(strMessageDelete);
		Assert.assertTrue(strMessageDelete.equalsIgnoreCase("POI deleted."), "Address not present in expendable list");

	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testDeleteFavPoiFromPoiSearch(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT = "93551";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickPOISearch();

		Assert.assertTrue(mapPage.isPoiSearchPageDisplayed(), "Poi Search page is not displaying.");
		Assert.assertTrue(mapPage.isSearchBarDisplay(), "POI search bar not displayed");

		mapPage.enterSearchText(STR_SEARCHTEXT);
		String strAddress = mapPage.getMapLocationAddress();
		String strAddressLabel = mapPage.getMapLocationAddressLabel();

		System.out.println(strAddress);
		Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");
		System.out.println(strAddressLabel);
		empAppDevice.sleepFor(5000);
		String strMessage = mapPage.clickOnSaveIcon();

		System.out.println(strMessage);

		if (strMessage.contains("POI successfully added")) {

			Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
		}

		else {
			Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
					"POI is not already saved.");

		}
		mapPage.clickDownArrow();
		String strMessageDelete = mapPage.clickOnSaveIcon();

		System.out.println(strMessageDelete);
		Assert.assertTrue(strMessageDelete.equalsIgnoreCase("POI deleted."), "Address not present in expendable list");

	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtSearchPOI(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT = "93551";

		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickPOISearch();

		Assert.assertTrue(mapPage.isPoiSearchPageDisplayed(), "Poi Search page is not displaying.");
		mapPage.enterSearchText(STR_SEARCHTEXT);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtCarFinder(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{

		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickCarFinderOption();
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledWhenSelectGPSIcon(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{

		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();

//		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
//				"Location Service Disabled alert not displayed");
//
//		mapPage.clickCancelAlertButton();
		mapPage.clickGPSIcon();

		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");

	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtGasStation(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT = "93551";

		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickNearByGas();

		Assert.assertTrue(mapPage.isGasStationPageDisplayed(), "Gas Station page is not displaying.");
		mapPage.enterSearchText(STR_SEARCHTEXT);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtDealerLocator(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT = "93551";

		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickDealersLocatorOption();
		Assert.assertTrue(mapPage.isDealerLocatorPageDisplayed(), "Dealer Locator page is not displaying.");
		mapPage.enterSearchText(STR_SEARCHTEXT);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtFavorites(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		// String STR_SEARCHTEXT = "93551";

		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.navigateToFavorites();
		Assert.assertTrue(mapPage.isFavoritesPageDisplayed(), "Favorites page is not displaying.");
		// mapPage.enterSearchText(STR_SEARCHTEXT);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search Dealer POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testPreferredDealerLocatorScheduleService(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT = "20852";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickDealersLocatorOption();

		Assert.assertTrue(mapPage.isDealerLocatorPageDisplayed(), "Dealer Locator page is not displaying.");

		mapPage.isSearchBarDisplay();

		Assert.assertTrue(mapPage.isAddressDisplay(), "Default dealer address not displayed");

		mapPage.clickUpArrow();

		Assert.assertTrue(mapPage.getSetAsPreferedEnabledState().equals("false"),
				"Default dealer address not displayed");

		mapPage.clickBtnScheduleService();

		Assert.assertTrue(mapPage.isPageScheduleServiceDisplay(), "Schedule Service page not displayed.");
		mapPage.clickOnBackButton();

	}

	@Test(description = "Search Dealer POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testDealerLocatorScheduleServiceWithNewSearchData(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT1 = "20852";
		String STR_SEARCHTEXT2 = "90810";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickDealersLocatorOption();

		Assert.assertTrue(mapPage.isDealerLocatorPageDisplayed(), "Dealer Locator page is not displaying.");

		mapPage.isSearchBarDisplay();
		Assert.assertTrue(mapPage.isAddressDisplay(), "Default dealer address not displayed");

		

		String strAddressbefore = mapPage.getMapLocationAddress();
		if (strAddressbefore.contains(STR_SEARCHTEXT1)) {
			mapPage.enterSearchText(STR_SEARCHTEXT2);
		} else {
			mapPage.enterSearchText(STR_SEARCHTEXT1); 
		}
		mapPage.clickUpArrow();

		System.out.println(mapPage.getSetAsPreferedEnabledState());

		Assert.assertTrue(mapPage.getSetAsPreferedEnabledState().equals("true"),
				"Default dealer address not displayed");

		mapPage.clickSetAsPreferred();
		Assert.assertTrue(mapPage.islabelPreferredGenesisDealerSuccess(),
				"Preferred genesis dealer service center has been updated popup not appear");

		mapPage.clickBtnScheduleService();

		Assert.assertTrue(mapPage.isPageScheduleServiceDisplay(), "Schedule Service page not displayed.");
		mapPage.clickOnBackButton();

	}

	@Test(description = "Search Dealer POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testSetAsPreferredWithNewSearchData(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT1 = "22030";
		String STR_SEARCHTEXT2 = "90810";

		String strAddress = null;

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickDealersLocatorOption();

		Assert.assertTrue(mapPage.isDealerLocatorPageDisplayed(), "Dealer Locator page is not displaying.");

		mapPage.isSearchBarDisplay();
		Assert.assertTrue(mapPage.isAddressDisplay(), "Default dealer address not displayed");


		String strAddressBefore = mapPage.getMapLocationAddress();

		if (strAddressBefore.contains(STR_SEARCHTEXT1)) {
			mapPage.enterSearchText(STR_SEARCHTEXT2);
			strAddress = STR_SEARCHTEXT2;
		} else {
			mapPage.enterSearchText(STR_SEARCHTEXT1);
			strAddress = STR_SEARCHTEXT1;
		}
		
		mapPage.clickUpArrow();

		Assert.assertTrue(mapPage.getSetAsPreferedEnabledState().equals("true"),
				"Default dealer address not displayed");

		mapPage.clickSetAsPreferred();
		Assert.assertTrue(mapPage.islabelPreferredGenesisDealerSuccess(),
				"Preferred genesis dealer service center has been updated popup not appear");
		mapPage.clickDownArrow();
		mapPage.clickDealersLocatorOption();

		String strAddressAfter = mapPage.getMapLocationAddress();

		Assert.assertTrue(strAddressAfter.contains(strAddress), "New search address not set as preffered location");

	}

	@Test(description = "Search Dealer POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testDealerPOISearchResultWithList(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT1 = "22030";
		String STR_SEARCHTEXT2 = "90810";

		String strAddress = null;

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickDealersLocatorOption();

		Assert.assertTrue(mapPage.isDealerLocatorPageDisplayed(), "Dealer Locator page is not displaying.");

		mapPage.isSearchBarDisplay();
		Assert.assertTrue(mapPage.isAddressDisplay(), "Default dealer address not displayed");

		String strAddressBefore = mapPage.getMapLocationAddress();

		if (strAddressBefore.contains(STR_SEARCHTEXT1)) {
			mapPage.enterSearchText(STR_SEARCHTEXT2);
			strAddress = STR_SEARCHTEXT2;
		} else {
			mapPage.enterSearchText(STR_SEARCHTEXT1);
			strAddress = STR_SEARCHTEXT1;
		}
		
		String strAddressAfter = mapPage.getMapLocationAddress();
		System.out.println(strAddressAfter);
		Assert.assertTrue(strAddressAfter.contains(strAddress), "Address Not Matched");

		boolean addressExpandableList = mapPage.verifyAddressInExpandableList(strAddressAfter);

		Assert.assertTrue(addressExpandableList, "Address not present in expendable list");
		mapPage.clickUpArrow();
		mapPage.clickSendToCar();
		Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");
		mapPage.clickSetAsPreferred();
		Assert.assertTrue(mapPage.islabelPreferredGenesisDealerSuccess(),
				"Preferred genesis dealer service center has been updated popup not appear");

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
