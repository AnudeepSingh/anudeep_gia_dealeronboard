package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.auto.framework.base.Log;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.ClimateSettingsPage_GIA;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.EnterPinPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.RemoteActionsPage_GIA;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class RemotePresetTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;

	private static LoginPage_GIA loginPage;
	private static EnterPinPage_GIA enterPinPage;
	private static RemoteActionsPage_GIA remoteActionsPage;
	private static ClimateSettingsPage_GIA climateSettingsPage;
	private static DashboardPage_GIA dashboardPage;

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		enterPinPage = new EnterPinPage_GIA(empAppDevice);
		remoteActionsPage = new RemoteActionsPage_GIA(empAppDevice);
		climateSettingsPage = new ClimateSettingsPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

//	@Test(description = "Test Invalid Pin- Verify Remote Horn and Lights with Incorrect Pin shows Incorrect Pin notification", dataProvider = "TestLoginData")
//	public void testHornAndLightsWithIncorrectPin(String strVehicleType, String strTestRun, String strEmail, String strPassword,
//			String strPin, String strVIN, String strUserType) {
//
//		String strIncorrectPin = "0901";
//
//		loginPage.validLogin(strEmail, strPassword, strVIN);
//		System.out.println("Logged In using : " + strEmail);
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnHornLightsButton();
//		remoteActionsPage.clickOnHornAndLightsButton();
//		enterPinPage.enterPin(strIncorrectPin);
//		empAppDevice.sleepFor(3000);
//
//		String resultText = dashboardPage.getRemoteResultPopUpMessageText("Incorrect PIN");
//		System.out.println("Remote Result Text:" + resultText);
//		Assert.assertTrue(resultText.contains("Incorrect PIN"), "Incorrect Pin popup does not display");
//		dashboardPage.clickOnRemoteCommandOKButton();
//		enterPinPage.enterPin("1234");
//		dashboardPage.clickOnRemoteCommandOKButton();
//	}

//	@Test(description = "Test Invalid Pin- Verify Remote Flash Lights with Incorrect Pin shows Incorrect Pin notification", dataProvider = "TestLoginData")
//	public void testFlashLightsWithIncorrectPin(String strVehicleType, String strTestRun, String strEmail, String strPassword,
//			String strPin, String strVIN, String strUserType) {
//
//		String strIncorrectPin = "0901";
//
//		loginPage.validLogin(strEmail, strPassword, strVIN);
//		System.out.println("Logged In using : " + strEmail);
//
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnHornLightsButton();
//		remoteActionsPage.clickOnFlashLightsButton();
//		enterPinPage.enterPin(strIncorrectPin);
//		empAppDevice.sleepFor(3000);
//
//		String resultText = dashboardPage.getRemoteResultPopUpMessageText("Incorrect PIN");
//		System.out.println("Remote Result Text:" + resultText);
//		Assert.assertTrue(resultText.contains("Incorrect PIN"), "Incorrect Pin popup does not display");
//		dashboardPage.clickOnRemoteCommandOKButton();
//		enterPinPage.enterPin("1234");
//		dashboardPage.clickOnRemoteCommandOKButton();
//
//	}

	@Test(description = "Test Invalid Pin- Verify Remote Unlock with Incorrect Pin shows Incorrect Pin notification", dataProvider = "TestLoginData")
	public void testRemoteUnlockWithIncorrectPin(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) {

		String strIncorrectPin = "0901";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		System.out.println("Logged In using : " + strEmail);

		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnDoorLocksButton();
		remoteActionsPage.clickOnUnlockButton();
		enterPinPage.enterPin(strIncorrectPin);
		empAppDevice.sleepFor(3000);
		String resultText = dashboardPage.getCommandsPopupMessageText("Incorrect PIN");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("Incorrect PIN"), "Incorrect Pin popup does not display");
		dashboardPage.clickOnPopupOKButton();
		enterPinPage.enterPin("1234");
		dashboardPage.clickOnPopupOKButton();

	}

	@Test(description = "Test Invalid Pin- Verify Remote Lock with Incorrect Pin shows Incorrect Pin notification", dataProvider = "TestLoginData")
	public void testRemoteLockWithIncorrectPin(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) {

		String strIncorrectPin = "0901";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		System.out.println("Logged In using : " + strEmail);

		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnDoorLocksButton();
		remoteActionsPage.clickOnLockButton();
		enterPinPage.enterPin(strIncorrectPin);
		empAppDevice.sleepFor(3000);

		String resultText = dashboardPage.getCommandsPopupMessageText("Incorrect PIN");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("Incorrect PIN"), "Incorrect Pin popup does not display");
		dashboardPage.clickOnPopupOKButton();
		enterPinPage.enterPin("1234");
		dashboardPage.clickOnPopupOKButton();

	}

	@Test(description = "Test Invalid Pin- Verify Remote Stop with Incorrect Pin shows Incorrect Pin notification", dataProvider = "TestLoginData")
	public void testRemoteStopWithIncorrectPin(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) {

		String strIncorrectPin = "0901";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		System.out.println("Logged In using : " + strEmail);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartStopButton();
		remoteActionsPage.clickOnStopButton();
		empAppDevice.sleepFor(10000);
		enterPinPage.enterPin(strIncorrectPin);
		empAppDevice.sleepFor(3000);

		String resultText = dashboardPage.getCommandsPopupMessageText("Incorrect PIN");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("Incorrect PIN"), "Incorrect Pin popup does not display");
		dashboardPage.clickOnPopupOKButton();
		enterPinPage.enterPin("1234");
		dashboardPage.clickOnPopupOKButton();

	}

	/*****************************************************************************************
	 * @Description : Pin Locked popup shows with three incorrect attempts Test
	 * @Steps : Login and go to Flash Lights remote action. Enter Incorrect Pin.
	 *        (Repeat steps three times) Shows Pin Locked message Popup.
	 *
	 * @author : Pushpa Kumari
	 * @throws IOException
	 *
	 * @Date : 03-Jan-2020
	 *
	 ******************************************************************************************/

//	@Test(description = "Test Pin Locked- Verify Pin Locked popup is displayed on entering incorrect PIN three times", dataProvider = "TestLoginData")
//	public void testPinLockedWithThreeIncorrectAttempts(String strVehicleType, String strTestRun, String strEmail,
//			String strPassword) {
//
//		String _vin = "KM8J73A60JU000204";
//		
//		loginPage.validLogin(strEmail, strPassword, _vin);
//		System.out.println("Logged In using : " + strEmail);
//		
//		for(int i=0; i<=2; i++){
//			dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnFlashLightsButton();
//		enterPinPage.enterPin("0901");
//		empAppDevice.sleepFor(3000);
//		dashboardPage.clickOnRemoteCommandOKButton();
//		}
//		
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnFlashLightsButton();
//		enterPinPage.enterPin("0901");
//		empAppDevice.sleepFor(3000);
//		
//		String resultText = dashboardPage.getCommandsPopupMessageText("locked");
//		System.out.println("Remote Result Text:" + resultText);
//		Assert.assertTrue(resultText.contains("PIN has been locked"), "PIN locked popup is not displayed.");
//	}
//	
	/*****************************************************************************************
	 * @Description : Pin Locked popup shows with three incorrect attempts Test
	 * @Steps : Login and go to Flash Lights remote action. Enter Incorrect Pin.
	 *        (Repeat steps three times) Shows Pin Locked message Popup.
	 *
	 * @author : Pushpa Kumari
	 * @throws IOException
	 *
	 * @Date : 03-Jan-2020
	 *
	 ******************************************************************************************/

//	@Test(description = "Test Remote commands access restored- Verify User Restored Access To Remote CommandsA fter 5 Minutes", dataProvider = "TestLoginData")
//	public void testUserRestoredAccessToRemoteCommandsAfterFiveMinutes(String strVehicleType, String strTestRun, String strEmail,
//			String strPassword) {
//
//		String _vin = "KM8J73A60JU000204";
//		
//		loginPage.validLogin(strEmail, strPassword, _vin);
//		System.out.println("Logged In using : " + strEmail);
//		
//		//first send remote commands three times using incorrect pin for pin locked
//		for(int i=0; i<=2; i++){
//			dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnFlashLightsButton();
//		enterPinPage.enterPin("0901");
//		empAppDevice.sleepFor(3000);
//		dashboardPage.clickOnRemoteCommandOKButton();
//		}
//		
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnFlashLightsButton();
//		enterPinPage.enterPin("0901");
//		empAppDevice.sleepFor(3000);
//		
//		String resultText = dashboardPage.getCommandsPopupMessageText("locked");
//		System.out.println("Remote Result Text:" + resultText);
//		Assert.assertTrue(resultText.contains("PIN has been locked"), "PIN locked popup is not displayed.");
//		
//		//wait for 5-6 minutes to restore access
//		empAppDevice.sleepFor(400000);
//		
//		//verify remote commands access is restored by sending remote commands with  correct pin
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnFlashLightsButton();
//		enterPinPage.enterPin("1234");
//		empAppDevice.sleepFor(3000);
//	    resultText = dashboardPage.getRemoteResultPopUpMessageText("Remote Lights Only");
//		System.out.println("Remote Result Text:" + resultText);
//		Assert.assertNotEquals(resultText.contains("locked"), "Remote commands access is not restored succesfully.");
//	}

	/*****************************************************************************************
	 * @Description : Pin Locked popup shows with three incorrect attempts Test
	 * @Steps : Login and go to Flash Lights remote action. Enter Incorrect Pin.
	 *        (Repeat steps three times) Shows Pin Locked message Popup.
	 *
	 * @author : Pushpa Kumari
	 * @throws IOException
	 *
	 * @Date : 03-Jan-2020
	 *
	 ******************************************************************************************/

	@Test(description = "Test Invalid Pin- Verify Remote Start with Incorrect Pin shows Incorrect Pin notification", dataProvider = "TestLoginData")
	public void testRemoteStartWithIncorrectPin(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) {

		String strIncorrectPin = "0901";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		System.out.println("Logged In using : " + strEmail);

		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartStopButton();
		remoteActionsPage.clickOnStartButton();
		climateSettingsPage.clickOnbtnStartVehicleWithoutPresets();
		climateSettingsPage.clickOnbtnbtnStartVehicleNow();
		enterPinPage.enterPin(strIncorrectPin);
		empAppDevice.sleepFor(3000);

		String resultText = dashboardPage.getCommandsPopupMessageText("Incorrect PIN");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("Incorrect PIN"), "Incorrect Pin popup does not display");
		dashboardPage.clickOnPopupOKButton();
		enterPinPage.enterPin("1234");
		dashboardPage.clickOnPopupOKButton();

	}

	@Test(description = "Remote lock Test", dataProvider = "TestLoginData")
	public void remoteLock(String strVehicleType, String strTestRun, String strEmail, String strPassword, String strPin,
			String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);

		String REMOTE_COMMAND = "Remote Door Lock";

		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnDoorLocksButton();
		remoteActionsPage.clickOnLockButton();
		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);
		dashboardPage.clickOnPopupOKButton();
		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Door Lock command was not successful");

	}

	@Test(description = "Remote UnLock Test", dataProvider = "TestLoginData")
	public void remoteUnLock(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {

		String REMOTE_COMMAND = "Remote Door Unlock";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnDoorLocksButton();
		remoteActionsPage.clickOnUnlockButton();
		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);
		dashboardPage.clickOnPopupOKButton();
		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Door UnLock command was not successful");

	}

//	@Test(description = "Remote Flash Lights Test", dataProvider = "TestLoginData")
//	public void remoteLights(String strVehicleType, String strTestRun, String strEmail, String strPassword,
//			String strPin, String strVIN, String strUserType) throws IOException {
//
//		String REMOTE_COMMAND = "Remote Lights";
//		loginPage.validLogin(strEmail, strPassword, strVIN);
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnHornLightsButton();
//		remoteActionsPage.clickOnFlashLightsButton();
//		enterPinPage.enterPin("1234");
//
//		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);
//
//		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);
//
//	dashboardPage.clickOnRemoteCommandOKButton();
//		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was sent to vehicle"),
//				"The Remote Flash Lights command was not successful");
//
//	}

//	@Test(description = "Remote Horn and Lights Lights Test", dataProvider = "TestLoginData")
//	public void remoteHornAndLights(String strVehicleType, String strTestRun, String strEmail, String strPassword,
//			String strPin, String strVIN, String strUserType) throws IOException {
//
//		String REMOTE_COMMAND = "Remote Horn & Lights";
//		loginPage.validLogin(strEmail, strPassword, strVIN);
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnHornLightsButton();
//		remoteActionsPage.clickOnHornAndLightsButton();
//		enterPinPage.enterPin("1234");
//
//		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);
//
//		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);
//	dashboardPage.clickOnRemoteCommandOKButton();
//		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was sent to vehicle"),
//				"The Remote Horn and Lights command was not successful");
//
//	}

	@Test(description = "Remote Start Stop Climate with Front Defrost and Heated Features On Test", dataProvider = "TestLoginData")
	public void remoteClimateStartStopWithTemperatureOnFrontDefrostOnHeatedAccessoriesOn(String strVehicleType,
			String strTestRun, String strEmail, String strPassword, String strPin, String strVIN, String strUserType)
			throws IOException {

		String REMOTE_COMMAND = "Remote Start";
		String REMOTE_COMMAND2 = "Remote Control Stop";
		String TEMERATURE_VALUE = "70";
		String FRONT_DEFROST_VALUE = "1";
		String HEATED_ACCESSORIES_VALUE = "1";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartStopButton();
		remoteActionsPage.clickOnStartButton();
		climateSettingsPage.clickOnbtnStartVehicleWithoutPresets();
		// climateSettingsPage.setTemperatureScrollValue(TEMERATURE_VALUE);
		climateSettingsPage.setStartSettingsStates(FRONT_DEFROST_VALUE, HEATED_ACCESSORIES_VALUE);
		climateSettingsPage.clickOnbtnbtnStartVehicleNow();
		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);
		dashboardPage.clickOnPopupOKButton();
		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Start command was not successful");
		remoteActionsPage.clickOnBackArror();
		remoteActionsPage.clickOnStartStopButton();

		remoteActionsPage.clickOnStopButton();
		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE2 = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND2);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE2);
		dashboardPage.clickOnPopupOKButton();
		Assert.assertTrue(REMOTE_COMMAND_MESSAGE2.contains("was successful"),
				"The Remote Stop command was not successful");

	}

	@Test(description = "Remote Start Stop with Front Defrost and Heated Features  Off Test", dataProvider = "TestLoginData")
	public void remoteClimateStartStopWithTemperatureOffFrontDefrostOffHeatedAccessoriesOff(String strVehicleType,
			String strTestRun, String strEmail, String strPassword, String strPin, String strVIN, String strUserType)
			throws IOException {

		String REMOTE_COMMAND = "Remote Start";
		String REMOTE_COMMAND2 = "Remote Control Stop";
		String TEMERATURE_VALUE = "70";
		String FRONT_DEFROST_VALUE = "0";
		String HEATED_ACCESSORIES_VALUE = "0";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartStopButton();
		remoteActionsPage.clickOnStartButton();

		climateSettingsPage.clickOnbtnStartVehicleWithoutPresets();
		// climateSettingsPage.setTemperatureScrollValue(TEMERATURE_VALUE);
		climateSettingsPage.setStartSettingsStates(FRONT_DEFROST_VALUE, HEATED_ACCESSORIES_VALUE);
		climateSettingsPage.clickOnbtnbtnStartVehicleNow();

		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);
		climateSettingsPage.clickOnRemoteCommandOKButton();
		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Start command was not successful");

		remoteActionsPage.clickOnBackArror();
		remoteActionsPage.clickOnStartStopButton();

		remoteActionsPage.clickOnStopButton();
		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE2 = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND2);
		dashboardPage.clickOnPopupOKButton();

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE2);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE2.contains("was successful"),
				"The Remote Stop command was not successful");

	}

	@Test(description = "Remote Stop Failure Test", dataProvider = "TestLoginData")
	public void remoteClimateStopFailure(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartStopButton();
		remoteActionsPage.clickOnStopButton();
		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText("Remote Stop");
		dashboardPage.clickOnPopupOKButton();

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("Remote Stop for your vehicle cannot be processed."),
				"The Remote Stop command was  successful");

	}

	/*****************************************************************************************
	 * @Description : Remote Charge Start Test
	 * @Steps : Login and send a Remote Start Charge Request with correct PIN
	 *
	 * @author : Rashmi Sharma
	 * @throws IOException
	 *
	 * @Date : 08-July-2019
	 *
	 ******************************************************************************************/

	@Test(description = "Remote Start Stop with Settings On Test", dataProvider = "TestLoginData")
	public void remoteStartStopWithTemperatureOnFrontDefrostOnHeatedAccessoriesOn_Gas(String strVehicleType,
			String strTestRun, String strEmail, String strPassword, String strPin, String strVin) throws IOException {
		String REMOTE_COMMAND = "Remote Start";
		String REMOTE_COMMAND2 = "Remote Control Stop";
		String TEMERATURE_VALUE = "1";
		String FRONT_DEFROST_VALUE = "1";
		String HEATED_ACCESSORIES_VALUE = "1";

		loginPage.validLogin(strEmail, strPassword, strVin);
		System.out.println(1);
		dashboardPage.clickRemoteOption();
		System.out.println(2);
		remoteActionsPage.clickOnStartStopButton();
		System.out.println(3);
		remoteActionsPage.clickOnStartButton();
		climateSettingsPage.clickOnbtnStartVehicleWithoutPresets();
		// climateSettingsPage.setTemperatureScrollValue(TEMERATURE_VALUE);
		climateSettingsPage.setStartSettingsStates(TEMERATURE_VALUE, FRONT_DEFROST_VALUE, HEATED_ACCESSORIES_VALUE);
		climateSettingsPage.clickOnbtnbtnStartVehicleNow();

		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);
		dashboardPage.clickOnPopupOKButton();

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Start command was not successful");
		remoteActionsPage.clickOnBackArror();
		remoteActionsPage.clickOnStartStopButton();
		remoteActionsPage.clickOnStopButton();
		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE2 = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND2);
		dashboardPage.clickOnPopupOKButton();

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE2);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE2.contains("was successful"),
				"The Remote Stop command was not successful");

	}

	/*****************************************************************************************
	 * @Description : Remote Start Stop with Settings Off Test
	 * @Steps : Login and send a Remote Start Request with Temperature OFF, Front
	 *        Defroster OFF and Heated Accessories OFF correct PIN and then send a
	 *        Remote Stop Command
	 *
	 * @author : Rashmi Sharma
	 * @throws IOException
	 *
	 * @Date : 13-June-2019
	 *
	 ******************************************************************************************/

	@Test(description = "Remote Start Stop with Settings Off Test", dataProvider = "TestLoginData")
	public void remoteStartStopWithTemperatureOffFrontDefrostOffHeatedAccessoriesOff_Gas(String strVehicleType,
			String strTestRun, String strEmail, String strPassword, String strPin, String strVin) throws IOException {
		String REMOTE_COMMAND = "Remote Start";
		String REMOTE_COMMAND2 = "Remote Control Stop";
		String TEMERATURE_VALUE = "0";
		String FRONT_DEFROST_VALUE = "0";
		String HEATED_ACCESSORIES_VALUE = "0";

		loginPage.validLogin(strEmail, strPassword, strVin);
		System.out.println(1);
		dashboardPage.clickRemoteOption();
		System.out.println(2);
		remoteActionsPage.clickOnStartStopButton();
		System.out.println(3);

		climateSettingsPage.clickOnbtnStartVehicleWithoutPresets();
		// climateSettingsPage.setTemperatureScrollValue(TEMERATURE_VALUE);
		climateSettingsPage.setStartSettingsStates(TEMERATURE_VALUE, FRONT_DEFROST_VALUE, HEATED_ACCESSORIES_VALUE);
		climateSettingsPage.clickOnbtnbtnStartVehicleNow();

		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);

		climateSettingsPage.clickOnRemoteCommandOKButton();

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Start command was not successful");

		remoteActionsPage.clickOnBackArror();

		remoteActionsPage.clickOnStartStopButton();

		remoteActionsPage.clickOnStopButton();
		enterPinPage.enterPin("1234");

		String REMOTE_COMMAND_MESSAGE2 = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND2);
		dashboardPage.clickOnPopupOKButton();

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE2);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE2.contains("was successful"),
				"The Remote Stop command was not successful");

	}
}
