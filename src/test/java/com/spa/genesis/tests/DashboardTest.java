package com.spa.genesis.tests;

import java.io.IOException;

import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.ClimateSettingsPage_GIA;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.EnterPinPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;
import com.spa.genesis.pageObjects.MenuPage_GIA;
import com.spa.genesis.pageObjects.RemoteActionsPage_GIA;
import com.spa.genesis.pageObjects.SettingsPage_GIA;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class DashboardTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;

	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MapPage_GIA mapPage;
	private static ClimateSettingsPage_GIA climateSettingsPage;
	private static MenuPage_GIA menuPage;
	private static SettingsPage_GIA settingsPage;
	private static EnterPinPage_GIA enterPinPage;
	private static RemoteActionsPage_GIA remoteActionsPage;

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
		climateSettingsPage = new ClimateSettingsPage_GIA(empAppDevice);
		menuPage = new MenuPage_GIA(empAppDevice);
		settingsPage = new SettingsPage_GIA(empAppDevice);
		enterPinPage = new EnterPinPage_GIA(empAppDevice);
		remoteActionsPage = new RemoteActionsPage_GIA(empAppDevice);

	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	@Test(description = "Verify the elements present in dashboard", dataProvider = "TestLoginData")
	public void testElementsPresentOnDashboard(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		String strCurrentLocation = "Greater Noida";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		//Assert.assertTrue(dashboardPage.isWeatherTempratureIconDisplayed(), "Weather icon not displayed");
		Assert.assertTrue(dashboardPage.isCurrentLocationDisplay(strCurrentLocation), "Current Location not displayed");
		Assert.assertTrue(dashboardPage.isRemoteStartButtonDisplay(), "Remote Start button not displayed");
		Assert.assertTrue(dashboardPage.isUnlockButtonDisplay(), "Remote Unlock button not displayed");
		Assert.assertTrue(dashboardPage.isLockButtonDisplay(), "Remote Lock button not displayed");

	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testSearchDashboardPOI(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException

	{

		String str_SearchText = "20001";
		loginPage.validLogin(strEmail, strPassword, strVIN);

		Assert.assertTrue(dashboardPage.isSearchFielDisplay(), "Search field not present");

		dashboardPage.enterSearchText(str_SearchText);
		Assert.assertTrue(mapPage.isPoiSearchPageDisplayed(), "Poi Search page is not displaying.");
		Assert.assertTrue(mapPage.searchResultGetText(str_SearchText).contains(str_SearchText), "Address Not Matched");

	}

	@Test(description = "Verify the elements present in dashboard", dataProvider = "TestLoginData")
	public void testNavigateToRemoteStartPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		String REMOTE_COMMAND = "Remote Start";
		String REMOTE_COMMAND2 = "Remote Control Stop";
		String TEMERATURE_VALUE = "70";
		String FRONT_DEFROST_VALUE = "1";
		String HEATED_ACCESSORIES_VALUE = "1";

		dashboardPage.clickOnRemoteStarttButton();
		Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		Assert.assertTrue(climateSettingsPage.isSwitchDefrostDisplay(), "Defrost not displayed");

		climateSettingsPage.setStartSettingsStates(FRONT_DEFROST_VALUE, HEATED_ACCESSORIES_VALUE);

		enterPinPage.enterPin(strVIN);
		dashboardPage.clickOnPopupOKButton();

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Start command was not successful");

		climateSettingsPage.clickOnRemoteCommandOKButton();
		remoteActionsPage.clickOnBackArror();
		dashboardPage.clickOnRemoteStarttButton();
		remoteActionsPage.clickOnStopButton();
		enterPinPage.enterPin(strVIN);
		dashboardPage.clickOnPopupOKButton();

		String REMOTE_COMMAND_MESSAGE2 = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND2);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE2);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE2.contains("was successful"),
				"The Remote Stop command was not successful");

	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testSearchDashboardPoiDisablePopup(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{

		String str_SearchText = "20001";
		loginPage.validLogin(strEmail, strPassword, strVIN);

		Assert.assertTrue(dashboardPage.isSearchFielDisplay(), "Search field not present");

		dashboardPage.enterSearchText(str_SearchText);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

//	@Test(description = "Verify the elements present in dashboard", dataProvider = "TestLoginData")
//	public void testCancelScheduleService(String strVehicleType, String strTestRun, String strEmail, String strPassword,
//			String strPin, String strVIN, String strUserType) throws IOException {
//		loginPage.validLogin(strEmail, strPassword, strVIN);
//		dashboardPage.clickSchedule();
//
//		Assert.assertTrue(dashboardPage.isPopupCarCareScheduleDisplay(), "Car care popup not displayed");
//
//		Assert.assertTrue(dashboardPage.isCancelButtonDisplay(), "Cancel button not displayed");
//
//		Assert.assertTrue(dashboardPage.isVisitWebsiteButtonDisplay(), "Visit Website button not displayed");
//
//		dashboardPage.clickCancel();
//
//		Assert.assertTrue(!dashboardPage.isPopupCarCareScheduleDisplay(), "Car care popup displayed");
//
//	}

	@Test(description = "Verify the elements present in dashboard", dataProvider = "TestLoginData")
	public void testAddEvent(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);

		Assert.assertTrue(dashboardPage.isLinkAddEventDisplay(), "Add Event link not displayed");
		dashboardPage.clickAddEvent();
		dashboardPage.clickAddEventFromAddEventWindow();

		Assert.assertTrue(dashboardPage.isCancelButtonDisplay(), "Cancel button not displayed");

		Assert.assertTrue(dashboardPage.isAddEventButtonPopupDisplay(), "Add Event button not displayed");

		System.out.println(dashboardPage.getResultPopUpMessageText("Add Event"));
		Assert.assertTrue(
				dashboardPage.getResultPopUpMessageText("Add Event")
						.contains("Add an event with location and start time in calendar app to get Genesis alert."),
				"Add Event popup not displayed");
	}

	@Test(description = "Verify quick remote start by toggle on the quick start  ", dataProvider = "TestLoginData")
	public void testQuickStart(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		clickQuickStartToggleAndBackToArrowButton();
		dashboardPage.clickOnRemoteStarttButton();
		Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
	}

	@Test(description = "Verify long press the remote start by toggle on the quick start", dataProvider = "TestLoginData")
	public void testQuickStartLongPress(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		clickQuickStartToggleAndBackToArrowButton();
		dashboardPage.longPressOnRemoteStartButton();
		Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		//Assert.assertTrue(climateSettingsPage.isSwitchDefrostDisplay(), "Defrost not displayed");
	}

	@Test(description = "Verify quick remote start by toggle on the quick start  ", dataProvider = "TestLoginData")
	public void testLockButton(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		String REMOTE_COMMAND = "Remote Door Lock";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickLockButton();
		Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
		enterPinPage.enterPin(strVIN);

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Door Lock command was not successful");
	}

	@Test(description = "Verify quick remote start by toggle on the quick start  ", dataProvider = "TestLoginData")
	public void testUnLockButton(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		String REMOTE_COMMAND = "Remote Door Unlock";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickUnLockButton();
		Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
		enterPinPage.enterPin(strVIN);

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Door UnLock command was not successful");
	}

	@Test(description = "Verify quick remote start by toggle on the quick start  ", dataProvider = "TestLoginData")
	public void testTutorialPage(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.enterLoginCredentialPerformLogin(strEmail, strPassword);
		loginPage.handelSwichVehicle(strVIN);

		Assert.assertTrue(dashboardPage.isLabelQuickStartTutorialDisplay(), "Quick Star Tutorial not displayed");

		Assert.assertTrue(dashboardPage.isLabelPresetTutorialDisplay(), "Preset Tutorial not displayed");

		Assert.assertTrue(dashboardPage.isLabelLockTutorailDisplay(), "Lock Tutorial not displayed");

		Assert.assertTrue(dashboardPage.isLabelUnLockTutorialDisplay(), "Unlock Tutorial not displayed");

		Assert.assertTrue(dashboardPage.isLabelSettingTutorailDisplay(), "Setting Tutorial not displayed");

		Assert.assertTrue(dashboardPage.isLabelRemoteFeatureTutorialDisplay(), "Remote Feature Tutorial not displayed");
		dashboardPage.clickGoToSettingBtn();
		loginPage.clickAllAfterExitPopup();
		Assert.assertTrue(settingsPage.isSettingsPageDisplayed(), "Setting Page not displayed");

	}

	@Test(description = "Verify long press the remote start by toggle on the quick start", dataProvider = "TestLoginData")
	public void testLockLongPress(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		clickQuickStartToggleAndBackToArrowButton();
		dashboardPage.longPressOnLockButton();
		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
	}

	@Test(description = "Verify long press the remote start by toggle on the quick start", dataProvider = "TestLoginData")
	public void testUnlockLongPress(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		clickQuickStartToggleAndBackToArrowButton();
		dashboardPage.longPressOnUnlockButton();
		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
	}

	@Test(description = "Verify long press the remote start by toggle on the quick start", dataProvider = "TestLoginData")
	public void testByDefaultQuickStartToggle(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		String strToggleValue = settingsPage.getQuickStartToggleOnOffValue();
		System.out.println(strToggleValue);
		Assert.assertEquals(strToggleValue, "0", "The Quick Start toggle is off by default");
	}

	@Test(description = "Verify long press the remote start by toggle on the quick start", dataProvider = "TestLoginData")
	public void testCovid19popupWithDismissBtn(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.LoginWithoutCovid19Handel(strEmail, strPassword, strVIN);
		String strCovid19Message = dashboardPage.getResultPopUpMessageText("Important Information");

		Assert.assertEquals(strCovid19Message,
				"The safety, health and well-being of Genesis customers is of the utmost importance. Learn more about our response to COVID-19.",
				strCovid19Message + " covid message displayed instead of expected one.");
		
		Assert.assertTrue(loginPage.isDismissBtnDisplayed(), "Dismiss button not displayed");
		
		Assert.assertTrue(!dashboardPage.isRemoteStartButtonDisplay(), "Remote Start button is displayed");
			
		loginPage.clickDismissButton();
		
		dashboardPage.clickGotItButton();
		Assert.assertTrue(dashboardPage.isRemoteStartButtonDisplay(), "Remote Start button not displayed");
		
	}
	
	@Test(description = "Verify long press the remote start by toggle on the quick start", dataProvider = "TestLoginData")
	public void testCovid19popupWithLearnMoreBtn(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.LoginWithoutCovid19Handel(strEmail, strPassword, strVIN);
		String strCovid19Message = dashboardPage.getResultPopUpMessageText("Important Information");

		Assert.assertEquals(strCovid19Message,
				"The safety, health and well-being of Genesis customers is of the utmost importance. Learn more about our response to COVID-19.",
				strCovid19Message + " covid message displayed instead of expected one.");
		
		Assert.assertTrue(loginPage.isLearnMoreBtnDisplayed(), "Learn More button not displayed");
		
		loginPage.clickLearenMoreButton();
		
		Assert.assertTrue(loginPage.isCovid19LabelDisplayed(), "Covid 19 label page is displayed");
		
		Assert.assertTrue(loginPage.isCovid19textDisplayed(), "Covid 19 text not displayed");
		
	}

	private void clickQuickStartToggleAndBackToArrowButton() {
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.clickQuickStartToggle();
		settingsPage.clickArrowBack();

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
