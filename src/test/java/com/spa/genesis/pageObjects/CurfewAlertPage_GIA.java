package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindAll;
import io.appium.java_client.pagefactory.iOSFindBy;

public class CurfewAlertPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;

	public CurfewAlertPage_GIA(BlueLinkAppDevice empAppDevice) { 
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	@iOSFindBy(accessibility = "CURFEW ALERT")
	private MobileElement _txtTitle;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='From']/ancestor:: XCUIElementTypeOther/XCUIElementTypeButton[1]")
	private MobileElement _txtFromDateTimeSelect;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='From']/ancestor:: XCUIElementTypeOther/XCUIElementTypeButton[2]")
	private MobileElement _txtToDateTimeSelect;

	@iOSFindBy(accessibility = "save button")
	private MobileElement _btnSave;

	@iOSFindBy(accessibility = "Successfully Saved.")
	private MobileElement _popUpSave;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	@iOSFindBy(accessibility = "delete button")
	private MobileElement btnDelete;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Delete']")
	private MobileElement btnDeletePopUp;

	public boolean isPresent() {
		appDevice.waitForElementToLoad(_txtTitle);
		return _txtTitle.isDisplayed();
	}

	public void setCurfewFromValues(String day, String hour, String minute, String amPm) {

		_txtFromDateTimeSelect.click();
		List<MobileElement> PickerColumns = driver
				.findElements(By.xpath("//XCUIElementTypePicker/XCUIElementTypePickerWheel"));

		PickerColumns.get(0).setValue(day);
		PickerColumns.get(1).setValue(hour);
		PickerColumns.get(2).setValue(minute);
		PickerColumns.get(3).setValue(amPm);

		driver.findElement(By.xpath("//XCUIElementTypeButton[@name='Done']")).click();

	}

	public void setCurfewToValues(String day, String hour, String minute, String amPm) {

		_txtToDateTimeSelect.click();
		List<MobileElement> PickerColumns = driver
				.findElements(By.xpath("//XCUIElementTypePicker/XCUIElementTypePickerWheel"));

		PickerColumns.get(0).setValue(day);
		PickerColumns.get(1).setValue(hour);
		PickerColumns.get(2).setValue(minute);
		PickerColumns.get(3).setValue(amPm);

		driver.findElement(By.xpath("//XCUIElementTypeButton[@name='Done']")).click();

	}

	public void clickOnSaveButton() {

		_btnSave.click();
		appDevice.sleepFor(10000);
		_btnOkPopUp.click();
		appDevice.sleepFor(3000);

	}

	public boolean isMentioedCurfewDisplay(String curfewFromData, String curfewToData) {
		boolean flag = false;
		try {
			MobileElement PickerColumns = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"
					+ curfewFromData + "']/following-sibling::XCUIElementTypeStaticText[@name='" + curfewToData
					+ "']/parent::XCUIElementTypeCell"));
			return PickerColumns.isDisplayed();
		} catch (Exception e) {
			return flag;
		}
	}
	
	public void selectCurfew(String curfewFromData, String curfewToData) {
		if (isMentioedCurfewDisplay(curfewFromData, curfewToData)) {
			MobileElement PresentCurfew = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"
					+ curfewFromData + "']/following-sibling::XCUIElementTypeStaticText[@name='" + curfewToData
					+ "']/parent::XCUIElementTypeCell"));
			PresentCurfew.click();
		}

	}

	public void selectCurfewAndDelete(String curfewFromData, String curfewToData) {
		if (isMentioedCurfewDisplay(curfewFromData, curfewToData)) {
			MobileElement PresentCurfew = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"
					+ curfewFromData + "']/following-sibling::XCUIElementTypeStaticText[@name='" + curfewToData
					+ "']/parent::XCUIElementTypeCell"));
			PresentCurfew.click(); 
			deleteSelectedCurfew(); 
		}

	}

	public void deleteSelectedCurfew() {
		btnDelete.click();
		btnDeletePopUp.click();
		appDevice.waitForElementToLoad(_btnOkPopUp);
		_btnOkPopUp.click();
	}
}
