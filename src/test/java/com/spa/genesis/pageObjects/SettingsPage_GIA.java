package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.lang.model.element.Element;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class SettingsPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "SETTINGS")
	private MobileElement _txtSettings;

	@iOSFindBy(accessibility = "Change PIN")
	private MobileElement _btnChangePin;

	@iOSFindBy(accessibility = "AUTOMATIC COLLISION NOTIFICATION (ACN)")
	private MobileElement _txtACN;

	@iOSFindBy(accessibility = "SOS EMERGENCY ASSISTANCE")
	private MobileElement _txtSosEmergencyAssitance;

	@iOSFindBy(accessibility = "AUTOMATIC DTC")
	private MobileElement _txtAutomaticDtc;

	@iOSFindBy(accessibility = "MONTHLY VEHICLE HEALTH REPORT")
	private MobileElement _txtMonthlyVehicleHealthReport;

	@iOSFindBy(accessibility = "MAINTENANCE ALERT")
	private MobileElement _txtMaintenanceAlert;

	@iOSFindBy(accessibility = "sdm navibar back")
	private MobileElement _iconNavigateBack;

	@iOSFindBy(xpath = "(//XCUIElementTypeSwitch)[2]")
	private MobileElement toggleQuickStart;

	@iOSFindBy(accessibility = "arrow back")
	private MobileElement _iconArrowBack;

	public SettingsPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public void clickQuickStartToggle() {
		appDevice.waitForElementToLoad(toggleQuickStart);
		toggleQuickStart.click();
	}

	public String getQuickStartToggleOnOffValue() {
		appDevice.waitForElementToLoad(toggleQuickStart);
		return toggleQuickStart.getAttribute("value");
	}

	public boolean isArrowIconDisplay() {
		appDevice.waitForElementToLoadWithProvidedTime(_iconArrowBack,2);
		System.out.println("AfterMethod "+appDevice.isElementDisplayed(_iconArrowBack));
		return appDevice.isElementDisplayed(_iconArrowBack);
	}
	public void clickArrowBack() {
		appDevice.waitForElementToLoad(_iconArrowBack);
		_iconArrowBack.click();
	}

	public boolean isSettingsPageVisible() {
		return appDevice.isElementDisplayed(_txtSettings);
	}

	public boolean isSettingsPageDisplayed() {
		appDevice.waitForElementToLoad(_txtSettings);
		appDevice.sleepFor(2000);
		return appDevice.isElementDisplayed(_txtSettings);
	}



	public String toggleAllNotification(String notificationType, int notificationIndex) {

//		 notificationType = "Remote" or "Connected Care" , notificationIndex= 1 for
//		 Text, 2 for Email,3 for App
		MobileElement toggleAllXpath = driver.findElement(By.xpath("//XCUIElementTypeOther[@name='" + notificationType
				+ "']/following-sibling::XCUIElementTypeButton[@name='rectangleCopy'][" + notificationIndex + "]"));
		String beforeClick=toggleAllXpath.getAttribute("value");
		System.out.println(beforeClick);
		appDevice.sleepFor(5000);
		toggleAllXpath.click();
		appDevice.sleepFor(5000);
		String afterClick=toggleAllXpath.getAttribute("value");
		System.out.println(afterClick);
		Assert.assertTrue(afterClick!=beforeClick, notificationType +" Toggle value not changed");
		return afterClick;

	}



	public String getNotifictaionStateOfSetting(String notificationText, String notificationType) {
		// notificationText=" Element text to be passed" and notificationType = "text"
		// or "email or "phone"
		
		if (notificationType.equalsIgnoreCase("text")) {
			notificationType = "btnTextSelected";

		} else if (notificationType.equalsIgnoreCase("email")) {
			notificationType = "btnEmailNormal";
		} else {
			notificationType = "textIcon 1";
		}

		return driver
				.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + notificationText
						+ "']//XCUIElementTypeButton[@name='"+notificationType+"']"))
				.getAttribute("value");
	}
	
	public String getNotifictaionEnabledState(String notificationText, String notificationType) {
		// notificationText=" Element text to be passed" and notificationType = "text"
		// or "email or "phone"
		
		if (notificationType.equalsIgnoreCase("text")) {
			notificationType = "btnTextSelected";

		} else if (notificationType.equalsIgnoreCase("email")) {
			notificationType = "btnEmailNormal";
		} else {
			notificationType = "textIcon 1";
		}

		return driver
				.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + notificationText
						+ "']/following-sibling::XCUIElementTypeButton[@name='"+notificationType+"']"))
				.getAttribute("enabled");
	}

	public List<MobileElement> xpathOfAllNotificationTexts(String notificationType) {
		return driver.findElements(By.xpath("//XCUIElementTypeOther[@name='" + notificationType
				+ "']/following-sibling::XCUIElementTypeCell[@enabled='true']/XCUIElementTypeStaticText"));
	}

	public void clickOnToggleButtonForNotification(String notificationText, String notificationType) {

		if (notificationType.equalsIgnoreCase("text")) {
			notificationType = "btnTextSelected";

		} else if (notificationType.equalsIgnoreCase("email")) {
			notificationType = "btnEmailNormal";
		} else {
			notificationType = "textIcon 1";
		}
		// notificationText=" Element text to be passed" and notificationType = "text"
		// or "email or "phone"
		MobileElement notificationElement = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"
				+ notificationText + "']/following-sibling::XCUIElementTypeButton[@name='" + notificationType + "']"));
		Assert.assertTrue(notificationElement.isDisplayed());

		String toggleEnableStatus = notificationElement.getAttribute("enabled");
		if (toggleEnableStatus.equalsIgnoreCase("true")) {

			String oldToggleStatus = notificationElement.getAttribute("value");
			notificationElement.click();

			appDevice.sleepFor(7000);
			String newToggleStatus = notificationElement.getAttribute("value");
			System.out.println(oldToggleStatus);
			System.out.println(newToggleStatus);
			Assert.assertTrue(oldToggleStatus != newToggleStatus,
					notificationText + " toggle not changing its state after click");
		} else {
			Assert.assertTrue(toggleEnableStatus.equalsIgnoreCase("false"),
					notificationText + " toggle is not in enabled state");

		}

	}

	public void navigateBack() {
		_iconNavigateBack.click();

	}

	public void scrollByText(String notificationType, int notificationIndex) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap scrollObjectDown = new HashMap<>();
		scrollObjectDown.put("direction", "up");
		for (int i = 0; i <= 1; i++) {
			boolean ele = getNotificationElement(notificationType, notificationIndex);
			System.out.println(ele);

			if (ele) {
				break;
			} else {
				System.out.println(3);
				js.executeScript("mobile: swipe", scrollObjectDown);
			}
		}
		appDevice.sleepFor(5000);

	}

	public boolean getNotificationElement(String notificationType, int notificationIndex) {
		try {
//notificationType = "Remote" or "Connected Care" , notificationIndex= 1 for Text, 2 for Email,3 for App

			MobileElement ele = driver.findElement(By.xpath("//XCUIElementTypeOther[@name='" + notificationType
					+ "']/following-sibling::XCUIElementTypeButton[@name='rectangleCopy'][" + notificationIndex + "]"));
			return appDevice.isElementDisplayed(ele);
		} catch (Exception e) {
			return false;
		}

	}

}
