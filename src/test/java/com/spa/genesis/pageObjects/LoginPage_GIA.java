package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;

public class LoginPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@AndroidFindBy(id ="com.stationdm.genesis:id/login_username")
	@iOSFindBy(xpath = "//XCUIElementTypeTextField")
	private MobileElement inputUsername;

	@AndroidFindBy(id ="login_password")
	@iOSFindBy(xpath = "//XCUIElementTypeSecureTextField")
	private MobileElement inputPassword;

	@AndroidFindBy(id ="login_btn")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='LOGIN']")
	private MobileElement _btnLogin;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Register']")
	private MobileElement _btnRegister;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Demo mode']")
	private MobileElement _linkDemo;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Enable Face ID']/preceding-sibling:: XCUIElementTypeImage[@name='login_checkbox_off'][last()]")
	private MobileElement radioBtnEnableFaceId;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Keep me logged in']/preceding-sibling:: XCUIElementTypeImage[@name='login_checkbox_off'][last()]")
	private MobileElement radioBtnKeepMeLogin;

	@AndroidFindBy(id="android:id/button1")
	@iOSFindBy(accessibility = "Accept")
	private MobileElement _btnAccept;

	@AndroidFindBy(id="permission_allow_foreground_only_button")
	private MobileElement _btnlocationallow;
	
	@AndroidFindBy(id = "permission_allow_button")
	private MobileElement _btnAllow;

	@AndroidFindBy(id="com.android.permissioncontroller:id/permission_allow_foreground_only_button")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Allow While Using App']")
	private MobileElement _btnAllowUsingAppPopup;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Don’t Allow']")
	private MobileElement _btnDontUsingAppPopup;

	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[contains(@name,'Allow')]")
	private List<MobileElement> _btnAllow1d;

	@iOSFindBy(accessibility = "View")
	private MobileElement _btnViewDetails;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='TERMS & CONDITIONS'])[1]")
	private MobileElement _labelTermsAndConditions;

	@iOSFindBy(xpath = "//XCUIElementTypeAlert[@name='MyHyundai App']")
	private MobileElement alertMyHyundaiApp;

	@iOSFindBy(accessibility = "Close")
	private MobileElement btnCloseGenesisApp;

	@iOSFindBy(accessibility = "Open")
	private MobileElement btnOpenGenesisApp;

	@iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='Genesis']")
	private MobileElement labelGenesis;

	@iOSFindBy(accessibility = "gen_login_icLogo")
	private MobileElement imageLogo;

	@AndroidFindBy(id="com.stationdm.genesis:id/iaetut_gotit_btn")
	@iOSFindBy(accessibility = "IAEtut btn gotit default")
	private MobileElement _btnGotIt;

	@iOSFindBy(accessibility = "Dismiss")
	private MobileElement _btnDismiss;

	@iOSFindBy(accessibility = "Learn More")
	private MobileElement _btnLearnMore;

	@iOSFindBy(accessibility = "IMPORTANT INFORMATION")
	private MobileElement labelCovid19Page;

	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='Information for our customers regarding COVID-19']")
	private MobileElement txtCovid19Page;

	@AndroidFindBy(id="com.stationdm.genesis:id/tvClose")
	@iOSFindBy(accessibility = "Exit")
	private MobileElement _linkExit;

	@iOSFindBy(accessibility = "SELECT VEHICLE")
	private MobileElement _txtSelectVehicle;

	
	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	@iOSFindBy(accessibility = "Cancel")
	private MobileElement _btnCancel;

	@iOSFindBy(accessibility = "Submit")
	private MobileElement _btnSubmit;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Please Enter Your Email Address']")
	private MobileElement popupEnterEmailId;

	@iOSFindBy(xpath = "//XCUIElementTypeAlert[@name='Please Enter Your Email Address']//XCUIElementTypeTextField")
	private MobileElement inputEnterEmailAddress;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='This Email Is Already Registered']")
	private MobileElement ipopupAlreadyRegistered;

	@AndroidFindBy(id ="login_btn")
	@iOSFindBy(accessibility = "Login")
	private MobileElement btnPopupLogin;

	@iOSFindBy(accessibility = "Forgot Password")
	private MobileElement btnPopupForgotPassword;
	
	//@AndroidFindBy(xpath="//android.widget.EditText[text(),'User ID']")
	@AndroidFindBy(id="com.stationdm.genesis:id/login_username")
	@iOSFindBy(accessibility = "User ID")
	private MobileElement txtUserId;
	
	//@AndroidFindBy(xpath="//android.widget.EditText[text(),'Password']")
	@AndroidFindBy(id="com.stationdm.genesis:id/login_password")
	@iOSFindBy(accessibility = "Password")
	private MobileElement txtPassword;
	

	public LoginPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public void clickRegisterButton() {
		appDevice.waitForElementToLoad(_btnRegister);
		_btnRegister.click();
	}

	public boolean isPopupAlreadyRegisteredDisplay() {
		appDevice.waitForElementToLoad(ipopupAlreadyRegistered);
		return ipopupAlreadyRegistered.isEnabled();
	}

	public boolean isPopupLoginButtonDisplay() {
		appDevice.waitForElementToLoad(btnPopupLogin);
		return btnPopupLogin.isEnabled();
	}

	public void clickPopupLoginButton() {
		appDevice.waitForElementToLoad(btnPopupLogin);
		btnPopupLogin.click();
	}

	public boolean isPopupForgotPasswordButtonDisplay() {
		appDevice.waitForElementToLoad(btnPopupForgotPassword);
		return btnPopupForgotPassword.isEnabled();
	}

	public void enterRegisteredEmailId(String strEmailId) {
		inputEnterEmailAddress.setValue(strEmailId);
	}

	public void clickSubmitButton() {
		appDevice.waitForElementToLoad(_btnSubmit);
		_btnSubmit.click();
	}

	public boolean isPopupEnterEmailIdDisplay() {
		appDevice.waitForElementToLoad(popupEnterEmailId);
		return popupEnterEmailId.isEnabled();
	}

	public boolean isSubmitButtonDisplay() {
		appDevice.waitForElementToLoad(_btnSubmit);
		return _btnSubmit.isEnabled();
	}

	public boolean isCancelButtonDisplay() {
		appDevice.waitForElementToLoad(_btnCancel);
		return _btnCancel.isEnabled();
	}

	public void clickCancelButton() {
		appDevice.waitForElementToLoad(_btnCancel);
		_btnCancel.click();
	}

	public void clickOkPopupButton() {
		appDevice.waitForElementToLoadWithProvidedTime(_btnOkPopUp, 3);
		System.out.println(appDevice.isElementDisplayed(_btnOkPopUp));
		if(appDevice.isElementDisplayed(_btnOkPopUp))
		{
		_btnOkPopUp.click();
		}
	}

	public boolean isImageLogoDisplay() {
		appDevice.waitForElementToLoad(imageLogo);
		return imageLogo.isEnabled();
	}

	public boolean isRegisterButtonDisplay() {
		appDevice.waitForElementToLoad(_btnRegister);
		return appDevice.isElementDisplayed(_btnRegister);
	}

	public boolean isDemoButtonDisplay() {
		appDevice.waitForElementToLoad(_linkDemo);
		return appDevice.isElementDisplayed(_linkDemo);
	}

	public boolean isEnableFaceIdDisplay() {
		appDevice.waitForElementToLoad(radioBtnEnableFaceId);
		return radioBtnEnableFaceId.isEnabled();
	}

	public boolean isKeepMeLoginDisplay() {
		appDevice.waitForElementToLoad(radioBtnKeepMeLogin);
		return radioBtnKeepMeLogin.isEnabled();
	}

	public void clickBtnViewDetails() {
		_btnViewDetails.click();
	}

	public void setUsername(String username) {
        appDevice.waitForElementToLoad(txtUserId);
		//txtUserId.click();
		inputUsername.clear();
		inputUsername.sendKeys(username);
	}

	public void setPassword(String password) {
		appDevice.waitForElementToLoad(txtPassword);
		//txtPassword.click();
		inputPassword.clear();
		inputPassword.sendKeys(password);

	}

	public void clickOnLoginButton() {
		// appDevice.waitForElementToLoad(_btnLogin);
		_btnLogin.click();
	}

	public void clickOnPerimssionAllowButton() {
		appDevice.waitForElementToLoad(_btnAllow);
		_btnAllow.click();
	}

	public void clickOnAcceptButton() {
		appDevice.waitForElementToLoad(_btnAccept);
		_btnAccept.click();
	}

	public void AcceptPopop() {
		driver.switchTo().alert().accept();
	}

	public boolean isVinDisplayed(String VIN) {
		try {
			MobileElement ele = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='VIN: " + VIN + "']"));
			return appDevice.isElementDisplayed(ele);

		} catch (Exception e) {
			return false;
		}
	}

	public void selectVehicleByVIN(String VIN) {
		for (int i = 1; i <= 10; i++) {
			System.out.println("VIN " + isVinDisplayed(VIN));
			if (!isVinDisplayed(VIN)) {
				appDevice.scrollDown();
				appDevice.sleepFor(5000);
			}

			else if (isVinDisplayed(VIN)) {
				driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='VIN: " + VIN + "']")).click();
				break;
			} else {
				System.out.println(VIN + " vin not displayed ");
			}

		}
	}

	public void clickAllAfterExitPopup() {

		driver.switchTo().alert().accept();
		appDevice.sleepFor(2000);
		Alert ele = driver.switchTo().alert();
		System.out.println(_btnAllow1d.size());
		if (_btnAllow1d.size() == 3) {
			_btnAllowUsingAppPopup.click();
		} else {

			ele.accept();
		}
		clickOkPopupButton();
		clickDismissButton();
		clickGotItButton();

	}

	public void validLogin(String username, String password, String VIN) {

			
			clickOnLoginButton();
			driver.switchTo().alert().accept();
			driver.switchTo().alert().accept();
			driver.switchTo().alert().accept();
			setUsername(username);
			setPassword(password);
			clickOnLoginButton();
			appDevice.waitForElementToLoadWithProvidedTime(_btnAccept, 2);
			System.out.println(appDevice.isElementDisplayed(_txtSelectVehicle));
			if (appDevice.isElementDisplayed(_btnAccept)) {
				_btnAccept.click();
				appDevice.waitForElementToLoadWithProvidedTime(_linkExit, 3);
				_linkExit.click();
				appDevice.sleepFor(1000);
				driver.switchTo().alert().accept();
				appDevice.sleepFor(1000);
////				Alert ele = driver.switchTo().alert();
//				System.out.println(_btnAllow1d.size());
//				if (_btnAllow1d.size() == 3) {
//					_btnAllowUsingAppPopup.click();
//				} else {
//
//				ele.accept();
//				}
//				clickOkPopupButton();
//				clickDismissButton();
				clickGotItButton();

			} else if (appDevice.isElementDisplayed(_txtSelectVehicle)) {

				selectVehicleByVIN(VIN);
				appDevice.sleepFor(1000);
				_btnAccept.click();
				appDevice.waitForElementToLoadWithProvidedTime(_linkExit, 3);
				_linkExit.click();
				appDevice.sleepFor(1000);
				driver.switchTo().alert().accept();
				System.out.println("Test1");
				appDevice.sleepFor(1000);
				Alert ele = driver.switchTo().alert();
				System.out.println(_btnAllow1d.size());
				if (_btnAllow1d.size() == 3) {
					_btnAllowUsingAppPopup.click();
				} else {

					ele.accept();
				}
				clickOkPopupButton();
				clickDismissButton();
				clickGotItButton();

			} else {
				_btnAccept.click();
				clickOkPopupButton();
				clickDismissButton();
				clickGotItButton();
			}
		System.out.println("Logged In with the user : " + username);
	}
	
	public void handelSwichVehicleWithoutAcceptPopup(String VIN) {

		appDevice.waitForElementToLoadWithProvidedTime(_btnAccept, 3);
		System.out.println(appDevice.isElementDisplayed(_txtSelectVehicle));
		if (appDevice.isElementDisplayed(_txtSelectVehicle)) {

			selectVehicleByVIN(VIN);
		}

	}

	public void handelSwichVehicle(String VIN) {

		appDevice.waitForElementToLoadWithProvidedTime(_btnAccept, 3);
		System.out.println(appDevice.isElementDisplayed(_txtSelectVehicle));
		if (appDevice.isElementDisplayed(_txtSelectVehicle)) {

			selectVehicleByVIN(VIN);
			_btnAccept.click();

		} else {

			_btnAccept.click();
		}

	}

	public void validLoginSelectDenyPopup(String username, String password, String VIN) {
			driver.switchTo().alert().dismiss();
			clickOnLoginButton();
			setUsername(username);
			setPassword(password);
			clickOnLoginButton();
			appDevice.waitForElementToLoadWithProvidedTime(_btnAccept, 2);
			if (appDevice.isElementDisplayed(_btnAccept)) {
				_btnAccept.click();
				appDevice.waitForElementToLoadWithProvidedTime(_linkExit, 3);
				_linkExit.click();
				appDevice.sleepFor(5000);
				driver.switchTo().alert().accept();
				appDevice.sleepFor(5000);
				Alert ele = driver.switchTo().alert();
				System.out.println(_btnAllow1d.size());
				if (_btnAllow1d.size() == 3) {
					_btnDontUsingAppPopup.click();
				} else {

					ele.dismiss();
				}
				clickOkPopupButton();
				clickDismissButton();
				clickGotItButton();

			} else if (appDevice.isElementDisplayed(_txtSelectVehicle)) {

				selectVehicleByVIN(VIN);
				_btnAccept.click();
				appDevice.waitForElementToLoadWithProvidedTime(_linkExit, 3);
				_linkExit.click();
				appDevice.sleepFor(5000);
				driver.switchTo().alert().accept();
				appDevice.sleepFor(5000);
				Alert ele = driver.switchTo().alert();
				System.out.println(_btnAllow1d.size());
				if (_btnAllow1d.size() == 3) {
					System.out.println("TEsting123");
					_btnDontUsingAppPopup.click();
				} else {

					ele.dismiss();
				}
				System.out.println("TEsting");
				clickOkPopupButton();
				System.out.println("TEsting456");
				clickDismissButton();
				System.out.println("TEsting789");
				clickGotItButton();

			} else {
				_btnAccept.click();
				clickOkPopupButton();
				clickDismissButton();
				clickGotItButton();
			}
		System.out.println("Logged In with the user : " + username);

	}

	public void enterLoginCredentialPerformLogin(String username, String password) {

		try {

			driver.switchTo().alert().accept();
			clickOnLoginButton();
			setUsername(username);
			setPassword(password);

			clickOnLoginButton();

			appDevice.sleepFor(15000);

		} catch (NoSuchElementException e) {
			clickOnLoginButton();
			setUsername(username);
			setPassword(password);

			clickOnLoginButton();

			appDevice.sleepFor(9000);

		}

		catch (NoAlertPresentException ex) {
			ex.getStackTrace();
		}
	}

	public void navigateTermsAndConditionsPage(String username, String password, String strVin) {

		driver.switchTo().alert().accept();
		clickOnLoginButton();
		setUsername(username);
		setPassword(password);

		clickOnLoginButton();
		
		handelSwichVehicleWithoutAcceptPopup(strVin);

		appDevice.sleepFor(15000);
		clickBtnViewDetails();

	}

	public boolean labelTermsAndConditionsDisplayed() {
		appDevice.waitForElementToLoad(_labelTermsAndConditions);
		return _labelTermsAndConditions.isDisplayed();
	}

	public boolean isMyHyundaiAlertDisplayed() {
		appDevice.sleepFor(10000);
		try {
			driver.switchTo().alert();
			return alertMyHyundaiApp.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isLabelGenesisDisplayed() {
		try {
			return labelGenesis.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public void clickCloseBtnGenesisApp() {
		btnCloseGenesisApp.click();

	}

	public void clickOpenBtnGenesisApp() {
		_btnLogin.click();
		btnOpenGenesisApp.click();

	}

	public void clickGotItButton() {
		System.out.println("TEST!@#$%");
		System.out.println(appDevice.isElementDisplayed(_btnGotIt));
		if (appDevice.isElementDisplayed(_btnGotIt)) {
			_btnGotIt.click();
		}
	}

	public void clickDismissButton() {
		System.out.println("TEST!@#$%");
		System.out.println(appDevice.isElementDisplayed(_btnDismiss));
		if (appDevice.isElementDisplayed(_btnDismiss)) {
			_btnDismiss.click();
		}
	}

	public String getRemoteSuccessMessageText(String remoteCommand) {

		String remoteMessage = "//XCUIElementTypeStaticText[contains(@name,'" + remoteCommand + "')]";
		appDevice.sleepFor(10000);
		appDevice.waitForElementToLoad(_btnOkPopUp);

		return driver.findElement(By.xpath(remoteMessage)).getText();

	}

	public boolean isDismissBtnDisplayed() {
		appDevice.waitForElementToLoad(_btnDismiss);
		return appDevice.isElementDisplayed(_btnDismiss);
	}

	public boolean isLearnMoreBtnDisplayed() {
		appDevice.waitForElementToLoad(_btnLearnMore);
		return appDevice.isElementDisplayed(_btnLearnMore);
	}

	public void clickLearenMoreButton() {
		System.out.println("TEST!@#$%");
		System.out.println(appDevice.isElementDisplayed(_btnLearnMore));
		if (appDevice.isElementDisplayed(_btnLearnMore)) {
			_btnLearnMore.click();
		}
	}

	public boolean isCovid19LabelDisplayed() {
		appDevice.waitForElementToLoad(labelCovid19Page);
		return appDevice.isElementDisplayed(labelCovid19Page);
	}

	public boolean isCovid19textDisplayed() {
		appDevice.waitForElementToLoad(txtCovid19Page);
		return appDevice.isElementDisplayed(txtCovid19Page);
	}

	public void LoginWithoutCovid19Handel(String username, String password, String VIN) {

		try {
			driver.switchTo().alert().accept();
			clickOnLoginButton();
			setUsername(username);
			setPassword(password);
			clickOnLoginButton();
			appDevice.waitForElementToLoadWithProvidedTime(_btnAccept, 2);
			System.out.println(appDevice.isElementDisplayed(_txtSelectVehicle));
			if (appDevice.isElementDisplayed(_btnAccept)) {
				_btnAccept.click();
				appDevice.waitForElementToLoadWithProvidedTime(_linkExit, 3);
				_linkExit.click();
				appDevice.sleepFor(5000);
				driver.switchTo().alert().accept();
				appDevice.sleepFor(5000);
				Alert ele = driver.switchTo().alert();
				System.out.println(_btnAllow1d.size());
				if (_btnAllow1d.size() == 3) {
					_btnAllowUsingAppPopup.click();
				} else {

					ele.accept();
				}
				clickOkPopupButton();

			} else if (appDevice.isElementDisplayed(_txtSelectVehicle)) {

				selectVehicleByVIN(VIN);
				_btnAccept.click();
				appDevice.waitForElementToLoadWithProvidedTime(_linkExit, 3);
				_linkExit.click();
				appDevice.sleepFor(5000);
				driver.switchTo().alert().accept();
				appDevice.sleepFor(5000);
				Alert ele = driver.switchTo().alert();
				System.out.println(_btnAllow1d.size());
				if (_btnAllow1d.size() == 3) {
					_btnAllowUsingAppPopup.click();
				} else {

					ele.accept();
				}
				clickOkPopupButton();

			} else {
				_btnAccept.click();
				clickOkPopupButton();
			}
		} catch (NoSuchElementException e) {
			e.getStackTrace();
		} catch (NoAlertPresentException ex) {
			ex.getStackTrace();
		}
		System.out.println("Logged In with the user : " + username);
	}

}