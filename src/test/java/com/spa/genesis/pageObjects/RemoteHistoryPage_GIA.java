package com.spa.genesis.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindBy;


public class RemoteHistoryPage_GIA {
	

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	
	private final int IMPLICIT_WAIT = 10;
	
	
	@iOSFindBy(accessibility="Remote History")
	private MobileElement _txtPageTitle;
	
	@iOSFindBy(accessibility = "OK") 
	private MobileElement _btnOKRemoteMessage;
	
	public RemoteHistoryPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	} 

	
	
	 public boolean isPresent() {
		 appDevice.sleepFor(3000);
	        return _txtPageTitle.getText().equals("Remote History"); 
	    }
	 
	 public String getMostRecentCommandName()
	 
	 {
		 appDevice.sleepFor(10000);
		 try {
		 return driver.findElement(By.xpath("(//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText)[1]")).getText();
	 }
	 catch(Exception e) {
		 if(_btnOKRemoteMessage.isDisplayed())
		 {
			 _btnOKRemoteMessage.click();
		 }
		 return driver.findElement(By.xpath("(//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText)[1]")).getText();
	 }
	 }
	 
	 public String getMostRecentCommandDate()
	 {
		 return driver.findElement(By.xpath("(//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText)[2]")).getText();
	 }
	 
	 public String getMostRecentCommandTime()
	 {
		 return driver.findElement(By.xpath("(//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText)[3]")).getText();
	 }
	 
	 public String getMostRecentFailedCommandStatus()
	 {
		 return driver.findElement(By.xpath("(//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeButton[@name='bl icon question'])[1]")).getText();
	 }
	  	
	 public String getMostRecentPendingOrSuccessCommandStatus(String commandName, String commandStatus)
	 {
		 return driver.findElement(By.xpath("(//XCUIElementTypeStaticText[@name='"+commandName+"']/following-sibling::XCUIElementTypeStaticText[@name='"+commandStatus+"'])[1]")).getText();
	 }
} 
