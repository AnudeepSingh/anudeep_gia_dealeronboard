package com.spa.genesis.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class RemoteActionsPage_GIA {
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='HORN/LIGHTS']")
	private MobileElement _btnHornLights;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='DOOR LOCKS']")
	private MobileElement _btnDoorLocks;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='START / STOP']")
	private MobileElement _btnStartStop;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='START']")
	private MobileElement _btnStart;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='STOP']")
	private MobileElement _btnStop;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='LOCK']")
	private MobileElement _btnLock;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='UNLOCK']")
	private MobileElement _btnUnlock;

	@iOSFindBy(accessibility = "FLASH LIGHTS")
	private MobileElement _btnFlashLight;

	@iOSFindBy(accessibility = "HORN & LIGHTS")
	private MobileElement _btnHornAndLights;

	@iOSFindBy(accessibility = "arrow back")
	private MobileElement arrowBack;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='remote start setting icon'])[1]")
	private MobileElement btnSettingPreset1;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='remote start setting icon'])[2]")
	private MobileElement btnSettingPreset2;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='remote start setting icon'])[3]")
	private MobileElement btnSettingPreset3;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='remote start setting icon'])[4]")
	private MobileElement btnSettingPreset4;

	@iOSFindBy(accessibility = "Set Preset Name")
	private MobileElement labelSetPresetName;
	
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Set Preset Name']/following-sibling:: XCUIElementTypeTextField")
	private MobileElement inputPresetName;
	
	@iOSFindBy(accessibility = "gen_map_detail_star")
	private MobileElement iconStarDefaultPreset;

	public RemoteActionsPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	public boolean isIconDefaultPresetDisplayed() {
		appDevice.waitForElementToLoad(iconStarDefaultPreset);
		return appDevice.isElementDisplayed(iconStarDefaultPreset);
	}

	public boolean isLblSetPrestNameDisplayed() {
		appDevice.waitForElementToLoad(labelSetPresetName);
		return appDevice.isElementDisplayed(labelSetPresetName);
	}

	public void clickOnFirstPresetssssetting() {
		appDevice.waitForElementToLoad(btnSettingPreset1);
		btnSettingPreset1.click();
	}

	public void clickOnSecondPresetssssetting() {
		appDevice.waitForElementToLoad(btnSettingPreset2);
		btnSettingPreset2.click();
	}

	public void clickOnThirdPresetssssetting() {
		appDevice.waitForElementToLoad(btnSettingPreset3);
		btnSettingPreset3.click();
	}

	public void clickOnFourthPresetssssetting() {
		appDevice.waitForElementToLoad(btnSettingPreset4);
		btnSettingPreset4.click();
	}

	public void clickOnBackArror() {
		appDevice.waitForElementToLoad(arrowBack);
		arrowBack.click();
	}

	public void clickOnHornAndLightsButton() {
		appDevice.waitForElementToLoad(_btnHornAndLights);
		_btnHornAndLights.click();
	}

	public void clickOnFlashLightsButton() {
		appDevice.waitForElementToLoad(_btnFlashLight);
		_btnFlashLight.click();
	}

	public void clickOnUnlockButton() {
		appDevice.waitForElementToLoad(_btnUnlock);
		_btnUnlock.click();
	}

	public void clickOnLockButton() {
		appDevice.waitForElementToLoad(_btnLock);
		_btnLock.click();
	}

	public void clickOnStopButton() {
		appDevice.waitForElementToLoad(_btnStop);
		_btnStop.click();
	}

	public void clickOnStartButton() {
		appDevice.waitForElementToLoad(_btnStart);
		_btnStart.click();
	}

	public void clickOnHornLightsButton() {
		appDevice.waitForElementToLoad(_btnHornLights);
		_btnHornLights.click();
	}

	public void clickOnDoorLocksButton() {
		appDevice.waitForElementToLoad(_btnDoorLocks);
		_btnDoorLocks.click();
	}

	public void clickOnStartStopButton() {
		appDevice.waitForElementToLoad(_btnStartStop);
		_btnStartStop.click();
	}

}
