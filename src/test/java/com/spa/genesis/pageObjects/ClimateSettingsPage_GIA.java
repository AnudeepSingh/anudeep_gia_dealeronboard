package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class ClimateSettingsPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "REMOTE FEATURES")
	private MobileElement labelRemoteFeature;

	@iOSFindBy(accessibility = "(//XCUIElementTypeStaticText[@name='TEMPERATURE']/following-sibling::XCUIElementTypeOther //XCUIElementTypeTable)[1]")
	private MobileElement _elementTemperatureScroll;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='FRONT DEFROSTER']/preceding-sibling::XCUIElementTypeSwitch)[last()]")
	private MobileElement switchFrontDefroster;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='TEMPERATURE']/preceding-sibling::XCUIElementTypeSwitch)[last()]")
	private MobileElement _switchTemperature;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Submit']")
	private MobileElement _btnSubmit;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOKRemoteMessage;
	
	@iOSFindBy(accessibility="icnCollapsed down")
	private MobileElement _accordianEngineDuration;
	
	@iOSFindBy(xpath="//XCUIElementTypeStaticText[@name='10']")
	private MobileElement _elementEngineScroll;
	
	
	@iOSFindBy(xpath="//XCUIElementTypeStaticText[@name='Front Defroster']/following-sibling:: XCUIElementTypeSwitch")
	private MobileElement _switchFrontDefroster;
	
	@iOSFindBy(xpath="//XCUIElementTypeStaticText[@name='Heated Features']/following-sibling:: XCUIElementTypeSwitch")
	private MobileElement _switchHeatedFeatures;
	
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Start vehicle without presets']/parent::XCUIElementTypeOther/preceding-sibling:: XCUIElementTypeButton)[last()]")
	private MobileElement btnStartVehicleWithoutPresets;
	
	@iOSFindBy(accessibility = "START VEHICLE NOW")
	private MobileElement btnStartVehicleNow;

	public ClimateSettingsPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isSwitchDefrostDisplay() {
		appDevice.waitForElementToLoad(switchFrontDefroster);

		return switchFrontDefroster.isDisplayed();

	}

	public boolean isRemoteFeaturePageDisplay() {
		appDevice.waitForElementToLoad(labelRemoteFeature);

		return labelRemoteFeature.isDisplayed();

	}

	public void setTemperatureScrollValue(String temperature) {

		driver.findElementByAccessibilityId(temperature).click();

	}

	public void clickDefrostSwitch() {
		appDevice.waitForElementToLoad(switchFrontDefroster);
		switchFrontDefroster.click();
	}

	public void setStartSettingsStates(String frontDefrosterToggle, String temperature) {

		if (!(switchFrontDefroster.getAttribute("value").equals(frontDefrosterToggle))) {
			clickDefrostSwitch();
		}

		if (!(_switchTemperature.getAttribute("value").equals(temperature))) {
			clickTemperatureSwitch();
		}
	}

	public void clickOnRemoteCommandOKButton() {
		appDevice.waitForElementToLoad(_btnOKRemoteMessage);
		_btnOKRemoteMessage.click();
	}
	
	public void clickOnbtnbtnStartVehicleNow() {
		appDevice.waitForElementToLoad(btnStartVehicleNow);
		btnStartVehicleNow.click();
	}
	public void clickOnbtnStartVehicleWithoutPresets() {
		appDevice.waitForElementToLoad(btnStartVehicleWithoutPresets);
		btnStartVehicleWithoutPresets.click();
	}
	
	public void clickEngineDurationAccordian()
	{
		appDevice.waitForElementToLoad(_accordianEngineDuration);
		_accordianEngineDuration.click();
	}
	
	public void clickTemperatureSwitch()
	{
		appDevice.waitForElementToLoad(_switchTemperature);
		_switchTemperature.click();
	}
	
	public void clickFrontDefrosterSwitch()
	{
		appDevice.waitForElementToLoad(_switchFrontDefroster);
		_switchFrontDefroster.click();
	}
	
	public void clickHeatedFeaturesSwitch()
	{
		appDevice.waitForElementToLoad(_switchHeatedFeatures);
		_switchHeatedFeatures.click();
	}
	
	public void clickSubmitButton()
	{
		appDevice.waitForElementToLoad(_btnSubmit);
		_btnSubmit.click();
	}
	
	public void setStartSettingsStates(String temperatureToggle, String frontDefrosterToggle, String heatedFeaturesToggle){
		
				
        if(!(_switchTemperature.getAttribute("value").equals(temperatureToggle))){
        	clickTemperatureSwitch();
        }

        if(!(_switchFrontDefroster.getAttribute("value").equals(frontDefrosterToggle))){
        	clickFrontDefrosterSwitch();
        }

        if(!(_switchHeatedFeatures.getAttribute("value").equals(heatedFeaturesToggle))){
        	clickHeatedFeaturesSwitch();
        }
        
       // clickSubmitButton();
    }
	
	public void swipeEngineScrollWheelUp(int entriesToScroll)
	{
		Rectangle dimensions=_elementEngineScroll.getRect();
		
		int xCoordinate = dimensions.x + (int)(dimensions.width * 0.25);
        int yCoordinateStart = dimensions.y + (int)(dimensions.height * 0.25);
        int yCoordinateEnd = dimensions.y + (int)(dimensions.height * 0.50);
        
        PointOption pointStart = new PointOption().withCoordinates(xCoordinate, yCoordinateStart);
        PointOption pointEnd = new PointOption().withCoordinates(0, yCoordinateEnd);
        WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));

        
        TouchAction touchAction = new TouchAction(driver);

        for (int i = 0; i < entriesToScroll; i++) {
            touchAction
                    .press(pointStart)
                    .waitAction(waitOptions)
                    .moveTo(pointEnd)
                    .release()
                    .perform();
            try {
                Thread.sleep(250);
            } catch (InterruptedException ie) {
                //
            }
        }
	}

}
