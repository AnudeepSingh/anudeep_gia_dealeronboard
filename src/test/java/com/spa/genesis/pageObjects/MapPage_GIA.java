package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;


import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import freemarker.core._TemplateModelException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;

public class MapPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	// Map Options

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='FAVORITES']")
	private MobileElement tabFavorites;

	@AndroidFindBy(id="com.stationdm.genesis:id/home_map_fifth_bar")
	@iOSFindBy(accessibility = "icn Dealer default")
	private MobileElement _linkHyundaiDealers;

	@iOSFindBy(accessibility = "icn Gas default")
	private MobileElement _linkNerebyGas;

	@iOSFindBy(accessibility = "icn FMC default")
	private MobileElement _linkCarFinder;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[contains(@name,'sdm popupmenu map')]")
	private MobileElement _popUpMenuMap;

	@AndroidFindBy(id="com.stationdm.genesis:id/title_left_icon")
	@iOSFindBy(accessibility = "arrow back")
	private MobileElement _btnBack;

	@AndroidFindBy(id="com.stationdm.genesis:id/map_search_et")
	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='map_img_search']/following-sibling::XCUIElementTypeTextField")
	private MobileElement _searchBar;

	@AndroidFindBy(id="com.stationdm.genesis:id/map_search_icon")
	@iOSFindBy(accessibility = "sdm search icn")
	private MobileElement _btnSearch;

	@iOSFindBy(accessibility = "icn POI default")
	private MobileElement _linkSearchPoi;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[starts-with(@name,'No')]")
	private MobileElement _txtNotfound;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	@iOSFindBy(accessibility = "sdm btn share default")
	private MobileElement _linkShare;

	@iOSFindBy(accessibility = "map icn favorite desel")
	private MobileElement _linkSave;

	@iOSFindBy(accessibility = "btn poicall default")
	private MobileElement _linkPoiCall;

	@iOSFindBy(accessibility = "btn sendtocarsmall default")
	private MobileElement btnSendToCarIcon;

	@iOSFindBy(accessibility = "//XCUIElementTypeStaticText[@name='SEND TO CAR']")
	private MobileElement _linkSendToCar;

	@AndroidFindBy(id="marker_detail_arrow")
	@iOSFindBy(accessibility = "map icn arrow up")
	private MobileElement _tooltipArrowUp;

	@iOSFindBy(accessibility = "map icn arrow down")
	private MobileElement _tooltipArrowDown;

	@iOSFindBy(accessibility = "POI successfully added.")
	private MobileElement _txtPOISuccessfullyAdded;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'This location has already been saved to your favorites.')]")
	private MobileElement _txtPOIAlreadySaved;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='OK']/ancestor::XCUIElementTypeAlert//XCUIElementTypeStaticText")
	private MobileElement _labelMesssage;

	@iOSFindBy(accessibility = "btn map list default")
	private MobileElement _poiViewList;

	@iOSFindBy(accessibility = "sdm btn delete default")
	private MobileElement _linkDelete;

	@iOSFindBy(accessibility = "map new car finder")
	private MobileElement iconMapNewCarFinder;

	@iOSFindBy(accessibility = "btn centermap default")
	private MobileElement iconMapGPSIcon;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='map icn arrow up']/following-sibling::XCUIElementTypeStaticText)[2]")
	private MobileElement txtMapLocationAddress;

	@iOSFindBy(xpath = "//XCUIElementTypeAlert[@name='Location Services Disabled']")
	private MobileElement _alertLocationServiceDisabled;

	@iOSFindBy(accessibility = "Cancel")
	private MobileElement buttonCancelAlert;

	@iOSFindBy(accessibility = "Favorites")
	private MobileElement linkFavorites;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='map icn arrow up']/following-sibling:: XCUIElementTypeStaticText)[1]")
	private MobileElement labelAddress;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='SEND TO CAR']")
	private MobileElement btnSendToCar;

	@iOSFindBy(accessibility = "POI successfully sent to Genesis.")
	private MobileElement labelSuccessfullySentToCar;

	@iOSFindBy(accessibility = "DEALER LOCATOR")
	private MobileElement _mapDealerLocator;

	@iOSFindBy(accessibility = "PLACES OF INTEREST")
	private MobileElement _mapFavourites;

	@iOSFindBy(accessibility = "GAS STATION")
	private MobileElement _mapGasStation;

	@iOSFindBy(accessibility = "POI SEARCH")
	private MobileElement _mapPoiSearch;

	@AndroidFindBy(id="marker_detail_middle")
	@iOSFindBy(accessibility = "btn setgdealer default")
	private MobileElement _mapSetAsPreferred;

	
	@iOSFindBy(accessibility = "Your preferred Genesis Dealer Service Center has been updated.")
	private MobileElement labelPreferredGenesisDealerSuccess;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='map img navigation']/following-sibling:: XCUIElementTypeStaticText[contains(@name,'mi')]")
	private MobileElement btnMapNavigationIcon;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Do you want to call this phone number')]")
	private MobileElement popupCalltext;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Call']")
	private MobileElement btnCallPopup;

	@iOSFindBy(accessibility = "icn_hours")
	private MobileElement iconHours;

	@iOSFindBy(accessibility = "Shop Accessories")
	private MobileElement linkShopAccessories;
	
	@AndroidFindBy(id="com.stationdm.genesis:id/title_title")
	@iOSFindBy(accessibility = "SCHEDULE SERVICE")
	private MobileElement pageScheduleService;
	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='SCHEDULE SERVICE']")
	private MobileElement btnScheduleService;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@resource-id='com.stationdm.genesis:id/marker_detail_title']")
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='map icn arrow down']/following-sibling::XCUIElementTypeStaticText)[1]")
	private MobileElement txtDealerName;
	
	@AndroidFindBy(xpath = "//android.view.View[@resource-id='ocas-header']|//android.view.View[@text='SCHEDULE A SERVICE APPOINTMENT']|//android.view.View[@resource-id='root']//android.widget.TextView|//android.view.View[@text='SCHEDULE A SERVICE CHECK IN']|//android.view.View[@resource-id='ext-element-31']|//android.view.View[@text='SCHEDULE A SERVICE APPOINTMENT NORTH PALM HYUNDAI']")
	@iOSFindBy(xpath = "(//XCUIElementTypeOther[@name='banner']//XCUIElementTypeStaticText)[1]|//XCUIElementTypeOther[@name='navigation']/XCUIElementTypeButton/following-sibling::XCUIElementTypeLink|(//XCUIElementTypeOther[@name='ServiceEdge Appointments Mobile']//XCUIElementTypeStaticText)[1]|//XCUIElementTypeStaticText[contains(@name,'SCHEDULE A SERVICE')]")
	private MobileElement txtDealerNameOnScheduleServicePage;
	
	@AndroidFindBy(xpath = "//android.view.View[@resource-id='ocas-header']|//android.view.View[@text='SCHEDULE A SERVICE APPOINTMENT']|//android.view.View[@resource-id='root']//android.widget.TextView|//android.view.View[@text='SCHEDULE A SERVICE CHECK IN']|//android.view.View[@resource-id='ext-element-31']|//android.view.View[@text='SCHEDULE A SERVICE APPOINTMENT NORTH PALM HYUNDAI']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='navigation']/XCUIElementTypeButton/following-sibling::XCUIElementTypeLink|//XCUIElementTypeOther[@name='Services Page']//XCUIElementTypeOther[@name='banner']|//XCUIElementTypeOther[@name='Start Page']//XCUIElementTypeOther[@name='banner']|//XCUIElementTypeOther[@name='ServiceEdge Appointments Mobile']|//XCUIElementTypeStaticText[contains(@name,'SCHEDULE A SERVICE')]")
	private MobileElement screenScheduleServicePage2;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@resource-id='android:id/message']|//android.view.View[@text='Webpage not available']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='Error - CDK']|//XCUIElementTypeWebView//XCUIElementTypeStaticText[contains(@name,'schedulingUrl')]")
	private MobileElement errorPageSchedulePage;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@resource-id='android:id/message']|//android.view.View[@text='Webpage not available']")
	@iOSFindBy(xpath = "(//XCUIElementTypeOther[@name='Error - CDK']/XCUIElementTypeOther)[1]|//XCUIElementTypeWebView//XCUIElementTypeStaticText[contains(@name,'schedulingUrl')]")
	private MobileElement txtNetworkIssue;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='main']/XCUIElementTypeOther[@name='SERVICE VALET']")
	private MobileElement ScreenValetSchedulePage;

	public MapPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	public boolean isPageScheduleServiceDisplay() {
		appDevice.waitForElementToLoad(pageScheduleService);
		return appDevice.isElementDisplayed(pageScheduleService);
	}
	
	public void clickBtnScheduleService() {
		appDevice.waitForElementToLoad(btnScheduleService);
		btnScheduleService.click();
	}

	public boolean isAddressDisplay() {
		appDevice.waitForElementToLoad(labelAddress);
		return appDevice.isElementDisplayed(labelAddress);
	}

	public String getSetAsPreferedEnabledState() {
		appDevice.waitForElementToLoad(_mapSetAsPreferred);
		return _mapSetAsPreferred.getAttribute("enabled");
	}

	public void clickShopAccessoriesLink() {
		appDevice.waitForElementToLoad(linkShopAccessories);
		linkShopAccessories.click();
	}

	public boolean isIconHoursDisplay() {
		appDevice.waitForElementToLoad(iconHours);
		return appDevice.isElementDisplayed(iconHours);
	}

	public boolean isBtnCallPopupDisplay() {
		appDevice.waitForElementToLoad(btnCallPopup);
		return appDevice.isElementDisplayed(btnCallPopup);
	}

	public boolean isBtnMapNavigationIconDisplay() {
		appDevice.waitForElementToLoad(btnMapNavigationIcon);
		return appDevice.isElementDisplayed(btnMapNavigationIcon);
	}

	public void clickDealersLocatorOption() {
		appDevice.sleepFor(1000);
		_linkHyundaiDealers.click();
	}

	public void clickCarFinderOption() {
		appDevice.waitForElementToLoad(_linkCarFinder);
		_linkCarFinder.click();

	}

	public void clickNearByGas() {
		_linkNerebyGas.click();

	}

	public void clickOnBackButton() {
		_btnBack.click();
	}

	public void navigateToFavorites() {
		clickPOISearch();
		tabFavorites.click();
		appDevice.sleepFor(10000);

	}

	public boolean txtNotfoundPopupDisplay() {
		try {
			return _txtNotfound.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}

	}

	public void navigateToHyundaiDealer() {
		_linkHyundaiDealers.click();
		appDevice.sleepFor(10000);
		if (txtNotfoundPopupDisplay()) {
			_btnOkPopUp.click();
		}
	}

	public void navigateToNearbyGas() {
		_linkNerebyGas.click();
		appDevice.sleepFor(10000);
		if (_txtNotfound.isDisplayed()) {
			_btnOkPopUp.click();
		}
	}

	@SuppressWarnings("deprecation")
	public void enterSearchText(String zip) {
		
		_searchBar.sendKeys(zip);
		_searchBar.click();
		appDevice.sleepFor(4000);
		 new TouchAction(driver).tap(PointOption.point(916,2046)).release().perform();
//		if (appDevice.isElementDisplayed(_btnOkPopUp)) {
//			_btnOkPopUp.click();
//		}
		//appDevice.sleepFor(2000);

	}

	public String searchResultGetText(String searchText) {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[contains(@name,'" + searchText + "')]"))
				.getText();
	}

	public String clickOnSaveIcon() {
		_tooltipArrowUp.click();
		_linkSave.click();
		appDevice.sleepFor(10000);

		if (_btnOkPopUp.isDisplayed()) {
			String strMsg = _labelMesssage.getText();
			_btnOkPopUp.click();
			return strMsg;
		} else {
			return null;
		}
	}

	public boolean clickSavedPOI(String savedPOIText) {
		_poiViewList.click();

		MobileElement parent = appDevice.getDriver().findElement(By.xpath("//XCUIElementTypeTable"));
		// identifying the parent Table
		String parentID = parent.getId();
		System.out.println("ID " + parentID);
		HashMap<String, String> scrollObject = new HashMap<>();
		scrollObject.put("element", parentID);
		scrollObject.put("direction", "down");

		boolean flag = false;
		for (int i = 0; i <= 20; i++) {
			MobileElement poiName = appDevice.getDriver()
					.findElement((By.xpath("//XCUIElementTypeStaticText[@name='" + savedPOIText + "']")));
			if (poiName.isDisplayed()) {
				poiName.click();
				flag = true;
				break;

			} else {
				appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
			}

		}

		_tooltipArrowUp.click();
		appDevice.sleepFor(5000);
		_linkSave.isDisplayed();
		_linkSave.click();
		appDevice.sleepFor(5000);
		_btnOkPopUp.click();

		return flag;
	}

	public boolean isIconMapNewCarFinderDisplay() {

		return iconMapNewCarFinder.isDisplayed();

	}

	public boolean isMapGpsIconDisplay() {

		return iconMapGPSIcon.isDisplayed();

	}

	public String getTxtDisplayedUnderSearchTextBox() {
		return _searchBar.getAttribute("value");
	}

	public boolean verifyAddressInExpandableList(String savedPOIText) {
		_poiViewList.click();

		MobileElement parent = appDevice.getDriver().findElement(By.xpath("//XCUIElementTypeTable"));
		// identifying the parent Table
		String parentID = parent.getId();
		System.out.println("ID " + parentID);
		HashMap<String, String> scrollObject = new HashMap<>();
		scrollObject.put("element", parentID);
		scrollObject.put("direction", "down");

		boolean flag = false;
		for (int i = 0; i <= 50; i++) {
			try {
				MobileElement poiName = appDevice.getDriver()
						.findElement((By.xpath("//XCUIElementTypeStaticText[contains(@name,'" + savedPOIText + "')]")));
				System.out.println(appDevice.isElementDisplayed(poiName));
				if (appDevice.isElementDisplayed(poiName)) {
					poiName.click();
					flag = true;
					break;

				} else {
					appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
				}
			} catch (NoSuchElementException ex) {
				appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
			}

		}

		return flag;
	}

	public boolean verifyFullAddressInExpandableList(String savedPOIText, String strAddressLabel) {
		_poiViewList.click();

		MobileElement parent = appDevice.getDriver().findElement(By.xpath("//XCUIElementTypeTable"));
		// identifying the parent Table
		String parentID = parent.getId();
		HashMap<String, String> scrollObject = new HashMap<>();
		HashMap<String, String> scrollObjectUp = new HashMap<>();
		scrollObject.put("element", parentID);
		scrollObject.put("direction", "down");

		boolean flag = false;
		for (int i = 0; i <= 10; i++) {
			try {

				MobileElement poiName = appDevice.getDriver()
						.findElement((By.xpath("//XCUIElementTypeTable//XCUIElementTypeStaticText[@name='"
								+ strAddressLabel + "']/following-sibling::XCUIElementTypeStaticText[contains(@name,'"
								+ savedPOIText + "')]")));

				System.out.println(appDevice.isElementDisplayed(poiName));

				if (appDevice.isElementDisplayed(poiName)) {
					poiName.click();
					flag = true;
					break;

				} else {
					appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
				}
			} catch (NoSuchElementException ex) {
				appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
			}

		}

		return flag;
	}

	public String getMapLocationAddress() {
		appDevice.waitForElementToLoad(txtMapLocationAddress);
		return txtMapLocationAddress.getAttribute("value");
	}

	public boolean isSearchBarDisplay() {
		appDevice.waitForElementToLoad(_searchBar);
		return _searchBar.isDisplayed();

	}

	public boolean isAlertLocationServiceDisabledDisplay() {

		appDevice.waitForElementToLoadWithProvidedTime(buttonCancelAlert, 3);
		driver.switchTo().alert();
		return appDevice.isElementDisplayed(_alertLocationServiceDisabled);

	}

	public void clickCancelAlertButton() {
		buttonCancelAlert.click();
	}

	public void clickGPSIcon() {
		iconMapGPSIcon.click();

	}

	public String getMapLocationAddressLabel() {
		return labelAddress.getAttribute("value");
	}

	public void clickFavorites() {
		linkFavorites.click();
	}

	public void clickSendToCar() {
		appDevice.sleepFor(60000);
		appDevice.waitForElementToLoad(btnSendToCar);
		btnSendToCar.click();
	}

	public boolean isPoiSuccessfullySentToCar() {
		appDevice.waitForElementToLoad(labelSuccessfullySentToCar);
		boolean b = appDevice.isElementDisplayed(labelSuccessfullySentToCar);
		_btnOkPopUp.click();
		return b;
	}

	public void clickPOISearch() {
		appDevice.waitForElementToLoad(_mapPoiSearch);
		_linkSearchPoi.click();
	}

	public boolean isDealerLocatorPageDisplayed() {
		appDevice.waitForElementToLoad(_mapDealerLocator);
		return _mapDealerLocator.isDisplayed();
	}

	public boolean isGasStationPageDisplayed() {
		appDevice.sleepFor(5000);
		appDevice.waitForElementToLoad(_mapGasStation);
		return _mapGasStation.isDisplayed();
	}

	public boolean isFavoritesPageDisplayed() {
		appDevice.sleepFor(5000);
		appDevice.waitForElementToLoad(_mapFavourites);
		return _mapFavourites.isDisplayed();
	}

	public boolean isPoiSearchPageDisplayed() {
		appDevice.waitForElementToLoad(_mapPoiSearch);
		return _mapPoiSearch.isDisplayed();
	}

	public void clickDownArrow() {
		_tooltipArrowDown.click();
	}

	public void clickUpArrow() {
		//appDevice.waitForElementToLoad(_tooltipArrowUp);
		_tooltipArrowUp.click();
	}

	public void clickSetAsPreferred() {
		//appDevice.waitForElementToLoad(_mapSetAsPreferred);
		_mapSetAsPreferred.click();
	}

	public boolean islabelPreferredGenesisDealerSuccess() {
		appDevice.waitForElementToLoad(labelPreferredGenesisDealerSuccess);
		boolean b = appDevice.isElementDisplayed(labelPreferredGenesisDealerSuccess);
		System.out.println(b);
		//_btnOkPopUp.click();
		return b;
	}
	
	public String getDealerName() {
		appDevice.waitForElementToLoad(txtDealerName);
		return txtDealerName.getText();
	}
	
	public String getScheduleServiceNameFromScheduleServicePage() {
		return txtDealerNameOnScheduleServicePage.getText();
	}
	
	public boolean isScreenScheduleServicePage2Displayed() {
		appDevice.sleepFor(1000);
		appDevice.waitForElementToLoadWithProvidedTime(screenScheduleServicePage2,4);
		System.out.println("Schedule Page Status" +appDevice.isElementDisplayed(screenScheduleServicePage2));
		return appDevice.isElementDisplayed(screenScheduleServicePage2);

	}
	
	public boolean isErrorScheduleServicePageDisplayed() {
		appDevice.waitForElementToLoadWithProvidedTime(errorPageSchedulePage, 3);
		return appDevice.isElementDisplayed(errorPageSchedulePage);

	}
	
	public String getNetworkIssueText() {
		return txtNetworkIssue.getText();
	}
}
