package com.spa.scheduleServiceUI.test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.auto.framework.base.Log;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.EnterPinPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;
import com.spa.hyundai.pageObjects.CarCarePage;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.DealerLocatorPage;
import com.spa.hyundai.pageObjects.EnterPinPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.MapPage;
import com.spa.hyundai.pageObjects.MenuPage;
import com.spa.hyundai.pageObjects.SettingsPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class scheduleServiceUIGIATest extends BaseTest {
	private String strVehicleType, strTestRun, strPin = "";

	String strEmail = "dh95@mailnesia.com";
	String strPassword = "test1111";
	String strVin = "KMHGN4JE9HU170195";

	String strSNo, strDealerCd, strZipCode, strDealerNm, strVendorName = null;

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static EnterPinPage_GIA enterPinPage;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MapPage_GIA mapPage;

	String sheetName = null;
	String strStatus = "Execution Not complete";
	String actualDealerName = null;
	String actualDealerName_SchedulePage = "";
	String strComments = "";
	String errorMessageOrSchedulePageType = "";

	int serialColNo = 0;
	int dealerCodeColNo = 1;
	int dealerNameColNo = 2;
	int vendorNameColNo = 3;
	int zipCodeColNo = 4;
	int actualDealerNameColNo = 5;
	int actualDealerName_SchedulePageColNo = 6;
	int statusColNo = 7;
	int executionTimeColNo = 8;
	int commentsColNo = 9;
	int errorMessageCol = 10;

	@Factory(dataProvider = "userDataProvider")
	public scheduleServiceUIGIATest(String strSNo, String strDealerCd, String strZipCode, String strDealerNm,
			String strVendorName) {
		this.strSNo = strSNo;
		this.strDealerCd = strDealerCd;
		this.strZipCode = strZipCode;
		System.out.println("Zip Code " + strZipCode);
		this.strDealerNm = strDealerNm;
		this.strVendorName = strVendorName;

	}

	@DataProvider(name = "userDataProvider")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTabArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/DealerOnboarding_20200407.xlsx",
				"Sheet6");
	}

	@BeforeSuite(alwaysRun = true)
	public void beforeClass() throws IOException, Exception {
		Thread.sleep(2000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		driver = empAppDevice.getDriver();
		enterPinPage = new EnterPinPage_GIA(empAppDevice);
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
		System.out.println(strEmail);
	}

	public void relaunchAppAndInitializedVariable() throws Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		driver = empAppDevice.getDriver();
		enterPinPage = new EnterPinPage_GIA(empAppDevice);
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
	}

	public void loginConditions() throws Exception {
		try {
			System.out.println("Logged In using : " + strEmail);
			//System.out.println(dashboardPage.isUnlockButtonDisplayWithinExpectedTime(2));

			if (!dashboardPage.isUnlockButtonDisplayWithinExpectedTime(2)) {
				loginPage.validLogin(strEmail, strPassword, strVin);
				//System.out.println(dashboardPage.isUnlockButtonDisplayWithOutHandelException());
			} else {
				//System.out.println("Already Login with " + strEmail);
			}
		} catch (NoSuchElementException ex) {
			//System.out.println("Logged In Again Due To No Such Element Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		} catch (WebDriverException e) {
			System.out.println("Logged In Again Due To Wed Driver Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		}

	}

	@Test(description = "Search dealer POI by passing zipcode as search parameter and set poi as prefferd location and then perform send to car")
	public void testSearchDealerPOI() throws Exception

	{
		// Fetching integer from dealer code for Hyundai and Genesis URL selection
		int dealerCdLength = strDealerCd.length();
		int dealerCdLastLength = dealerCdLength - 3;
		String codeDealer = strDealerCd.substring(dealerCdLastLength);
		System.out.println("Integer Value Of DealerCd " + codeDealer);
		// Compare Dealer Code interger for comparison
		if (Integer.parseInt(codeDealer) < 701) {
			sheetName = "Sheet1";
		} else {
			sheetName = "Marchrunoutput";
		}

		boolean flag1 = false;
		boolean flag2 = false;

		loginConditions();
		dashboardPage.isUnlockButtonDisplay();
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickDealersLocatorOption();

		//mapPage.isSearchBarDisplay();

		strZipCode = strZipCode.replace(".0", "");

		if (strZipCode.length() != 5) {
			strZipCode = 0 + strZipCode;
		}

		mapPage.enterSearchText(strZipCode);
		mapPage.clickUpArrow();
		mapPage.clickSetAsPreferred();

		System.out.println("Zip Code " + strZipCode);

		//Assert.assertTrue(mapPage.islabelPreferredGenesisDealerSuccess(),	"Preferred genesis dealer service center has been updated popup not appear");

		actualDealerName = mapPage.getDealerName();
			if (!actualDealerName.equalsIgnoreCase(strDealerNm)) {
			strComments = "Dealer Name From Dealer Detail Not Matched";
		}

		mapPage.clickOnBackButton();
		dashboardPage.isUnlockButtonDisplay();
		dashboardPage.clickSchedule();

		if (dashboardPage.isBtnDropOffDisplay()) {
			dashboardPage.clickBtnDropOff();
		}

		if (mapPage.isPageScheduleServiceDisplay()) {
			flag2 = mapPage.isScreenScheduleServicePage2Displayed();
			// flag1 = mapPage.isScheduleservicePageVisible();
			if (flag2) {
				strStatus = "Pass";
				actualDealerName_SchedulePage = mapPage.getScheduleServiceNameFromScheduleServicePage();

//				System.out.println(actualDealerName_SchedulePage);

	//			System.out.println("Schedule service Page Visible With" + actualDealerName_SchedulePage);

				if (actualDealerName_SchedulePage.contains("SCHEDULE A SERVICE CHECK IN")
						|| actualDealerName_SchedulePage.contains("SCHEDULE A SERVICE APPOINTMENT")
						|| actualDealerName_SchedulePage.contains("West Broad Hyundai")) {
					errorMessageOrSchedulePageType = actualDealerName_SchedulePage + " Page Is Displayed";
					actualDealerName_SchedulePage = "";
				}

				else if (!actualDealerName_SchedulePage.toLowerCase().contains(strDealerNm.toLowerCase())) {
					strComments = strComments + " Dealer Name From Schedule Page Not Matched";
				}

			} else {
				strStatus = "Fail";
				if (mapPage.isErrorScheduleServicePageDisplayed()) {
					errorMessageOrSchedulePageType = mapPage.getNetworkIssueText();
				}
			}
			mapPage.clickOnBackButton();

			Assert.assertTrue(flag2, "Schedule Service Page not displayed");
		} else {

			strStatus = "No Schedule Service Page Display";
			errorMessageOrSchedulePageType = dashboardPage.getPopupMessage();
			dashboardPage.clickCancelButton();
		}
	}

	@AfterMethod
	public void WriteExcel() throws Exception {

		//System.out.println("TestComplete");

		String strFilePath = System.getProperty("user.dir")
				+ "/src/test/resources/TestingData/DealerOnboarding_20200407.xlsx";

		ExcelReader excelReader = new ExcelReader();
		//System.out.println(strSNo);
		int rowNum = Integer.parseInt(strSNo.replace(".0", ""));

		//System.out.println(strFilePath);
		//System.out.println(rowNum);
		//System.out.println(sheetName);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, serialColNo, String.valueOf(rowNum));
		excelReader.writeExcel(strFilePath, sheetName, rowNum, dealerCodeColNo, strDealerCd);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, dealerNameColNo, strDealerNm);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, vendorNameColNo, strVendorName);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, zipCodeColNo, strZipCode.replace(".0", ""));
		excelReader.writeExcel(strFilePath, sheetName, rowNum, actualDealerNameColNo, actualDealerName);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, actualDealerName_SchedulePageColNo,
				actualDealerName_SchedulePage);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, statusColNo, strStatus);

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		//System.out.println(formatter.format(date));
		excelReader.writeExcel(strFilePath, sheetName, rowNum, executionTimeColNo, formatter.format(date).toString());
		excelReader.writeExcel(strFilePath, sheetName, rowNum, commentsColNo, strComments);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, errorMessageCol, errorMessageOrSchedulePageType);
		excelReader.closeWorkBook();

	}

}
