package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class VehicleStatusPage {
	
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	


	private final int IMPLICIT_WAIT = 10;
	
	
	
	@iOSFindBy(accessibility="Vehicle Status")
	private MobileElement _txtTitle;
	
	@iOSFindBy(xpath="//XCUIElementTypeStaticText[contains(@name,'Last Updated')]")
	private MobileElement _txtLastUpdated;
	
	@iOSFindBy(accessibility="bl vs refresh but")
	private MobileElement _btnRefresh;
	
	public VehicleStatusPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	public boolean isPresent()
	{
		appDevice.waitForElementToLoad(_txtTitle);
		return _txtTitle.getText().equals("Vehicle Status");
	}

	
	public String getLastUpdatedText()
	{
		appDevice.waitForElementToLoad(_txtLastUpdated);
		return _txtLastUpdated.getText();
	}
	
	public void clickOnRefreshButton()
	{
		appDevice.waitForElementToLoad(_btnRefresh);
		_btnRefresh.click();
	}
	
	public boolean isVehicleStatusDisplayed()
	{
		return _txtLastUpdated.isDisplayed();
	}
}
