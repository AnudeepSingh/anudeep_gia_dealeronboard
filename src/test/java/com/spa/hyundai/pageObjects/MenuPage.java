package com.spa.hyundai.pageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class MenuPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private static SwitchVehiclePage switchVehiclePage;

	private final int IMPLICIT_WAIT = 10;

	
	// Menu option locators

	@iOSFindBy(accessibility = "Alert Settings")
	private MobileElement _linkAlertSettings;

	@iOSFindBy(accessibility = "Schedule Service")
	private MobileElement _linkScheduleServices;

	@iOSFindBy(accessibility = "Accessories")
	private MobileElement _linkAccessories;

	@iOSFindBy(accessibility = "Dealer Locator")
	private MobileElement _linkDealerLocator;

	@iOSFindBy(accessibility = "Call Roadside")
	private MobileElement _linkCallRoadside;

	@iOSFindBy(accessibility = "About & Support")
	private MobileElement _linkAboutAndSupport;

	@iOSFindBy(accessibility = "Settings")
	private MobileElement _linkSettings;

	@iOSFindBy(accessibility = "Switch Vehicle")
	private MobileElement _linkSwitchVehicle;

	@iOSFindBy(accessibility = "Logout")
	private MobileElement _linkLogout;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='OK']")
	private MobileElement _btnLogOutOk;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='sdm navibar menu'])[2]")
	private MobileElement _btnCloseMenuPanel;
	
	@iOSFindBy(accessibility = "sdm navibar menu")
	private MobileElement _btnMenu;

	@iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='MyHyundai']//XCUIElementTypeTable")
	private MobileElement _panelMenu;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Sign Out']")
	private MobileElement labelSignOut;
	
	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;
	

	public MenuPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
		switchVehiclePage = new SwitchVehiclePage(empAppDevice);
	}


	public void clickSignOut() {
		labelSignOut.click();

	}
	
	public void navigateToAlertSettings() {
		_linkAlertSettings.click();

	}

	public void navigateToScheduleServices() {
		_linkScheduleServices.click();
	}

	public void navigateToAccessories() {
		_linkAccessories.click();
	}

	public void navigateToDealerLocator() {
		_linkDealerLocator.click();
	}

	public void navigateToAboutAndSupport() {
		_linkAboutAndSupport.click();
	}

	public void navigateToSettings() {
		_linkSettings.click();
	}

	public void navigateToSwitchVehicle() {
		_linkSwitchVehicle.click();
	}

	public void logOut() {
		_linkLogout.click();
		appDevice.waitForElementToLoad(_btnLogOutOk);
		_btnLogOutOk.click();
	}

	public void closeMenu() {
		_btnCloseMenuPanel.click();
	}
	

	//Popup method
	public String getRemoteResultPopUpMessageText(String remoteCommand) {

		String remoteMessage = "//XCUIElementTypeAlert//XCUIElementTypeStaticText[contains(@name,'" + remoteCommand + "')]";

		appDevice.sleepFor(10000);

//		if (_txtFaceId.isDisplayed()) {
//			_btnNotNowFaceId.click();
//		}

		appDevice.waitForElementToLoad(_btnOkPopUp);

		return driver.findElement(By.xpath(remoteMessage)).getText();

	}

	public boolean isVehicleNameDisplayed(String strVehicleName) {

		boolean flag = false;
		try {
			MobileElement vehicleName = driver
					.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + strVehicleName + "']"));
			if (vehicleName.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			e.getStackTrace();

		}
		return flag;

	}
	
	public void openMenu() {

		appDevice.sleepFor(15000);
		_btnMenu.click();
		appDevice.waitForElementToLoad(_panelMenu);
	}
	
	public boolean isMenuPanelDisplayed() {
		return _panelMenu.isDisplayed();
	}
}