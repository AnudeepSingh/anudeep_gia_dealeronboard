package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class ScheduleServicePage {
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	
	private final int IMPLICIT_WAIT = 10;
	
	
	@iOSFindBy(accessibility = "Car Care Scheduling")
	private MobileElement _txtCarCareScheduling;
	
	public ScheduleServicePage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	
	public boolean isScheduleServicePageDisplayed()
	{
		appDevice.waitForElementToLoad(_txtCarCareScheduling);
		return _txtCarCareScheduling.isDisplayed();
	}

}
