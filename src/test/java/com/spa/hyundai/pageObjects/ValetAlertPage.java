package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class ValetAlertPage {
	
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;

	public ValetAlertPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	@iOSFindBy(accessibility = "Valet Alert")
	private MobileElement _txtTitle;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'miles')]")
	private MobileElement _txtValetSelect;

	@iOSFindBy(accessibility = "sdm btn save default")
	private MobileElement _btnSave;

	@iOSFindBy(accessibility = "Successfully Saved.")
	private MobileElement _popUpSave;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	public boolean isPresent() {
		appDevice.waitForElementToLoad(_txtTitle);
		return _txtTitle.isDisplayed();
	}
	
	public String getValetValue()	{
		
		return _txtValetSelect.getText();
		
	}

	public void setValetValue(String value) {

		_txtValetSelect.click();
		driver.findElement(By.xpath("//XCUIElementTypePicker/XCUIElementTypePickerWheel")).setValue(value);
		driver.findElement(By.xpath("//XCUIElementTypeButton[@name='Done']")).click();

	}

	public void clickOnSaveButton() {

		_btnSave.click();
		appDevice.sleepFor(10000);
		_btnOkPopUp.click();
		appDevice.sleepFor(3000);

	}


}
