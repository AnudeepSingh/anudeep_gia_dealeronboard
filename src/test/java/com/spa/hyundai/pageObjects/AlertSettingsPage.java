package com.spa.hyundai.pageObjects;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindAll;
import io.appium.java_client.pagefactory.iOSFindBy;

public class AlertSettingsPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "Alert Settings")
	private MobileElement txtAlertSettingTitle;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Speed Alert']/ancestor:: XCUIElementTypeOther/XCUIElementTypeSwitch")
	private MobileElement _toggleSpeedAlert;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Valet Alert']/ancestor:: XCUIElementTypeOther/XCUIElementTypeSwitch")
	private MobileElement _toggleValetAlert;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Curfew Alert']/ancestor:: XCUIElementTypeOther/XCUIElementTypeSwitch")
	private MobileElement _toggleCurfewAlert;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Geo-Fence Alert']/ancestor:: XCUIElementTypeOther/XCUIElementTypeSwitch")
	private MobileElement _toggleGeoFenceAlert;

	@iOSFindAll({
			@iOSBy(xpath = "//XCUIElementTypeStaticText[@name= 'Speed Alert']/following-sibling::XCUIElementTypeOther/XCUIElementTypeButton") })
	private List<MobileElement> _btnSpeedAlert;

	@iOSFindAll({
			@iOSBy(xpath = "//XCUIElementTypeStaticText[@name= 'Valet Alert']/following-sibling::XCUIElementTypeOther/XCUIElementTypeButton") })
	private List<MobileElement> _btnValetAlert;

	@iOSFindAll({
			@iOSBy(xpath = "//XCUIElementTypeStaticText[@name= 'Curfew Alert']/following-sibling::XCUIElementTypeOther/XCUIElementTypeButton") })
	private List<MobileElement> _btnCurfewAlert;

	@iOSFindAll({
			@iOSBy(xpath = "//XCUIElementTypeStaticText[@name= 'Geo-Fence Alert']/following-sibling::XCUIElementTypeOther/XCUIElementTypeButton") })
	private List<MobileElement> _btnGeoFenceAlert;

	@iOSFindBy(accessibility = "sdm btn save default")
	private MobileElement _btnSave;

	@iOSFindBy(accessibility = "Successfully Saved.")
	private MobileElement _popUpSave;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	@iOSFindAll({
			@iOSBy(xpath = "//XCUIElementTypeStaticText[@name='Curfew Alert']/following-sibling:: XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeStaticText") })
	private List<MobileElement> _txtCurfewAlertValues;

	@iOSFindAll({
			@iOSBy(xpath = "//XCUIElementTypeStaticText[@name='Curfew Alert']/following-sibling:: XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell") })
	private List<MobileElement> _txtCurfewAlertAddedValues;

	@iOSFindAll({
			@iOSBy(xpath = "//XCUIElementTypeStaticText[@name='Geo-Fence Alert']/following-sibling:: XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeStaticText") })
	private List<MobileElement> _txtGeoFenceAlertValues;

	@iOSFindAll({
			@iOSBy(xpath = "//XCUIElementTypeStaticText[@name='Geo-Fence Alert']/following-sibling:: XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell") })
	private List<MobileElement> _txtGeoFenceAlertAddedValues;
	
	@iOSFindBy(accessibility = "sdm navibar back")
	private MobileElement _btnBack;

	public AlertSettingsPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isAlertSettingTitleDisplayed() {

		appDevice.sleepFor(10000);
		appDevice.waitForElementToLoad(txtAlertSettingTitle);
		return txtAlertSettingTitle.isDisplayed();
	}

	public String getSpeedAlertToggleValue() {
		return _toggleSpeedAlert.getAttribute("value");
	}

	public String getValetAlertToggleValue() {
		return _toggleValetAlert.getAttribute("value");
	}

	public String getCurfewAlertToggleValue() {
		return _toggleCurfewAlert.getAttribute("value");
	}

	public String getGeoFenceAlertToggleValue() {
		return _toggleGeoFenceAlert.getAttribute("value");
	}

	public void clickSpeedAlertToggle() {
		_toggleSpeedAlert.click();
	}

	public void clickValetAlertToggle() {
		_toggleValetAlert.click();
	}

	public void clickCurfewAlertToggle() {
		_toggleCurfewAlert.click();
	}

	public void clickGeoFenceAlertToggle() {

		appDevice.scrollDown();
		_toggleGeoFenceAlert.click();
	}

	public void openSpeedAlertPage() {

		if (_btnSpeedAlert.size() == 2)

			_btnSpeedAlert.get(1).click();

		else
			_btnSpeedAlert.get(0).click();

	}

	public void openValetAlertPage() {

		if (_btnValetAlert.size() == 2)

			_btnValetAlert.get(1).click();

		else
			_btnValetAlert.get(0).click();

	}

	public void openCurfewAlertPage() {

		if (_btnCurfewAlert.size() == 2)

			_btnCurfewAlert.get(1).click();

		else
			_btnCurfewAlert.get(0).click();

	}

	public void openGeoFenceAlertPage() {
		System.out.println(_btnGeoFenceAlert.size());
		if (_btnGeoFenceAlert.size() == 2) {
			System.out.println(12);
			_btnGeoFenceAlert.get(0).click();
		} else {
			System.out.println(13);
			_btnGeoFenceAlert.get(0).click();
		}

	}

	public void openExistingAlert(String alertName) {
		driver.findElement(
				By.xpath("//XCUIElementTypeStaticText[@name='" + alertName + "']/ancestor:: XCUIElementTypeCell"))
				.click();
	}

	public void clickOnSaveButton() {
		appDevice.waitForElementToLoad(_btnSave);
		appDevice.isElementDisplayed(_btnSave);
		_btnSave.click();
		appDevice.waitForElementToLoad(_btnOkPopUp);
		_btnOkPopUp.click();

	}

	public String getPopupSuccessMessageText(String remoteCommand) {

		String remoteMessage = "//XCUIElementTypeStaticText[contains(@name,'" + remoteCommand + "')]";
		appDevice.sleepFor(10000);
		appDevice.waitForElementToLoad(_btnOkPopUp);

		return driver.findElement(By.xpath(remoteMessage)).getText();

	}

	public void clickOnOkButton() {
		_btnOkPopUp.click();

	}

	public boolean isOkButtonDisplay() {

		appDevice.waitForElementToLoad(_btnOkPopUp);
		return appDevice.isElementDisplayed(_btnOkPopUp);

	}

	public List<String> getCurfewAlertValuesList() {

		List<String> curfewValue = new ArrayList<>();

		for (MobileElement ele : _txtCurfewAlertValues) {

			curfewValue.add(ele.getAttribute("name"));
		}
		return curfewValue;
	}

	public List<MobileElement> getCurfewAlertAddedValuesList() {

		return _txtCurfewAlertAddedValues;
	}

	public List<String> getGeoFenceAlertValuesList() {

		appDevice.scrollDown();
		appDevice.scrollDown();
		appDevice.scrollDown();
		appDevice.scrollDown();
		List<String> geoFenceValue = new ArrayList<>();

		for (MobileElement ele : _txtGeoFenceAlertValues) {

			geoFenceValue.add(ele.getAttribute("name"));
		}
		return geoFenceValue;
	}

	public List<MobileElement> getGeoFenceAlertAddedValuesList() {

		appDevice.scrollDown();
		appDevice.scrollDown();
		appDevice.scrollDown();
		appDevice.scrollDown();
		return _txtGeoFenceAlertAddedValues;
	}
	
	public void clickOnBackButton() {
		_btnBack.click();
	}

	public boolean isBackButtonDisplay() {
		return appDevice.isElementDisplayed(_btnBack);
	}

}
