package com.spa.hyundai.pageObjects;

import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import freemarker.core._TemplateModelException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class CarCarePage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	// Map Options

	@iOSFindBy(accessibility = "Diagnostics")
	private MobileElement linkDiagnostics;

	@iOSFindBy(accessibility = "Monthly Report")
	private MobileElement linkMonthlyReport;

	@iOSFindBy(accessibility = "Schedule Service")
	private MobileElement linkScheduleService;

	@iOSFindBy(accessibility = "Bluetooth")
	private MobileElement linkBluetooth;

	@iOSFindBy(accessibility = "Dealer Service Offers")
	private MobileElement linkDealerServiceOffers;

	@iOSFindBy(accessibility = "Owner’s Manual")
	private MobileElement linkOwnersManual;

	@iOSFindBy(accessibility = "Bluetooth")
	private MobileElement lblBluetooth;

	@iOSFindBy(accessibility = "Car Care Scheduling")
	private MobileElement lblCarCareScheduling;

	@iOSFindBy(accessibility = "All systems normal")
	private MobileElement lblAllSystemNormal;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Report Date')]")
	private MobileElement lblReprtDate;

	@iOSFindBy(accessibility = "bl mvr sched serv but")
	private MobileElement btnScheduleService;

	@iOSFindBy(accessibility = "ELECTRIC VEHICLE")
	private MobileElement lblElectricalVehicle;

	@iOSFindBy(accessibility = "Monthly Vehicle Health Report")
	private MobileElement lblMonthlyVehicleHealthReport;

	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='Error - CDK']|//XCUIElementTypeWebView//XCUIElementTypeStaticText[contains(@name,'schedulingUrl')]")
	private MobileElement errorPageSchedulePage;

	@iOSFindBy(xpath = "(//XCUIElementTypeOther[@name='Error - CDK']/XCUIElementTypeOther)[1]|//XCUIElementTypeWebView//XCUIElementTypeStaticText[contains(@name,'schedulingUrl')]")
	private MobileElement txtNetworkIssue;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='navigation']/XCUIElementTypeButton/following-sibling::XCUIElementTypeLink|//XCUIElementTypeOther[@name='Services Page']//XCUIElementTypeOther[@name='banner']|//XCUIElementTypeOther[@name='ServiceEdge Appointments Mobile']|//XCUIElementTypeStaticText[contains(@name,'SCHEDULE A SERVICE')]|//XCUIElementTypeOther[@name='West Broad Hyundai']")
	private MobileElement screenScheduleServiceWebPage;


	public CarCarePage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public void clickBluetoothLink() {
		linkBluetooth.click();

	}

	public void clickDealerServiceOffersLink() {
		linkDealerServiceOffers.click();

	}

	public void clickScheduleServiceLink() {
		linkScheduleService.click();

	}

	public boolean isBluetoothLabelDisplayed() {
		appDevice.waitForElementToLoad(lblBluetooth);
		return lblBluetooth.isDisplayed();
	}

	public boolean isOwnersManualLinkDisplayed() {
		appDevice.waitForElementToLoad(linkOwnersManual);
		return linkOwnersManual.isDisplayed();
	}

	public boolean isScheduleServiceLabelDisplayed() {
		appDevice.waitForElementToLoadWithProvidedTime(lblCarCareScheduling, 3);
		return appDevice.isElementDisplayed(lblCarCareScheduling);

	}

	public boolean islinkDealerServiceOffersDisplayed() {
		appDevice.waitForElementToLoad(linkDealerServiceOffers);
		return linkDealerServiceOffers.isDisplayed();
	}

	public void clickDiagnosticsLink() {
		linkDiagnostics.click();

	}

	public boolean islblAllSystemNormalDisplayed() {
		appDevice.waitForElementToLoad(lblAllSystemNormal);
		return lblAllSystemNormal.isDisplayed();
	}

	public void clickMonthlyReportLink() {
		linkMonthlyReport.click();

	}

	public boolean islblReprtDateDisplayed() {
		appDevice.waitForElementToLoad(lblReprtDate);
		return lblReprtDate.isDisplayed();
	}

	public boolean isbtnScheduleServiceDisplayed() {
		appDevice.waitForElementToLoad(btnScheduleService);
		return btnScheduleService.isDisplayed();
	}

	public boolean islblMonthlyVehicleHealthReportDisplayed() {
		appDevice.waitForElementToLoad(lblMonthlyVehicleHealthReport);
		return lblMonthlyVehicleHealthReport.isDisplayed();
	}

	public boolean islblElectricalVehicleStateDisplayed() {
		appDevice.waitForElementToLoad(lblMonthlyVehicleHealthReport);
		return lblMonthlyVehicleHealthReport.isDisplayed();
	}

	public boolean isErrorScheduleServicePageDisplayed() {
		appDevice.waitForElementToLoadWithProvidedTime(errorPageSchedulePage, 3);
		return appDevice.isElementDisplayed(errorPageSchedulePage);

	}

	public String getNetworkIssueText() {
		return txtNetworkIssue.getAttribute("name");
	}
	
	public boolean isScreenScheduleServiceWebScreenDisplayed() {
		appDevice.sleepFor(5000);
		appDevice.waitForElementToLoadWithProvidedTime(screenScheduleServiceWebPage,3);
		System.out.println("Schedule Page Status" +appDevice.isElementDisplayed(screenScheduleServiceWebPage));
		return appDevice.isElementDisplayed(screenScheduleServiceWebPage);

	}

}
