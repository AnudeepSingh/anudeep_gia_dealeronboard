package com.spa.hyundai.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.AboutAndSupportPage;
import com.spa.hyundai.pageObjects.AccessoriesPage;
import com.spa.hyundai.pageObjects.AlertSettingsPage;
import com.spa.hyundai.pageObjects.DealerLocatorPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.MenuPage;
import com.spa.hyundai.pageObjects.ScheduleServicePage;
import com.spa.hyundai.pageObjects.SettingsPage;
import com.spa.hyundai.pageObjects.SwitchVehiclePage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class MenuTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static AlertSettingsPage alertSettingsPage;
	private static ScheduleServicePage scheduleServicePage;
	private static AccessoriesPage accessoriesPage;
	private static AboutAndSupportPage aboutAndSupportPage;
	private static SettingsPage settingsPage;
	private static SwitchVehiclePage switchVehiclePage;
	private static LoginPage loginPage;
	private static MenuPage menuPage;

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "Users");
	}

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		alertSettingsPage = new AlertSettingsPage(empAppDevice);
		scheduleServicePage = new ScheduleServicePage(empAppDevice);
		accessoriesPage = new AccessoriesPage(empAppDevice);
		aboutAndSupportPage = new AboutAndSupportPage(empAppDevice);
		settingsPage = new SettingsPage(empAppDevice);
		switchVehiclePage = new SwitchVehiclePage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		menuPage = new MenuPage(empAppDevice);
	}

	@Test(description = "Test navigation to alert setting page via menu.", dataProvider = "TestLoginData")
	public void testNavigationToAlertSettings(String strVehicleType, String strTestRun, String strEmail,
			String strPassword,String strPin,  String strVIN) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		consoleLog("Log in with : " + strEmail + "/" + strPassword + "/" + strVIN);
		menuPage.openMenu();

		// Verify Menu option and User profile section
		Assert.assertTrue(menuPage.isMenuPanelDisplayed(), "Menu Panel is displaying");
 
		menuPage.navigateToAlertSettings();

		Assert.assertTrue(alertSettingsPage.isAlertSettingTitleDisplayed(), "Alert Setting page is not displaying.");
	}

	@Test(description = "Test navigation to schedule services page using menu", dataProvider = "TestLoginData")
	public void testNavigationToScheduleService(String strVehicleType, String strTestRun, String strEmail,
			String strPassword,String strPin,  String strVIN) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		consoleLog("Log in with : " + strEmail + "/" + strPassword + "/" + strVIN);
		menuPage.openMenu();

		// Verify Menu option and User profile section
		Assert.assertTrue(menuPage.isMenuPanelDisplayed(), "Menu Panel is displaying");

		menuPage.navigateToScheduleServices();

		Assert.assertTrue(scheduleServicePage.isScheduleServicePageDisplayed(),
				"Schedule service page is not displaying.");

	}

	@Test(description = "Test navigation to Accessories page using menu", dataProvider = "TestLoginData")
	public void testNavigationToAccessories(String strVehicleType, String strTestRun, String strEmail,
			String strPassword,String strPin,  String strVIN) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		consoleLog("Log in with : " + strEmail + "/" + strPassword + "/" + strVIN);
		menuPage.openMenu();

		// Verify Menu option and User profile section
		Assert.assertTrue(menuPage.isMenuPanelDisplayed(), "Menu Panel is displaying");

		menuPage.navigateToAccessories();
		;

		Assert.assertTrue(accessoriesPage.isAccessoriesPageDisplayed(), "Accessories page is not displaying.");

	}

	// To Do ... Move This test Method Under POI Section

	@Test(description = "Test navigation to About and Support page using menu", dataProvider = "TestLoginData")
	public void testNavigationToAboutAndSupportPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword,String strPin,  String strVIN) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		consoleLog("Log in with : " + strEmail + "/" + strPassword + "/" + strVIN);
		menuPage.openMenu();

		// Verify Menu option and User profile section
		Assert.assertTrue(menuPage.isMenuPanelDisplayed(), "Menu Panel is displaying");

		menuPage.navigateToAboutAndSupport();

		Assert.assertTrue(aboutAndSupportPage.isAboutAndSupportPageDisplayed(),
				"About and Support page is not displaying.");

	}

	@Test(description = "Test navigation to Settings page using menu", dataProvider = "TestLoginData")
	public void testNavigationToSetting(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		consoleLog("Log in with : " + strEmail + "/" + strPassword + "/" + strVIN);
		menuPage.openMenu();

		// Verify Menu option and User profile section
		Assert.assertTrue(menuPage.isMenuPanelDisplayed(), "Menu Panel is displaying");

		menuPage.navigateToSettings();

		Assert.assertTrue(settingsPage.isSettingsPageDisplayed(), "About and Support page is not displaying.");

	}

	@Test(description = "Test navigation to Switch vehicle page using menu", dataProvider = "TestLoginData")
	public void testNavigationToSwitchVehicle(String strVehicleType, String strTestRun, String strEmail,
			String strPassword,String strPin,  String strVIN) throws IOException {
		loginPage.validLogin("AE222@hmausa.com", "test1234", "KMHC75LJ9LU049524");
		consoleLog("Log in with : " + strEmail + "/" + strPassword + "/" + strVIN);
		menuPage.openMenu();

		// Verify Menu option and User profile section
		Assert.assertTrue(menuPage.isMenuPanelDisplayed(), "Menu Panel is displaying");

		menuPage.navigateToSwitchVehicle();

		Assert.assertTrue(switchVehiclePage.isSwitchVehiclePageDisplay(), "Switch vehicle page is not displaying.");

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
