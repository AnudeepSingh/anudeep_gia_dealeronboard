package com.spa.hyundai.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.AlertSettingsPage;
import com.spa.hyundai.pageObjects.CurfewAlertPage;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.DealerLocatorPage;
import com.spa.hyundai.pageObjects.EnterPinPage;
import com.spa.hyundai.pageObjects.GeoFenceAlertPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.MapPage;
import com.spa.hyundai.pageObjects.MenuPage;
import com.spa.hyundai.pageObjects.SpeedAlertPage;
import com.spa.hyundai.pageObjects.ValetAlertPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class AlertSettingsTest extends BaseTest {

	private String strVehicleType, strTestRun, strEmail, strPassword, strPin, strVin = "";
	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;

	private static DashboardPage dashboardPage;
	private static AlertSettingsPage alertSettingsPage;
	private static SpeedAlertPage speedAlertPage;
	private static ValetAlertPage valetAlertPage;
	private static CurfewAlertPage curfewAlertPage;
	private static GeoFenceAlertPage geoFenceAlertPage;
	private static LoginPage loginPage;
	private static MenuPage menuPage;

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "Users");
	}

	@Factory(dataProvider = "TestLoginData")
	public AlertSettingsTest(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVin) {

		this.strVehicleType = strVehicleType;
		this.strTestRun = strTestRun;
		this.strEmail = strEmail;
		this.strPassword = strPassword;
		this.strPin = strPin;
		this.strVin = strVin;

	}

	@BeforeClass(alwaysRun = true)
	public void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		dashboardPage = new DashboardPage(empAppDevice);
		alertSettingsPage = new AlertSettingsPage(empAppDevice);
		speedAlertPage = new SpeedAlertPage(empAppDevice);
		valetAlertPage = new ValetAlertPage(empAppDevice);
		curfewAlertPage = new CurfewAlertPage(empAppDevice);
		geoFenceAlertPage = new GeoFenceAlertPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		menuPage = new MenuPage(empAppDevice);
		System.out.println(strEmail);

	}

	public void relaunchAppAndInitializedVariable() throws Exception {
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		dashboardPage = new DashboardPage(empAppDevice);
		alertSettingsPage = new AlertSettingsPage(empAppDevice);
		speedAlertPage = new SpeedAlertPage(empAppDevice);
		valetAlertPage = new ValetAlertPage(empAppDevice);
		curfewAlertPage = new CurfewAlertPage(empAppDevice);
		geoFenceAlertPage = new GeoFenceAlertPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		menuPage = new MenuPage(empAppDevice);
	}

	public void loginConditions() throws Exception {
		try {
			System.out.println("Logged In using : " + strEmail);
			if (!dashboardPage.isVehicleStatusButtonDisplayeWithinExpectedTime(2)) {
				loginPage.validLogin(strEmail, strPassword, strVin);
			    dashboardPage.VehicleStatusButtonDisplayStatusWithoutHandelException();
				System.out.println(dashboardPage.VehicleStatusButtonDisplayStatusWithoutHandelException());
				//i++;
				System.out.println("Logged In Fisr Time With : " + strEmail);
			}
			else {
				System.out.println("Already Login with " + strEmail);
			}
		}

		catch (NoSuchElementException ex) {
			System.out.println("Logged In Again Due To No Such Element Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		} catch (WebDriverException e) {
			System.out.println("Logged In Again Due To Wed Driver Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		}

	}

	@Test(description = "Speed Alert Switch Toggle Test")
	public void speedAlertTogggleSwitch() throws Exception {
		loginConditions();

		// navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();

		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get initial Toggle value of Speed Alert
		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getSpeedAlertToggleValue();

		System.out.println("Speed Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);

		String selectedSpeed = speedAlertPage.getSpeedValue();

		alertSettingsPage.clickSpeedAlertToggle();
		alertSettingsPage.clickOnSaveButton();

		// wait for Remote action to get completed
		empAppDevice.sleepFor(25000);

		// navigate back to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get updated Toggle value of Speed Alert
		String TOGGLE_VALUE_AFTER_UPDATE = alertSettingsPage.getSpeedAlertToggleValue();

		Assert.assertTrue(selectedSpeed.equals(speedAlertPage.getSpeedValue()),
				"The Edit Speed value not same after chage toggle");
		System.out.println("Speed Alert Toggle Value after Update:" + TOGGLE_VALUE_AFTER_UPDATE);

		System.out.println(TOGGLE_VALUE_BEFORE_UPDATE);
		System.out.println(TOGGLE_VALUE_AFTER_UPDATE);

		Assert.assertTrue(!(TOGGLE_VALUE_BEFORE_UPDATE.equals(TOGGLE_VALUE_AFTER_UPDATE)),
				"Speed Alert Toggle Value did not get updated");
	}

	@Test(description = "Valet Alert Switch Toggle Test")
	public void ValetAlertTogggleSwitch() throws Exception {
		loginConditions();

		// gasVehicleDashboardPage.isVehicleStatusButtonDisplaye();
		// navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();

		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get initial Toggle value of Valet Alert
		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getValetAlertToggleValue();

		System.out.println("Valet Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
		String selectedValet = valetAlertPage.getValetValue();

		alertSettingsPage.clickValetAlertToggle();
		alertSettingsPage.clickOnSaveButton();

		// wait for Remote action to get completed
		empAppDevice.sleepFor(10000);

		// navigate back to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get updated Toggle value of Valet Alert
		String TOGGLE_VALUE_AFTER_UPDATE = alertSettingsPage.getValetAlertToggleValue();

		System.out.println("Valet Alert Toggle Value after Update:" + TOGGLE_VALUE_AFTER_UPDATE);
		Assert.assertTrue(selectedValet.equals(valetAlertPage.getValetValue()),
				"The Edit Valet value updated successful");

		Assert.assertTrue(!(TOGGLE_VALUE_BEFORE_UPDATE.equals(TOGGLE_VALUE_AFTER_UPDATE)),
				"Valet Alert Toggle Value did not get updated");
	}

	@Test(description = "Curfew Alert Switch Toggle Test")
	public void CurfewAlertTogggleSwitch() throws Exception {
		loginConditions();

		// gasVehicleDashboardPage.isVehicleStatusButtonDisplaye();

		// navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();

		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get initial Toggle value of Curfew Alert
		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getCurfewAlertToggleValue();

		System.out.println("Curfew Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);

		List<MobileElement> curfewAlertValueBefore = alertSettingsPage.getCurfewAlertAddedValuesList();
		System.out.println(curfewAlertValueBefore);

		alertSettingsPage.clickCurfewAlertToggle();
		alertSettingsPage.clickOnSaveButton();

		// wait for Remote action to get completed
		empAppDevice.sleepFor(10000);

		// navigate back to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get updated Toggle value of Curfew Alert
		String TOGGLE_VALUE_AFTER_UPDATE = alertSettingsPage.getCurfewAlertToggleValue();

		List<MobileElement> curfewAlertValueAfter = alertSettingsPage.getCurfewAlertAddedValuesList();
		System.out.println(curfewAlertValueAfter);

		System.out.println("Curfew Alert Toggle Value after Update:" + TOGGLE_VALUE_AFTER_UPDATE);

		Assert.assertTrue(curfewAlertValueAfter.equals(curfewAlertValueBefore),
				"Curfew Alert Value get changed after perform toggle action");

		Assert.assertTrue(!(TOGGLE_VALUE_BEFORE_UPDATE.equals(TOGGLE_VALUE_AFTER_UPDATE)),
				"Curfew Alert Toggle Value did not get updated");
	}

	@Test(description = "GeoFence Alert Switch Toggle Test")
	public void GeoFenceAlertTogggleSwitch() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();

		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get initial Toggle value of GeoFence Alert
		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getGeoFenceAlertToggleValue();

		System.out.println("GeoFence Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);

		List<MobileElement> geoFenceValueBefore = alertSettingsPage.getGeoFenceAlertAddedValuesList();
		System.out.println(geoFenceValueBefore);
		alertSettingsPage.clickGeoFenceAlertToggle();
		alertSettingsPage.clickOnSaveButton();

		// wait for Remote action to get completed
		empAppDevice.sleepFor(10000);

		// navigate back to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get updated Toggle value of GeoFence Alert
		String TOGGLE_VALUE_AFTER_UPDATE = alertSettingsPage.getGeoFenceAlertToggleValue();

		System.out.println("GeoFence Alert Toggle Value after Update:" + TOGGLE_VALUE_AFTER_UPDATE);

		List<MobileElement> geoFenceValueAfter = alertSettingsPage.getGeoFenceAlertAddedValuesList();
		System.out.println(geoFenceValueAfter);

		Assert.assertTrue(geoFenceValueAfter.equals(geoFenceValueBefore),
				"Geo Fence Alert Value get changed after perform toggle action");
		Assert.assertTrue(!(TOGGLE_VALUE_BEFORE_UPDATE.equals(TOGGLE_VALUE_AFTER_UPDATE)),
				"GeoFence Alert Toggle Value did not get updated");
	}

	@Test(description = "Speed Alert Create/Edit Test also validate the toggle not getting automatic on when putting toggle off and then create the alert")
	public void createEditSpeedAlert() throws Exception {
		String ALERT_NAME = "Speed Alert Preference";
		String SPEED_VALUE1 = "90 mph";
		String SPEED_VALUE2 = "45 mph";
		// Login to the application
		loginConditions();
		// Navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();
//		// Switching off the speed alert toggle
//		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getSpeedAlertToggleValue();
//		System.out.println("Speed Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
//		if (TOGGLE_VALUE_BEFORE_UPDATE.equals("1")) {
//			alertSettingsPage.clickSpeedAlertToggle();
//			alertSettingsPage.clickOnSaveButton();
//			empAppDevice.sleepFor(25000);
//			dashboardPage.openMenu();
//			menuPage.navigateToAlertSettings();
//			alertSettingsPage.isAlertSettingTitleDisplayed();
//			Assert.assertTrue(alertSettingsPage.getSpeedAlertToggleValue().equals("0"),
//					"The speed alert toggle not getting off");
//			TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getSpeedAlertToggleValue();
//		}
//		System.out.println("Speed Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
//		Assert.assertTrue(alertSettingsPage.getSpeedAlertToggleValue().equals("0"),
//				"The speed alert toggle not getting off");

		String selectedSpeedBeforeUpdate = speedAlertPage.getSpeedValue();

		// navigate to Speed alert Settings Page and verify by updating the speed value
		alertSettingsPage.openSpeedAlertPage();
		speedAlertPage.isPresent();
		if (selectedSpeedBeforeUpdate.equals(SPEED_VALUE1)) {
			speedAlertPage.setSpeedValue(SPEED_VALUE2);
		} else {
			speedAlertPage.setSpeedValue(SPEED_VALUE1);
		}
		String updatedSpeedValue = speedAlertPage.getSpeedValue();
		Assert.assertTrue(!selectedSpeedBeforeUpdate.equals(updatedSpeedValue),
				"The Edit Speed value not updated successful");
		speedAlertPage.clickOnSaveButton();

		// Validation after perform save at speed alert page
		String alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);
		alertSettingsPage.clickOnOkButton();
		System.out.println("Alert Save Message:" + alertPopupMessage);
		Assert.assertTrue(alertPopupMessage.contains("successfully updated"),
				"The Edit Speed Alert command was not successful");
		String speedValueAfterUpdate = speedAlertPage.getSpeedValue();
		Assert.assertTrue(!selectedSpeedBeforeUpdate.equals(speedValueAfterUpdate),
				"The Edit Speed value not updated successful");
		String TOGGLE_VALUE_AFTER_UPDATE = alertSettingsPage.getSpeedAlertToggleValue();
//		Assert.assertTrue(TOGGLE_VALUE_BEFORE_UPDATE.equals(TOGGLE_VALUE_AFTER_UPDATE),
//				"Speed Alert Toggle Value not equal to previous value after perform save frpm speed alert window");

		// Perform save at main alert window
		alertSettingsPage.clickOnSaveButton();

		// wait for Remote action to get completed
		empAppDevice.sleepFor(25000);

		// navigate back to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();
		String speedValueAfterSave = speedAlertPage.getSpeedValue();
		Assert.assertTrue(speedValueAfterSave.equals(updatedSpeedValue), "The Edit Speed value not equals");
		String TOGGLE_VALUE_AFTER_Save = alertSettingsPage.getSpeedAlertToggleValue();
//		Assert.assertTrue(TOGGLE_VALUE_AFTER_Save.equals(TOGGLE_VALUE_AFTER_UPDATE),
//				"Speed Alert Toggle Value not equal to previous value after perform save from alert wondow");

	}

	@Test(description = "Valet Alert Create/Edit Test also validate the toggle not getting automatic on when putting toggle off and then create the alert")
	public void createEditValetAlert() throws Exception {
		String ALERT_NAME = "Valet Alert Preference";
		String VALET_VALUE1 = "0.5 miles";
		String VALET_VALUE2 = "0.75 miles";
		loginConditions();
		// navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

//		// get initial Toggle value of Valet Alert
//		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getValetAlertToggleValue();
//		System.out.println("Valet Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
//		if (TOGGLE_VALUE_BEFORE_UPDATE.equals("1")) {
//			alertSettingsPage.clickValetAlertToggle();
//			alertSettingsPage.clickOnSaveButton();
//			empAppDevice.sleepFor(25000);
//			dashboardPage.openMenu();
//			menuPage.navigateToAlertSettings();
//			alertSettingsPage.isAlertSettingTitleDisplayed();
//			Assert.assertTrue(alertSettingsPage.getValetAlertToggleValue().equals("0"),
//					"The valet alert toggle not getting off");
//			TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getValetAlertToggleValue();
//		}
//		TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getValetAlertToggleValue();
//		Assert.assertTrue(alertSettingsPage.getValetAlertToggleValue().equals("0"),
//				"The valet alert toggle not getting off");

		String valetValueBeforeUpdate = valetAlertPage.getValetValue();

		// navigate to Valet alert Settings Page
		alertSettingsPage.openValetAlertPage();
		valetAlertPage.isPresent();
		if (valetValueBeforeUpdate.equals(VALET_VALUE1)) {
			valetAlertPage.setValetValue(VALET_VALUE2);
		} else {
			valetAlertPage.setValetValue(VALET_VALUE1);
		}
		Assert.assertTrue(!valetValueBeforeUpdate.equals(valetAlertPage.getValetValue()),
				"The Edit Valet value updated successful");
		valetAlertPage.clickOnSaveButton();

		// Validation after perform save at valet alert page
		String alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);
		alertSettingsPage.clickOnOkButton();
		System.out.println("Alert Save Message:" + alertPopupMessage);

		Assert.assertTrue(alertPopupMessage.contains("successfully updated"),
				"The Edit Valet Alert command was not successful");
		String valetValueAfterUpdate = valetAlertPage.getValetValue();
		Assert.assertTrue(!valetValueBeforeUpdate.equals(valetValueAfterUpdate),
				"The Edit Valet value updated successful");
		String TOGGLE_VALUE_AFTER_UPDATE = alertSettingsPage.getValetAlertToggleValue();
//		Assert.assertTrue(TOGGLE_VALUE_BEFORE_UPDATE.equals(TOGGLE_VALUE_AFTER_UPDATE),
//				"Valet Alert Toggle Value not equal to previous value after perform save frpm speed alert window");

		// Perform save at main alert window
		alertSettingsPage.clickOnSaveButton();

		// wait for Remote action to get completed
		empAppDevice.sleepFor(25000);

		// navigate back to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();
		String valetValueAfterSave = valetAlertPage.getValetValue();
		Assert.assertTrue(!valetValueBeforeUpdate.equals(valetValueAfterSave),
				"The Edit Valet value updated successful");
		String TOGGLE_VALUE_AFTER_SAVE = alertSettingsPage.getValetAlertToggleValue();
//		Assert.assertTrue(TOGGLE_VALUE_BEFORE_UPDATE.equals(TOGGLE_VALUE_AFTER_SAVE),
//				"Valet Alert Toggle Value not equal to previous value after perform save frpm speed alert window");

	}

	@Test(description = "Create Curfew Alert Testalso validate the toggle not getting automatic on when putting toggle off and then create the alert")
	public void createCurfewAlert() throws Exception {

		String ALERT_NAME = "Curfew Preference";
		String curfewFromDay = "Thursday";
		String curfewToDay = "Friday";
		String curfewFromHour = "8";
		String curfewToHour = "9";
		String curfewFromMinute = "30";
		String curfewToMinute = "45";
		String curfewFromAmPm = "AM";
		String curfewToAmPm = "PM";

		String curfewFromData = curfewFromDay + ", " + curfewFromHour + ":" + curfewFromMinute + " " + curfewFromAmPm;
		String curfewToData = curfewToDay + ", " + curfewToHour + ":" + curfewToMinute + " " + curfewToAmPm;
		System.out.println(curfewFromData + " " + curfewToData);
		loginConditions();
		// gasVehicleDashboardPage.isVehicleStatusButtonDisplaye();
		// navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();

		alertSettingsPage.isAlertSettingTitleDisplayed();

//		// get initial Toggle value of Curfew Alert
//		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getCurfewAlertToggleValue();
//		System.out.println("Curfew Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
//		// Switching off curfew alert toggle
//		if (TOGGLE_VALUE_BEFORE_UPDATE.equals("1")) {
//			alertSettingsPage.clickCurfewAlertToggle();
//			alertSettingsPage.clickOnSaveButton();
//			empAppDevice.sleepFor(25000);
//			dashboardPage.openMenu();
//			menuPage.navigateToAlertSettings();
//			alertSettingsPage.isAlertSettingTitleDisplayed();
//			Assert.assertTrue(alertSettingsPage.getCurfewAlertToggleValue().equals("0"),
//					"The curfew alert toggle not getting off");
//			TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getCurfewAlertToggleValue();
//		}

		Assert.assertTrue(alertSettingsPage.getCurfewAlertToggleValue().equals("0"),
				"The curfew alert toggle not getting off");

		List<MobileElement> curfewAlertValueBefore = alertSettingsPage.getCurfewAlertAddedValuesList();
		System.out.println(curfewAlertValueBefore);

		curfewAlertPage.selectCurfewAndDelete(curfewFromData, curfewToData);

		// navigate to Curfew alert Settings Page
		alertSettingsPage.openCurfewAlertPage();
		curfewAlertPage.isPresent();
		// Create new curfer alert
		curfewAlertPage.setCurfewFromValues(curfewFromDay, curfewFromHour, curfewFromMinute, curfewFromAmPm);
		curfewAlertPage.setCurfewToValues(curfewToDay, curfewToHour, curfewToMinute, curfewToAmPm);
		// Perform save at curfew alert page and validate that new curfew is added or
		// not
		curfewAlertPage.clickOnSaveButton();
		Assert.assertTrue(curfewAlertPage.isMentioedCurfewDisplay(curfewFromData, curfewToData),
				"The New Curfew Alert command not added successful");
		if (!alertSettingsPage.isOkButtonDisplay()) {
			curfewAlertPage.selectCurfew(curfewFromData, curfewToData);
			alertSettingsPage.clickOnSaveButton();
		}
		String alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);
		System.out.println("Alert Save Message:" + alertPopupMessage);
		alertSettingsPage.clickOnOkButton();
//		Assert.assertTrue(alertSettingsPage.getCurfewAlertToggleValue().equals("0"),
//				"The curfew alert toggle not getting off");
		List<MobileElement> curfewAlertValueAfter = alertSettingsPage.getCurfewAlertAddedValuesList();
		System.out.println(curfewAlertValueAfter);
		Assert.assertTrue(alertPopupMessage.contains("successfully updated"),
				"The Create Curfew Alert command was not successful");
		Assert.assertTrue(curfewAlertValueAfter.size() > curfewAlertValueBefore.size(),
				"Curfew Alert Count get affected after adding the new one as after adding count is"
						+ curfewAlertValueAfter.size() + "abd before adding count is " + curfewAlertValueBefore.size());
		// Validate that new created curfew still present after save from alert page
		alertSettingsPage.clickOnSaveButton();
		empAppDevice.sleepFor(25000);
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

		List<MobileElement> curfewAlertValueAfterSave = alertSettingsPage.getCurfewAlertAddedValuesList();
		Assert.assertTrue(curfewAlertValueAfter.size() == curfewAlertValueAfterSave.size(),
				"Curfew Alert Count get affected after perform save from alert setting page"
						+ curfewAlertValueAfter.size() + "and after perform save  " + curfewAlertValueAfterSave.size());
//		Assert.assertTrue(alertSettingsPage.getCurfewAlertToggleValue().equals("0"),
//				"The curfew alert toggle not getting off");

	}

	@Test(description = "Create Geo-Fence Alert Test also validate the toggle not getting automatic on when putting toggle off and then create the alert")
	public void createGeoFenceAlert() throws Exception {

		String ALERT_NAME = "Geo-Fence Preference";
		String STR_ZIP = "90001";
		String GEOFENCE_NAME = "test_GeoFence";
		String GEOFENCE_MILES = "10";

		loginConditions();
		// gasVehicleDashboardPage.isVehicleStatusButtonDisplaye();

		// navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();

		alertSettingsPage.isAlertSettingTitleDisplayed();
		empAppDevice.scrollDown();
//		// get initial Toggle value of Curfew Alert
//		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getGeoFenceAlertToggleValue();
//		System.out.println("Geo Fence Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
//		// Switching off curfew alert toggle
//		if (TOGGLE_VALUE_BEFORE_UPDATE.equals("1")) {
//			alertSettingsPage.clickGeoFenceAlertToggle();
//			alertSettingsPage.clickOnSaveButton();
//			empAppDevice.sleepFor(25000);
//			dashboardPage.openMenu();
//			menuPage.navigateToAlertSettings();
//			alertSettingsPage.isAlertSettingTitleDisplayed();
//			Assert.assertTrue(alertSettingsPage.getGeoFenceAlertToggleValue().equals("0"),
//					"The curfew alert toggle not getting off");
//			TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getCurfewAlertToggleValue();
//		}
//
//		Assert.assertTrue(alertSettingsPage.getGeoFenceAlertToggleValue().equals("0"),
//				"The curfew alert toggle not getting off");
		List<MobileElement> geoFenceValueBefore = alertSettingsPage.getGeoFenceAlertAddedValuesList();
		System.out.println(geoFenceValueBefore);

		// navigate to Geo Fence alert Settings Page
		alertSettingsPage.openGeoFenceAlertPage();
		geoFenceAlertPage.isPresent();
		geoFenceAlertPage.selectAndDeleteGeoFence(GEOFENCE_NAME);
		// empAppDevice.scrollDown();
		geoFenceAlertPage.setGeoFenceAlert(STR_ZIP, GEOFENCE_NAME, GEOFENCE_MILES);

		// Perform save at curfew alert page and validate that new curfew is added or
		// not
		geoFenceAlertPage.clickOnSaveButton();
		Assert.assertTrue(geoFenceAlertPage.selectGeoFenceDisplay(GEOFENCE_NAME),
				"The Create Geo-Fence Alert command not added successful");
		String alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);
		alertSettingsPage.clickOnOkButton();
		System.out.println("Alert Save Message:" + alertPopupMessage);
		List<MobileElement> geoFenceValueAfter = alertSettingsPage.getGeoFenceAlertAddedValuesList();
		System.out.println(geoFenceValueAfter);
		Assert.assertTrue(
				alertPopupMessage.contains("successfully updated") || alertPopupMessage.contains("failed to update"),
				"The Create Geo-Fence Alert command was not successful");
		System.out.println(geoFenceValueBefore.size());
		System.out.println(geoFenceValueAfter.size());
		Assert.assertTrue(geoFenceValueAfter.size() > geoFenceValueBefore.size(),
				"Geo Fence Alert count affect previous geo fence alert after adding the new one as after adding count is "
						+ geoFenceValueAfter.size() + " and before adding the count is" + geoFenceValueBefore.size());
//		Assert.assertTrue(alertSettingsPage.getGeoFenceAlertToggleValue().equals("0"),
//				"The curfew alert toggle not getting off");

		// Validate that new created Geo Fence still present after save from alert page
		alertSettingsPage.clickOnSaveButton();
		empAppDevice.sleepFor(25000);
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

		List<MobileElement> geoFenceAlertValueAfterSave = alertSettingsPage.getGeoFenceAlertAddedValuesList();
		Assert.assertTrue(geoFenceValueAfter.size() == geoFenceAlertValueAfterSave.size() - 1,
				"Geo Fence Alert Count get affected after perform save from alert setting page "
						+ geoFenceValueAfter.size() + "and after perform save  " + geoFenceAlertValueAfterSave.size());
//		Assert.assertTrue(alertSettingsPage.getGeoFenceAlertToggleValue().equals("0"),
//				"The Geo Fence alert toggle not getting off");
	}

	@Test(priority = 9, description = "Edit Geo-Fence Alert Test also validate the toggle does not getting off when we edit the value after putting toggle on")
	public void editGeoFenceAlert() throws Exception {

		String ALERT_NAME = "Geo-Fence Preference";
		String STR_ZIP = "90001";
		String GEOFENCE_OLD_NAME = "test_GeoFence";
		String GEOFENCE_NEW_NAME = "test23_GeoFence";
		String GEOFENCE_MILES = "10";

		loginConditions();
		// gasVehicleDashboardPage.isVehicleStatusButtonDisplaye();

		// navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();

		alertSettingsPage.isAlertSettingTitleDisplayed();
		empAppDevice.scrollDown();

		// get initial Toggle value of Curfew Alert
		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getGeoFenceAlertToggleValue();
		System.out.println("Geo Fence Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
		// Switching off Geo alert toggle
		if (TOGGLE_VALUE_BEFORE_UPDATE.equals("1")) {
			alertSettingsPage.clickGeoFenceAlertToggle();
			alertSettingsPage.clickOnSaveButton();
			empAppDevice.sleepFor(25000);
			dashboardPage.openMenu();
			menuPage.navigateToAlertSettings();
			alertSettingsPage.isAlertSettingTitleDisplayed();
			Assert.assertTrue(alertSettingsPage.getGeoFenceAlertToggleValue().equals("0"),
					"The geo fence alert toggle not getting off");
			TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getCurfewAlertToggleValue();
		}

		Assert.assertTrue(alertSettingsPage.getGeoFenceAlertToggleValue().equals("0"),
				"The geo fence alert toggle not getting off");
		alertSettingsPage.clickGeoFenceAlertToggle();
		Assert.assertTrue(alertSettingsPage.getGeoFenceAlertToggleValue().equals("1"),
				"The geo fence alert toggle not getting on");

		List<MobileElement> geoFenceValueBefore = alertSettingsPage.getGeoFenceAlertAddedValuesList();
		System.out.println(geoFenceValueBefore);

		if (!geoFenceAlertPage.selectGeoFenceDisplay(GEOFENCE_OLD_NAME)) {
			// navigate to Geo Fence alert Settings Page
			alertSettingsPage.openGeoFenceAlertPage();
			geoFenceAlertPage.isPresent();
			empAppDevice.sleepFor(10000);

			geoFenceAlertPage.setGeoFenceAlert(STR_ZIP, GEOFENCE_OLD_NAME, GEOFENCE_MILES);
			geoFenceAlertPage.clickOnSaveButton();

			String alertPopupMessage_Add = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);

			System.out.println("Alert Save Message:" + alertPopupMessage_Add);

			Assert.assertTrue(alertPopupMessage_Add.contains("successfully updated"),
					"The Create Geo-Fence Alert command was not successful");

			alertSettingsPage.clickOnOkButton();
			geoFenceValueBefore = alertSettingsPage.getGeoFenceAlertAddedValuesList();
			System.out.println(geoFenceValueBefore);

		}
		// navigate to Geo-Fence alert Settings Page
		alertSettingsPage.openExistingAlert(GEOFENCE_OLD_NAME);

		geoFenceAlertPage.isPresent();

		geoFenceAlertPage.editGeoFenceAlert(GEOFENCE_NEW_NAME);
		geoFenceAlertPage.clickOnSaveButton();

		if (!alertSettingsPage.isOkButtonDisplay()) {
			alertSettingsPage.openExistingAlert(GEOFENCE_NEW_NAME);
			alertSettingsPage.clickOnSaveButton();
		}
		alertSettingsPage.isOkButtonDisplay();
		String alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);

		System.out.println("Alert Save Message:" + alertPopupMessage);

		Assert.assertTrue(alertPopupMessage.contains("successfully updated"),
				"The Create Geo-Fence Alert command was not successful");

		alertSettingsPage.clickOnOkButton();

		Assert.assertTrue(!geoFenceAlertPage.selectGeoFenceDisplay(GEOFENCE_OLD_NAME),
				"The Create Geo-Fence Alert command not edited successful");

		Assert.assertTrue(geoFenceAlertPage.selectGeoFenceDisplay(GEOFENCE_NEW_NAME),
				"The Create Geo-Fence Alert command not present or edited successful");

		List<MobileElement> geoFenceValueAfter = alertSettingsPage.getGeoFenceAlertAddedValuesList();
		System.out.println(geoFenceValueAfter);

		Assert.assertTrue(geoFenceValueAfter.size() == geoFenceValueBefore.size(),
				"Geo Fence Alert Value affect the count of previous geo fence alert editing adding the new one");

		Assert.assertTrue(alertSettingsPage.getGeoFenceAlertToggleValue().equals("1"),
				"The geo fence alert toggle not getting on");

		// Validate delete curfew alert
		geoFenceAlertPage.selectAndDeleteGeoFence(GEOFENCE_NEW_NAME);

		alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);

		System.out.println("Alert Save Message:" + alertPopupMessage);

		Assert.assertTrue(alertPopupMessage.contains("successfully updated"),
				"The Create Geo Fence Alert command was not successful");

		alertSettingsPage.clickOnOkButton();

		Assert.assertTrue(!geoFenceAlertPage.selectGeoFenceDisplay(GEOFENCE_NEW_NAME),
				"The Create Geo-Fence Alert not deleted successful");
		alertSettingsPage.clickOnSaveButton();
		empAppDevice.sleepFor(25000);
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

		List<MobileElement> geoFenceValueAfterDelete = alertSettingsPage.getGeoFenceAlertAddedValuesList();
		System.out.println(geoFenceValueAfter);

		Assert.assertTrue(geoFenceValueAfterDelete.size() == geoFenceValueBefore.size() - 1,
				"Geo Fence Alert Value affect the count of previous geo fence alert deleting the new one");

	}

	@Test(priority = 10, description = "Edit Curfew Alert Test also validate the toggle does not getting off when we edit the value after putting toggle on ")
	public void editCurfewAlert() throws Exception {

		String ALERT_NAME = "Curfew Preference";

		String curfewFromDayOld = "Thursday";
		String curfewToDayOld = "Friday";
		String curfewFromHourOld = "8";
		String curfewToHourOld = "9";
		String curfewFromMinuteOld = "30";
		String curfewToMinuteOld = "45";
		String curfewFromAmPmOld = "AM";
		String curfewToAmPmOld = "PM";

		String curfewFromDay = "Wednesday";
		String curfewToDay = "Thursday";
		String curfewFromHour = "3";
		String curfewToHour = "4";
		String curfewFromMinute = "15";
		String curfewToMinute = "45";
		String curfewFromAmPm = "PM";
		String curfewToAmPm = "AM";

		String curfewFromDataOld = curfewFromDayOld + ", " + curfewFromHourOld + ":" + curfewFromMinuteOld + " "
				+ curfewFromAmPmOld;
		String curfewToDataOld = curfewToDayOld + ", " + curfewToHourOld + ":" + curfewToMinuteOld + " "
				+ curfewToAmPmOld;
		System.out.println(curfewFromDataOld + " " + curfewToDataOld);

		String curfewFromData = curfewFromDay + ", " + curfewFromHour + ":" + curfewFromMinute + " " + curfewFromAmPm;
		String curfewToData = curfewToDay + ", " + curfewToHour + ":" + curfewToMinute + " " + curfewToAmPm;
		System.out.println(curfewFromData + " " + curfewToData);

		loginConditions();
		dashboardPage.isVehicleStatusButtonDisplaye();
		// navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();

		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get initial Toggle value of Curfew Alert
		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getCurfewAlertToggleValue();
		System.out.println("Curfew Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
		// Switching off curfew alert toggle
		if (TOGGLE_VALUE_BEFORE_UPDATE.equals("1")) {
			alertSettingsPage.clickCurfewAlertToggle();
			alertSettingsPage.clickOnSaveButton();
			empAppDevice.sleepFor(25000);
			dashboardPage.openMenu();
			menuPage.navigateToAlertSettings();
			alertSettingsPage.isAlertSettingTitleDisplayed();
			Assert.assertTrue(alertSettingsPage.getCurfewAlertToggleValue().equals("0"),
					"The curfew alert toggle not getting off");
			TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getCurfewAlertToggleValue();
		}

		Assert.assertTrue(alertSettingsPage.getCurfewAlertToggleValue().equals("0"),
				"The curfew alert toggle not getting off");
		alertSettingsPage.clickCurfewAlertToggle();

		Assert.assertTrue(alertSettingsPage.getCurfewAlertToggleValue().equals("0"),
				"The curfew alert toggle not getting on");

		List<MobileElement> curfewAlertValueBefore = alertSettingsPage.getCurfewAlertAddedValuesList();
		System.out.println(curfewAlertValueBefore);

		System.out.println(curfewAlertPage.isMentioedCurfewDisplay(curfewFromDataOld, curfewToDataOld));

		if (!curfewAlertPage.isMentioedCurfewDisplay(curfewFromDataOld, curfewToDataOld)) {

			// navigate to Curfew alert Settings Page
			alertSettingsPage.openCurfewAlertPage();

			curfewAlertPage.isPresent();

			curfewAlertPage.setCurfewFromValues(curfewFromDayOld, curfewFromHourOld, curfewFromMinuteOld,
					curfewFromAmPmOld);

			curfewAlertPage.setCurfewToValues(curfewToDayOld, curfewToHourOld, curfewToMinuteOld, curfewToAmPmOld);

			curfewAlertPage.clickOnSaveButton();
			String alertPopupMessage_Add = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);

			System.out.println("Alert Save Message:" + alertPopupMessage_Add);

			Assert.assertTrue(alertPopupMessage_Add.contains("successfully updated"),
					"The Create Curfew Alert command was not successful");
			alertSettingsPage.clickOnOkButton();

		}

		// navigate to Curfew alert Settings Page
		curfewAlertPage.selectCurfew(curfewFromDataOld, curfewToDataOld);

		curfewAlertPage.setCurfewFromValues(curfewFromDay, curfewFromHour, curfewFromMinute, curfewFromAmPm);

		curfewAlertPage.setCurfewToValues(curfewToDay, curfewToHour, curfewToMinute, curfewToAmPm);

		curfewAlertPage.clickOnSaveButton();

		if (!alertSettingsPage.isOkButtonDisplay()) {
			curfewAlertPage.selectCurfew(curfewFromData, curfewToData);
			;
			alertSettingsPage.clickOnSaveButton();
		}
		alertSettingsPage.isOkButtonDisplay();

		String alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);

		System.out.println("Alert Save Message:" + alertPopupMessage);

		Assert.assertTrue(alertPopupMessage.contains("successfully updated"),
				"The Create Curfew Alert command was not successful");

		alertSettingsPage.clickOnOkButton();

		Assert.assertTrue(!curfewAlertPage.isMentioedCurfewDisplay(curfewFromDataOld, curfewToDataOld),
				"The Previous Curfew Alert command not edited successful");

		Assert.assertTrue(curfewAlertPage.isMentioedCurfewDisplay(curfewFromData, curfewToData),
				"The New Curfew Alert command not present or edited successful");

		List<MobileElement> curfewAlertValueAfter = alertSettingsPage.getCurfewAlertAddedValuesList();
		System.out.println(curfewAlertValueAfter);

		Assert.assertTrue(curfewAlertValueAfter.size() == curfewAlertValueBefore.size(),
				"Curfew Alert Value get affected the count of previous curfew alert after editing the new one");

		Assert.assertTrue(alertSettingsPage.getCurfewAlertToggleValue().equals("0"),
				"The curfew alert toggle not getting on");

		// Validate delete curfew alert

		curfewAlertPage.selectCurfewAndDelete(curfewFromData, curfewToData);

		alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);

		System.out.println("Alert Save Message:" + alertPopupMessage);

		Assert.assertTrue(alertPopupMessage.contains("successfully updated"),
				"The Create Curfew Alert command was not successful");

		alertSettingsPage.clickOnOkButton();

		Assert.assertTrue(!curfewAlertPage.isMentioedCurfewDisplay(curfewFromData, curfewToData),
				"The New Curfew Alert command not present or edited successful");
		alertSettingsPage.clickOnSaveButton();
		empAppDevice.sleepFor(25000);
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

		List<MobileElement> curfewAlertValueAfterDelete = alertSettingsPage.getCurfewAlertAddedValuesList();
		System.out.println(curfewAlertValueAfter);

		Assert.assertTrue(curfewAlertValueAfterDelete.size() == curfewAlertValueBefore.size() - 1,
				"Curfew Alert Value get affected the count of previous curfew alert after deleting the new one");

	}

	@Test(description = "Validate switch on the speed toggle and then update the value the toggle remain on")
	public void testspeedAlertToggleAfterUpdate() throws Exception {
		String ALERT_NAME = "Speed Alert Preference";
		String SPEED_VALUE1 = "90 mph";
		String SPEED_VALUE2 = "45 mph";
		// Login to the application
		loginConditions();
		// Navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();
		// Switching off the speed alert toggle
		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getSpeedAlertToggleValue();
		System.out.println("Speed Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
		if (TOGGLE_VALUE_BEFORE_UPDATE.equals("1")) {
			alertSettingsPage.clickSpeedAlertToggle();
			alertSettingsPage.clickOnSaveButton();
			empAppDevice.sleepFor(25000);
			dashboardPage.openMenu();
			menuPage.navigateToAlertSettings();
			alertSettingsPage.isAlertSettingTitleDisplayed();
			Assert.assertTrue(alertSettingsPage.getSpeedAlertToggleValue().equals("0"),
					"The speed alert toggle not getting off");
			TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getSpeedAlertToggleValue();
		}
		System.out.println("Speed Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
		Assert.assertTrue(alertSettingsPage.getSpeedAlertToggleValue().equals("0"),
				"The speed alert toggle not getting off");

		alertSettingsPage.clickSpeedAlertToggle();
		Assert.assertTrue(alertSettingsPage.getSpeedAlertToggleValue().equals("1"),
				"The speed alert toggle not getting on");

		String selectedSpeedBeforeUpdate = speedAlertPage.getSpeedValue();

		// navigate to Speed alert Settings Page and verify by updating the speed value
		alertSettingsPage.openSpeedAlertPage();
		speedAlertPage.isPresent();
		if (selectedSpeedBeforeUpdate.equals(SPEED_VALUE1)) {
			speedAlertPage.setSpeedValue(SPEED_VALUE2);
		} else {
			speedAlertPage.setSpeedValue(SPEED_VALUE1);
		}
		String updatedSpeedValue = speedAlertPage.getSpeedValue();
		Assert.assertTrue(!selectedSpeedBeforeUpdate.equals(updatedSpeedValue),
				"The Edit Speed value not updated successful");
		speedAlertPage.clickOnSaveButton();

		// Validation after perform save at speed alert page
		String alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);
		alertSettingsPage.clickOnOkButton();
		System.out.println("Alert Save Message:" + alertPopupMessage);
		Assert.assertTrue(alertPopupMessage.contains("successfully updated"),
				"The Edit Speed Alert command was not successful");
		String speedValueAfterUpdate = speedAlertPage.getSpeedValue();
		Assert.assertTrue(!selectedSpeedBeforeUpdate.equals(speedValueAfterUpdate),
				"The Edit Speed value not updated successful");
		Assert.assertTrue(alertSettingsPage.getSpeedAlertToggleValue().equals("1"),
				"The speed alert toggle not getting on");
	}

	@Test(description = "Validate switch on the valet toggle and then update the value the toggle remain on")
	public void testValetAlertAfterUpdate() throws Exception {
		String ALERT_NAME = "Valet Alert Preference";
		String VALET_VALUE1 = "0.5 miles";
		String VALET_VALUE2 = "0.75 miles";
		loginConditions();
		// navigate to alert Settings Page
		dashboardPage.openMenu();
		menuPage.navigateToAlertSettings();
		alertSettingsPage.isAlertSettingTitleDisplayed();

		// get initial Toggle value of Valet Alert
		String TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getValetAlertToggleValue();
		System.out.println("Valet Alert Toggle Value before Update:" + TOGGLE_VALUE_BEFORE_UPDATE);
		if (TOGGLE_VALUE_BEFORE_UPDATE.equals("1")) {
			alertSettingsPage.clickValetAlertToggle();
			alertSettingsPage.clickOnSaveButton();
			empAppDevice.sleepFor(25000);
			dashboardPage.openMenu();
			menuPage.navigateToAlertSettings();
			alertSettingsPage.isAlertSettingTitleDisplayed();
			Assert.assertTrue(alertSettingsPage.getValetAlertToggleValue().equals("0"),
					"The valet alert toggle not getting off");
			TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getValetAlertToggleValue();
		}
		TOGGLE_VALUE_BEFORE_UPDATE = alertSettingsPage.getValetAlertToggleValue();
		Assert.assertTrue(alertSettingsPage.getValetAlertToggleValue().equals("0"),
				"The valet alert toggle not getting off");
		alertSettingsPage.clickValetAlertToggle();
		Assert.assertTrue(alertSettingsPage.getValetAlertToggleValue().equals("1"),
				"The valet alert toggle not getting on");
		String valetValueBeforeUpdate = valetAlertPage.getValetValue();

		// navigate to Valet alert Settings Page
		alertSettingsPage.openValetAlertPage();
		valetAlertPage.isPresent();
		if (valetValueBeforeUpdate.equals(VALET_VALUE1)) {
			valetAlertPage.setValetValue(VALET_VALUE2);
		} else {
			valetAlertPage.setValetValue(VALET_VALUE1);
		}
		Assert.assertTrue(!valetValueBeforeUpdate.equals(valetAlertPage.getValetValue()),
				"The Edit Valet value updated successful");
		valetAlertPage.clickOnSaveButton();

		// Validation after perform save at valet alert page
		String alertPopupMessage = alertSettingsPage.getPopupSuccessMessageText(ALERT_NAME);
		alertSettingsPage.clickOnOkButton();
		System.out.println("Alert Save Message:" + alertPopupMessage);

		Assert.assertTrue(alertPopupMessage.contains("successfully updated"),
				"The Edit Valet Alert command was not successful");
		String valetValueAfterUpdate = valetAlertPage.getValetValue();
		Assert.assertTrue(!valetValueBeforeUpdate.equals(valetValueAfterUpdate),
				"The Edit Valet value updated successful");
		Assert.assertTrue(alertSettingsPage.getValetAlertToggleValue().equals("1"),
				"The valet alert toggle not getting on");
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

	@AfterMethod
	public void afterMethod() {

		if (alertSettingsPage.isBackButtonDisplay()) {
			alertSettingsPage.clickOnBackButton();
		}

	}

}
