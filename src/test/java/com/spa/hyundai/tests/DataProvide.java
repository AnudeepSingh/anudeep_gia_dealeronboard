package com.spa.hyundai.tests;

import org.testng.annotations.DataProvider;

import com.myh_genesis_spa.utils.ExcelReader;

public class DataProvide {

    @DataProvider(name = "userDataProvider")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		System.out.println("Test");
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "Users");
	}
}
