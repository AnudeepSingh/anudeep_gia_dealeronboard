package com.spa.hyundai.tests;

import java.io.IOException;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.auto.framework.base.Log;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.DealerLocatorPage;
import com.spa.hyundai.pageObjects.EnterPinPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.MapPage;
import com.spa.hyundai.pageObjects.MenuPage;
import com.spa.hyundai.pageObjects.SettingsPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class MapTest extends BaseTest {
	private String strVehicleType, strTestRun, strEmail, strPassword, strPin, strVin = "";
	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static DealerLocatorPage dealerLocatorPage;
	private static EnterPinPage enterPinPage;
	private static LoginPage loginPage;
	private static DashboardPage dashboardPage;
	private static MapPage mapPage;

	@Factory(dataProvider = "TestLoginData")
	public MapTest(String strVehicleType, String strTestRun, String strEmail, String strPassword, String strPin,
			String strVin) {

		this.strVehicleType = strVehicleType;
		this.strTestRun = strTestRun;
		this.strEmail = strEmail;
		this.strPassword = strPassword;
		this.strPin = strPin;
		this.strVin = strVin;

	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "Users");
	}

	@BeforeClass(alwaysRun = true)
	public void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		dealerLocatorPage = new DealerLocatorPage(empAppDevice);
		enterPinPage = new EnterPinPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		driver = empAppDevice.getDriver();
		dashboardPage = new DashboardPage(empAppDevice);
		mapPage = new MapPage(empAppDevice);
		System.out.println(strEmail);
	}

	public void relaunchAppAndInitializedVariable() throws Exception {
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		dealerLocatorPage = new DealerLocatorPage(empAppDevice);
		enterPinPage = new EnterPinPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		driver = empAppDevice.getDriver();
		dashboardPage = new DashboardPage(empAppDevice);
		mapPage = new MapPage(empAppDevice);
	}

	public void loginConditions() throws Exception {
		try {
			System.out.println("Logged In using : " + strEmail);
			if (!dashboardPage.isVehicleStatusButtonDisplayeWithinExpectedTime(2)) {
				loginPage.validLogin(strEmail, strPassword, strVin);
			    dashboardPage.VehicleStatusButtonDisplayStatusWithoutHandelException();
				System.out.println(dashboardPage.VehicleStatusButtonDisplayStatusWithoutHandelException());
				//i++;
				System.out.println("Logged In Fisr Time With : " + strEmail);
			}
			else {
				System.out.println("Already Login with " + strEmail);
			}
		}

		catch (NoSuchElementException ex) {
			System.out.println("Logged In Again Due To No Such Element Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		} catch (WebDriverException e) {
			System.out.println("Logged In Again Due To Wed Driver Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		}

	}

	@Test(description = "Search POI by passing zipcode as search parameter also doing send to car, make it fav and then delete it")
	public void testDeletFavPOIFromSearchPOI() throws Exception

	{
		String STR_SEARCHTEXT = "90101";
		loginConditions();
		dashboardPage.isVehicleStatusButtonDisplaye();
		dashboardPage.navigateToMapPageAndClickSearchPOI();

		mapPage.isSearchBarDisplay();

		mapPage.enterSearchText(STR_SEARCHTEXT);

		String strMapLocationAddress = mapPage.getMapLocationAddress();
		String strMapLocationAddressLabel = mapPage.getMapLocationAddressLabel();
		System.out.println(strMapLocationAddressLabel);

		Assert.assertTrue(strMapLocationAddress.replace(" ", "").contains(STR_SEARCHTEXT), "Address Not Matched");

		boolean addressExpandableList = mapPage.verifyAddressInExpandableList(strMapLocationAddressLabel,
				STR_SEARCHTEXT, strMapLocationAddress.split(" ")[0]);

		Assert.assertTrue(addressExpandableList, "Address not present in expendable list");

		System.out.println(mapPage.getMapLocationAddress());

		Assert.assertTrue(strMapLocationAddress.equals(mapPage.getMapLocationAddress()),
				"Address after selecting from expendable list not matched with the prvious searched address");

		String strMessage = mapPage.clickOnSaveIcon();

		System.out.println(strMessage);

		if (strMessage.contains("POI successfully added")) {

			Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
		}

		else {
			Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
					"POI is not already saved.");

		}
		String strDeletePoiMessage = mapPage.clickOnSaveIcon();

		System.out.println(strDeletePoiMessage);
		Assert.assertTrue(strDeletePoiMessage.contains("POI successfully deleted"), "POI Not deleted");

		mapPage.clickSendToCar();
		Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");
		dashboardPage.clickOnPopupOKButton();
	}

	@Test(description = "Search and save POI into my POI with send to car and then delete the poi ")
	public void testSearchAndMyPOI() throws Exception {
		String STR_SEARCHTEXT = "90001";

		loginConditions();
		dashboardPage.isVehicleStatusButtonDisplaye();
		dashboardPage.navigateToMapPageAndClickSearchPOI();
		Assert.assertTrue(mapPage.isSearchBarDisplay(), "Search Bar not displayed");
		mapPage.enterSearchText(STR_SEARCHTEXT);

		String strMapLocationAddress = mapPage.getMapLocationAddress();
		System.out.println(strMapLocationAddress);
		Assert.assertTrue(strMapLocationAddress.replace(" ", "").contains(STR_SEARCHTEXT), "Address Not Matched");

		String strMessage = mapPage.clickOnSaveIcon();

		System.out.println(strMessage);

		if (strMessage.contains("POI successfully added")) {

			Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
		}

		else {
			Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
					"POI is not already saved.");

		}

		mapPage.clickOnBackButton();
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickFavorites();
		boolean savedPOI = mapPage.clickSavedPOI(strMapLocationAddress);

		Assert.assertTrue(savedPOI, "POI is not present.");

		System.out.println(mapPage.getMapLocationAddress());

		Assert.assertTrue(strMapLocationAddress.equals(mapPage.getMapLocationAddress()),
				"Address after selecting from expendable list not matched with the prvious searched address");

		mapPage.clickSendToCar();
		Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");
		dashboardPage.clickOnPopupOKButton();
		String strDeletePoiMessage = mapPage.clickOnSaveIcon();

		System.out.println(strDeletePoiMessage);
		Assert.assertTrue(strDeletePoiMessage.contains("POI successfully deleted"), "POI Not deleted");

	}

	@Test(description = "Test car finder with invalid and valid pin")
	public void testCarFinderWithInvalidValidPin() throws Exception {
		loginConditions();
		dashboardPage.isVehicleStatusButtonDisplaye();
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickCarFinderOption();
		enterPinPage.enterPin("1222");

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getRemoteSuccessMessageText("Incorrect PIN");

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("Incorrect PIN"), "The incorrect pin popup not appears");

		dashboardPage.clickOnPopupOKButton();
		dashboardPage.clickNavigateBack();
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickCarFinderOption();
		System.out.println();
		enterPinPage.enterPin(strPin.replace(".0", ""));
		empAppDevice.sleepFor(10000);

	}

	@Test(description = "Search dealer POI by passing zipcode as search parameter and set poi as prefferd location and then perform send to car")
	public void testSearchDealerPOI() throws Exception

	{
		String STR_SEARCHTEXT = "93551";

		loginConditions();
		dashboardPage.isVehicleStatusButtonDisplaye();
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickHyundaiDealersOption();

		Assert.assertTrue(mapPage.isDealerLocatorPageDisplayed(), "Dealer Locator page is not displaying.");

		mapPage.isSearchBarDisplay();

		mapPage.enterSearchText(STR_SEARCHTEXT);
		String strAddress = mapPage.getMapLocationAddress();
		Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");

		boolean addressExpandableList = mapPage.verifyAddressInExpandableList(strAddress);

		Assert.assertTrue(addressExpandableList, "Address not present in expendable list");

		mapPage.clickSendToCar();
		Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");
		dashboardPage.clickOnPopupOKButton();
		String strSetPrefferedLocationMessage = mapPage.clickOnSaveIcon();

		System.out.println(strSetPrefferedLocationMessage);
		Assert.assertTrue(
				strSetPrefferedLocationMessage
						.contains("Your preferred Hyundai Dealer Service Center has been updated."),
				"Set Preffered location message not displayed");

	}

	@Test(description = "Search near by fuel POI by passing zipcode as search parameter and make poi fav and then check that poi under fav tab ")
	public void testNearByFuelPOI() throws Exception

	{
		System.out.println(strVehicleType);
		if (strVehicleType.equalsIgnoreCase("GAS") || strVehicleType.equalsIgnoreCase("PHEV")
				|| strVehicleType.equalsIgnoreCase("HEV")) {

			String STR_SEARCHTEXT = "92128";

			loginConditions();
			dashboardPage.isVehicleStatusButtonDisplaye();
			dashboardPage.navigateToMapOptionPage();

			mapPage.clickNearByFuel();

			mapPage.isSearchBarDisplay();

			mapPage.enterSearchText(STR_SEARCHTEXT);
			String strAddress = mapPage.getMapLocationAddress();
			String strAddressLabel = mapPage.getMapLocationAddressLabel();
			System.out.println(strAddress);
			Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");

			boolean addressExpandableList = mapPage.verifyAddressInExpandableList(strAddressLabel, STR_SEARCHTEXT,
					strAddress.split(" ")[0]);

			Assert.assertTrue(addressExpandableList, "Address not present in expendable list");
			System.out.println(mapPage.getMapLocationAddress());

			Assert.assertTrue(strAddress.equals(mapPage.getMapLocationAddress()),
					"Address after selecting from expendable list not matched with the prvious searched address");

			String strMessage = mapPage.clickOnSaveIcon();

			System.out.println(strMessage);

			if (strMessage.contains("POI successfully added")) {

				Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
			}

			else {
				Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
						"POI is not already saved.");

			}

			mapPage.clickOnBackButton();
			dashboardPage.navigateToMapOptionPage();
			mapPage.clickFavorites();
			boolean savedPOI = mapPage.clickSavedPOI(strAddress);

			Assert.assertTrue(savedPOI, "POI is not present.");

			System.out.println(mapPage.getMapLocationAddress());

			Assert.assertTrue(strAddress.equals(mapPage.getMapLocationAddress()),
					"Address after selecting from expendable list not matched with the prvious searched address");
			mapPage.clickOnSaveIcon();

		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Search h2 station POI by passing zipcode as search parameter and make poi fav and then check that poi under fav tab")
	public void testH2StationPOI() throws Exception

	{
		System.out.println(strVehicleType);
		if (strVehicleType.equalsIgnoreCase("NEXO")) {

			String STR_SEARCHTEXT = "12105";

			loginConditions();
			dashboardPage.isVehicleStatusButtonDisplaye();
			dashboardPage.navigateToMapOptionPage();

			mapPage.clickH2Stations();

			mapPage.isSearchBarDisplay();

			mapPage.enterSearchText(STR_SEARCHTEXT);
			String strAddress = mapPage.getMapLocationAddress();
			String strAddressLabel = mapPage.getMapLocationAddressLabel();
			System.out.println(strAddress);
			Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");

			boolean addressExpandableList = mapPage.verifyAddressInExpandableList(strAddressLabel, STR_SEARCHTEXT,
					strAddress.split(" ")[0]);

			Assert.assertTrue(addressExpandableList, "Address not present in expendable list");

			System.out.println(mapPage.getMapLocationAddress());

			Assert.assertTrue(strAddress.equals(mapPage.getMapLocationAddress()),
					"Address after selecting from expendable list not matched with the prvious searched address");

			String strMessage = mapPage.clickOnSaveIcon();

			System.out.println(strMessage);

			if (strMessage.contains("POI successfully added")) {

				Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
			}

			else {
				Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
						"POI is not already saved.");

			}

			mapPage.clickOnBackButton();
			dashboardPage.navigateToMapOptionPage();
			mapPage.clickFavorites();
			boolean savedPOI = mapPage.clickSavedPOI(strAddress);

			Assert.assertTrue(savedPOI, "POI is not present.");

			System.out.println(mapPage.getMapLocationAddress());

			Assert.assertTrue(strAddress.equals(mapPage.getMapLocationAddress()),
					"Address after selecting from expendable list not matched with the prvious searched address");
			mapPage.clickOnSaveIcon();
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Search charge staion POI by passing zipcode as search parameter and make poi fav and then check that poi under fav tab")
	public void testChargeStaionElement() throws Exception

	{
		if (strVehicleType.equalsIgnoreCase("EV")) {
			String STR_SEARCHTEXT = "33180";

			loginConditions();
			dashboardPage.isVehicleStatusButtonDisplaye();
			empAppDevice.sleepFor(10000);
			dashboardPage.navigateToMapOptionPage();

			mapPage.clickChargeStations();

			// dashboardPage.clickOnRemoteCommandOKButton();

			mapPage.isSearchBarDisplay();

			Assert.assertTrue(mapPage.isTxtDisplayedUnderSearchTextBox(),
					"Enter Zip or City Not Display in search box");

			Assert.assertTrue(mapPage.isIconMapNewCarFinderDisplay(), "Map New Car Finder Not Display");

			Assert.assertTrue(mapPage.isIconMapNewLocationDisplay(), "Map Map New Location Not Display");

			mapPage.enterSearchText(STR_SEARCHTEXT);

			String strAddress = mapPage.getMapLocationAddress();
			String strAddressLabel = mapPage.getMapLocationAddressLabel();
			System.out.println(strAddress);
			Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");

			boolean addressExpandableList = mapPage.verifyAddressInExpandableList(strAddressLabel, STR_SEARCHTEXT,
					strAddress.split(" ")[0]);

			Assert.assertTrue(addressExpandableList, "Address not present in expendable list");

			System.out.println(mapPage.getMapLocationAddress());

			Assert.assertTrue(strAddress.equals(mapPage.getMapLocationAddress()),
					"Address after selecting from expendable list not matched with the prvious searched address");

			String strMessage = mapPage.clickOnSaveIcon();

			System.out.println(strMessage);

			if (strMessage.contains("POI successfully added")) {

				Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
			}

			else {
				Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
						"POI is not already saved.");

			}

			mapPage.clickOnBackButton();
			dashboardPage.navigateToMapOptionPage();
			mapPage.clickFavorites();
			boolean savedPOI = mapPage.clickSavedPOI(strAddress);

			Assert.assertTrue(savedPOI, "POI is not present.");

			System.out.println(mapPage.getMapLocationAddress());

			Assert.assertTrue(strAddress.equals(mapPage.getMapLocationAddress()),
					"Address after selecting from expendable list not matched with the prvious searched address");
			mapPage.clickOnSaveIcon();
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Search near by fuel POI by passing zipcode as search parameter  and then perform send to car")
	public void testNearByFuelPOISentToCar() throws Exception

	{
		System.out.println(strVehicleType);
		if (strVehicleType.equalsIgnoreCase("GAS") || strVehicleType.equalsIgnoreCase("PHEV")
				|| strVehicleType.equalsIgnoreCase("HEV")) {

			String STR_SEARCHTEXT = "92128";

			loginConditions();
			dashboardPage.isVehicleStatusButtonDisplaye();
			dashboardPage.navigateToMapOptionPage();

			mapPage.clickNearByFuel();

			mapPage.isSearchBarDisplay();

			mapPage.enterSearchText(STR_SEARCHTEXT);

			mapPage.clickSendToCar();
			Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");
			dashboardPage.clickOnPopupOKButton();
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Search h2 station POI by passing zipcode as search parameter and then perform send to car")
	public void testH2StationPOISentToCar() throws Exception

	{
		System.out.println(strVehicleType);
		if (strVehicleType.equalsIgnoreCase("NEXO")) {

			String STR_SEARCHTEXT = "12105";

			loginConditions();
			dashboardPage.isVehicleStatusButtonDisplaye();
			dashboardPage.navigateToMapOptionPage();

			mapPage.clickH2Stations();

			mapPage.isSearchBarDisplay();

			mapPage.enterSearchText(STR_SEARCHTEXT);
			mapPage.clickSendToCar();
			Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");
			dashboardPage.clickOnPopupOKButton();
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Search charge staion POI by passing zipcode as search parameter and then perform send to car")
	public void testChargeStaionElementSentToCar() throws Exception

	{
		if (strVehicleType.equalsIgnoreCase("EV")) {
			String STR_SEARCHTEXT = "33180";
			loginConditions();
			dashboardPage.isVehicleStatusButtonDisplaye();
			empAppDevice.sleepFor(10000);
			dashboardPage.navigateToMapOptionPage();

			mapPage.clickChargeStations();

			// dashboardPage.clickOnRemoteCommandOKButton();

			mapPage.isSearchBarDisplay();

			mapPage.enterSearchText(STR_SEARCHTEXT);

			mapPage.clickSendToCar();
			Assert.assertTrue(mapPage.isPoiSuccessfullySentToCar(), "POI successfully sent to car alert not appear");
			dashboardPage.clickOnPopupOKButton();
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Search POI by passing zipcode as search parameter")
	public void testCollisionRepairCenter() throws Exception

	{

		loginConditions();
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickCollisionRepairCenter();

		Assert.assertTrue(mapPage.isCollisionRepairCenterPageDisplayed(), "Collision Repair Center page not displayed");
	}

	@Test(description = "Search near by fuel POI by passing zipcode as search parameter and then delete that poi ")
	public void testDeleteNearByFuelPOI() throws Exception

	{
		System.out.println(strVehicleType);
		if (strVehicleType.equalsIgnoreCase("GAS") || strVehicleType.equalsIgnoreCase("PHEV")
				|| strVehicleType.equalsIgnoreCase("HEV")) {

			String STR_SEARCHTEXT = "201009";

			loginConditions();
			dashboardPage.isVehicleStatusButtonDisplaye();
			dashboardPage.navigateToMapOptionPage();

			mapPage.clickNearByFuel();

			mapPage.isSearchBarDisplay();

			mapPage.enterSearchText(STR_SEARCHTEXT);
			String strAddress = mapPage.getMapLocationAddress();
			String strAddressLabel = mapPage.getMapLocationAddressLabel();
			System.out.println(strAddress);
			Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");

			boolean addressExpandableList = mapPage.verifyAddressInExpandableList(strAddressLabel, STR_SEARCHTEXT,
					strAddress.split(" ")[0]);

			Assert.assertTrue(addressExpandableList, "Address not present in expendable list");

			System.out.println(mapPage.getMapLocationAddress());

			Assert.assertTrue(strAddress.equals(mapPage.getMapLocationAddress()),
					"Address after selecting from expendable list not matched with the prvious searched address");

			String strMessage = mapPage.clickOnSaveIcon();

			System.out.println(strMessage);

			if (strMessage.contains("POI successfully added")) {

				Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
			}

			else {
				Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
						"POI is not already saved.");

			}

			String strDeletePoiMessage = mapPage.clickOnSaveIcon();

			System.out.println(strDeletePoiMessage);
			Assert.assertTrue(strDeletePoiMessage.contains("POI successfully deleted"), "POI Not deleted");

		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Search h2 station POI by passing zipcode as search parameter and then delete that poi")
	public void testDeleteH2StationPOI() throws Exception

	{
		System.out.println(strVehicleType);
		if (strVehicleType.equalsIgnoreCase("NEXO")) {

			String STR_SEARCHTEXT = "201009";

			loginConditions();
			dashboardPage.isVehicleStatusButtonDisplaye();
			dashboardPage.navigateToMapOptionPage();

			mapPage.clickH2Stations();

			mapPage.isSearchBarDisplay();

			mapPage.enterSearchText(STR_SEARCHTEXT);
			String strAddress = mapPage.getMapLocationAddress();
			String strAddressLabel = mapPage.getMapLocationAddressLabel();
			System.out.println(strAddress);
			Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");

			boolean addressExpandableList = mapPage.verifyAddressInExpandableList(strAddressLabel, STR_SEARCHTEXT,
					strAddress.split(" ")[0]);

			Assert.assertTrue(addressExpandableList, "Address not present in expendable list");

			System.out.println(mapPage.getMapLocationAddress());

			Assert.assertTrue(strAddress.equals(mapPage.getMapLocationAddress()),
					"Address after selecting from expendable list not matched with the prvious searched address");

			String strMessage = mapPage.clickOnSaveIcon();

			System.out.println(strMessage);

			if (strMessage.contains("POI successfully added")) {

				Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
			}

			else {
				Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
						"POI is not already saved.");

			}

			String strDeletePoiMessage = mapPage.clickOnSaveIcon();

			System.out.println(strDeletePoiMessage);
			Assert.assertTrue(strDeletePoiMessage.contains("POI successfully deleted"), "POI Not deleted");
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Search charge station POI by passing zipcode as search parameter and then delete that poi")
	public void testDeleteChargeStaionElement() throws Exception

	{
		if (strVehicleType.equalsIgnoreCase("EV")) {
			String STR_SEARCHTEXT = "20017";

			loginConditions();
			dashboardPage.isVehicleStatusButtonDisplaye();
			empAppDevice.sleepFor(10000);
			dashboardPage.navigateToMapOptionPage();

			mapPage.clickChargeStations();

			// dashboardPage.clickOnRemoteCommandOKButton();

			mapPage.isSearchBarDisplay();

			Assert.assertTrue(mapPage.isTxtDisplayedUnderSearchTextBox(),
					"Enter Zip or City Not Display in search box");

			Assert.assertTrue(mapPage.isIconMapNewCarFinderDisplay(), "Map New Car Finder Not Display");

			Assert.assertTrue(mapPage.isIconMapNewLocationDisplay(), "Map Map New Location Not Display");

			mapPage.enterSearchText(STR_SEARCHTEXT);

			String strAddress = mapPage.getMapLocationAddress();
			String strAddressLabel = mapPage.getMapLocationAddressLabel();
			System.out.println(strAddress);
			Assert.assertTrue(strAddress.contains(STR_SEARCHTEXT), "Address Not Matched");

			boolean addressExpandableList = mapPage.verifyAddressInExpandableList(strAddressLabel, STR_SEARCHTEXT,
					strAddress.split(" ")[0]);

			Assert.assertTrue(addressExpandableList, "Address not present in expendable list");

			System.out.println(mapPage.getMapLocationAddress());

			Assert.assertTrue(strAddress.equals(mapPage.getMapLocationAddress()),
					"Address after selecting from expendable list not matched with the prvious searched address");

			String strMessage = mapPage.clickOnSaveIcon();

			System.out.println(strMessage);

			if (strMessage.contains("POI successfully added")) {

				Assert.assertTrue(strMessage.contains("POI successfully added"), "POI Not Added");
			}

			else {
				Assert.assertTrue(strMessage.contains("This location has already been saved to your favorites"),
						"POI is not already saved.");

			}
			String strDeletePoiMessage = mapPage.clickOnSaveIcon();

			System.out.println(strDeletePoiMessage);
			Assert.assertTrue(strDeletePoiMessage.contains("POI successfully deleted"), "POI Not deleted");

		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "verify disable location permission for search POI by passing zipcode as search parameter")
	public void testLocationServiceDisabledAtSearchPOI() throws Exception

	{
		relaunchAppAndInitializedVariable();
		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVin);
		dashboardPage.isVehicleStatusButtonDisplaye();
		;
		dashboardPage.navigateToMapPageAndClickSearchPOI();

		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
		//mapPage.clickCancelAlertButton();
	}

	@Test(description = "verify disable location permission for car finder")
	public void testLocationServiceDisabledAtCarFinder() throws Exception

	{
		relaunchAppAndInitializedVariable();
		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVin);
		dashboardPage.isVehicleStatusButtonDisplaye();
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickCarFinderOption();

		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
		//mapPage.clickCancelAlertButton();
	}

	@Test(description = "verify disable location permission for near by fuel POI by passing zipcode as search parameter")
	public void testLocationServiceDisabledAtNearByFuel() throws Exception

	{
		if (strVehicleType.equalsIgnoreCase("GAS") || strVehicleType.equalsIgnoreCase("PHEV")
				|| strVehicleType.equalsIgnoreCase("HEV")) {
			relaunchAppAndInitializedVariable();
			loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVin);
			dashboardPage.isVehicleStatusButtonDisplaye();
			dashboardPage.navigateToMapOptionPage();
			mapPage.clickNearByFuel();
			Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
					"Location Service Disabled alert not displayed");
			//mapPage.clickCancelAlertButton();
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}
	}

	@Test(description = "verify disable location permission for charge station POI by passing zipcode as search parameter")
	public void testLocationServiceDisabledAtChargeStation() throws Exception

	{
		if (strVehicleType.equalsIgnoreCase("EV")) {
			relaunchAppAndInitializedVariable();
			loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVin);
			dashboardPage.isVehicleStatusButtonDisplaye();
			dashboardPage.navigateToMapOptionPage();
			mapPage.clickChargeStations();
			Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
					"Location Service Disabled alert not displayed");
			//mapPage.clickCancelAlertButton();
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}
	}

	@Test(description = "verify disable location permission for h2 station POI by passing zipcode as search parameter")
	public void testLocationServiceDisabledAtH2Stations() throws Exception

	{
		if (strVehicleType.equalsIgnoreCase("NEXO")) {
			relaunchAppAndInitializedVariable();
			loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVin);
			dashboardPage.isVehicleStatusButtonDisplaye();
			dashboardPage.navigateToMapOptionPage();
			mapPage.clickH2Stations();
			Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
					"Location Service Disabled alert not displayed");
			//mapPage.clickCancelAlertButton();
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}
	}

	@Test(description = "verify disable location permission for gps icon POI by passing zipcode as search parameter")
	public void testLocationServiceDisabledWhenSelectGPSIcon() throws Exception

	{
		relaunchAppAndInitializedVariable();
		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVin);
		dashboardPage.isVehicleStatusButtonDisplaye();
		dashboardPage.navigateToMapPageAndClickSearchPOI();

		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");

		mapPage.clickCancelAlertButton();
		mapPage.clickGPSIcon();

		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
		//mapPage.clickCancelAlertButton();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

	@AfterMethod
	public void afterMethod() {

		if (mapPage.isBackButtonDisplay()) {
			mapPage.clickOnBackButton();
		}

	}

}
