package com.spa.hyundai.tests;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.MenuPage;
import com.spa.hyundai.pageObjects.SettingsPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SettingsTest extends BaseTest {
	private String strVehicleType, strTestRun, strEmail, strPassword, strPin, strVin = "";
	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static SettingsPage settingsPage;
	private static LoginPage loginPage;
	private static DashboardPage dashboardPage;
	private static MenuPage menuPage;
	private int i = 0;

	@Factory(dataProvider = "userDataProvider")
	public SettingsTest(String strVehicleType, String strTestRun, String strEmail, String strPassword, String strPin,
			String strVin) {

		this.strVehicleType = strVehicleType;
		this.strTestRun = strTestRun; 
		this.strEmail = strEmail;
		this.strPassword = strPassword;
		this.strPin = strPin;
		this.strVin = strVin;

	}

	@BeforeClass(alwaysRun = true)
	public void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		settingsPage = new SettingsPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		menuPage = new MenuPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);
		System.out.println(strEmail);
		i = 0;

	}

	public void relaunchAppAndInitializedVariable() throws Exception {
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		settingsPage = new SettingsPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		menuPage = new MenuPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);
	}

	@DataProvider(name = "userDataProvider")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();

		Object[][] t = excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "Users");

		return t;
	}

	public void loginConditions() throws Exception {
		try {
			System.out.println("Logged In using : " + strEmail);
			if (!dashboardPage.isVehicleStatusButtonDisplayeWithinExpectedTime(2)) {
				loginPage.validLogin(strEmail, strPassword, strVin);
				dashboardPage.VehicleStatusButtonDisplayStatusWithoutHandelException();
				System.out.println(dashboardPage.VehicleStatusButtonDisplayStatusWithoutHandelException());
				// i++;
				System.out.println("Logged In Fisr Time With : " + strEmail);
			} else {
				System.out.println("Already Login with " + strEmail);
			}
		}

		catch (NoSuchElementException ex) {
			System.out.println("Logged In Again Due To No Such Element Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		} catch (WebDriverException e) {
			System.out.println("Logged In Again Due To Wed Driver Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		}

	}
	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}

	@AfterMethod
	public void afterMethod() {

		if (settingsPage.isBackButtonDisplay()) {
			settingsPage.navigateBack();
		}


	}

	// Test Connected Care Settings
	@Test(description = "Connected Care- Pressing the toggle all SMS button in connected care toggles every connected care SMS setting")
	public void togglingAllConnectedCareSmsSetsAllSmsToSameState() throws Exception {

	      
                //  Reporter.log(s);
	    Assert.assertTrue(true);
		loginConditions();

		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");

		settingsPage.toggleAllNotification("Connected Care", 1);

		MobileElement toggleAllXpath = driver.findElement(By.xpath(
				"//XCUIElementTypeOther[@name='Connected Care']" + "//XCUIElementTypeButton[@name='empty box'][1]"));

		String toggleState = toggleAllXpath.getAttribute("enabled");

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("connected care");

		boolean notificationStates = true;
		for (MobileElement notificationText : allLinks) {

			if (!empAppDevice.isElementDisplayed(notificationText)
					|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(), "text") != toggleState) {
				notificationStates = false;
				break;
			}
		}

		Assert.assertTrue(notificationStates);

	}

	@Test(description = "Connected Care- Pressing the toggle all Email button in connected care toggles every connected care Email setting")
	public void togglingAllConnectedCareEmailSetsAllEmailsToSameState() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");

		settingsPage.toggleAllNotification("Connected Care", 2);

		MobileElement toggleAllXpath = driver.findElement(By.xpath(
				"//XCUIElementTypeOther[@name='Connected Care']" + "//XCUIElementTypeButton[@name='empty box'][1]"));

		String toggleState = toggleAllXpath.getAttribute("enabled");

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("connected care");

		boolean notificationStates = true;
		for (MobileElement notificationText : allLinks) {

			if (!empAppDevice.isElementDisplayed(notificationText)
					|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(), "email") != toggleState) {
				notificationStates = false;
				break;
			}
		}

		Assert.assertTrue(notificationStates);

	}


	@Test(description = "Connected Care- Pressing the toggle all App button in connected care toggles every connected care App setting")
	public void togglingAllConnectedCareAppSetsAllAppsToSameState() throws Exception {
		loginConditions();

		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");

		settingsPage.toggleAllNotification("Connected Care", 3);

		MobileElement toggleAllXpath = driver.findElement(By.xpath(
				"//XCUIElementTypeOther[@name='Connected Care']" + "//XCUIElementTypeButton[@name='empty box'][1]"));

		String toggleState = toggleAllXpath.getAttribute("enabled");

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("connected care");

		boolean notificationStates = true;
		for (MobileElement notificationText : allLinks) {

			if (!empAppDevice.isElementDisplayed(notificationText)
					|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(), "phone") != toggleState) {
				notificationStates = false;
				break;
			}
		}

		Assert.assertTrue(notificationStates);
	}

	@Test(description = "Connected Care- Verify ACN sms toggle changes sms setting")
	public void acnSmsToggleChangesSmsSetting() {
		System.out.println(strEmail);
		String _vin = "KM8J73A60JU000204";

		if (!dashboardPage.isVehicleStatusButtonDisplaye()) {
			loginPage.validLogin(strEmail, strPassword, _vin);
		}
		System.out.println("Logged In using : " + strEmail);
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC COLLISION NOTIFICATION (ACN)", "text");

	}

	@Test(description = "Connected Care- Verify SOS sms toggle changes sms setting")
	public void sosSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("SOS EMERGENCY ASSISTANCE", "text");

	}

	@Test(description = "Connected Care- Verify Automatic Dtc sms toggle changes sms setting")
	public void automaticDtcSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC DTC", "text");

	}

	@Test(description = "Connected Care- Verify Monthly vehicle health report sms toggle changes sms setting")
	public void mvhrSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("MONTHLY VEHICLE HEALTH REPORT", "text");

	}

	@Test(description = "Connected Care- Verify Maintenance Alert sms toggle changes sms setting")
	public void maintenanceAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("MAINTENANCE ALERT", "text");

	}

	@Test(description = "Connected Care- Verify ACN sms toggle changes sms setting")
	public void acnEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC COLLISION NOTIFICATION (ACN)", "email");

	}

	@Test(description = "Connected Care- Verify SOS Email toggle changes Email setting")
	public void sosEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("SOS EMERGENCY ASSISTANCE", "email");

	}

	@Test(description = "Connected Care- Verify Automatic Dtc Email toggle changes Email setting")
	public void automaticDtcEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC DTC", "email");

	}

	@Test(description = "Connected Care- Verify Monthly vehicle health report Email toggle changes Email setting")
	public void mvhrEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("MONTHLY VEHICLE HEALTH REPORT", "email");

	}

	@Test(description = "Connected Care- Verify Maintenance Alert Email toggle changes Email setting")
	public void maintenanceAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("MAINTENANCE ALERT", "email");
	}

	@Test(description = "Connected Care- Verify ACN Phone toggle changes Phone setting")
	public void acnPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC COLLISION NOTIFICATION (ACN)", "phone");

	}

	@Test(description = "Connected Care- Verify SOS Phone toggle changes Phone setting")
	public void sosPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("SOS EMERGENCY ASSISTANCE", "phone");

	}

	@Test(description = "Connected Care- Verify Automatic Dtc Phone toggle changes Phone setting")
	public void automaticDtcPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC DTC", "phone");

	}

	@Test(description = "Connected Care- Verify Monthly vehicle health report Phone toggle changes Phone setting")
	public void mvhrPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("MONTHLY VEHICLE HEALTH REPORT", "phone");

	}

	@Test(description = "Connected Care- Verify Maintenance Alert Phone toggle changes Phone setting")
	public void maintenanceAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Connected Care");
		settingsPage.clickOnToggleButtonForNotification("MAINTENANCE ALERT", "phone");
	}

	// Test Remote Settings
	@Test(description = "Remote- Pressing the toggle all SMS button in Remote toggles every Remote SMS setting")
	public void togglingAllRemoteSmsSetsAllSmssToSameState() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.toggleAllNotification("Remote", 1);

		MobileElement toggleAllXpath = driver.findElement(
				By.xpath("//XCUIElementTypeOther[@name='Remote']" + "//XCUIElementTypeButton[@name='empty box'][1]"));

		String toggleState = toggleAllXpath.getAttribute("enabled");
		empAppDevice.scrollDown();

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("Remote");

		boolean notificationStates = true;
		for (MobileElement notificationText : allLinks) {

			if (!empAppDevice.isElementDisplayed(notificationText)
					|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(), "text") != toggleState) {
				notificationStates = false;
				break;
			}
		}
		Assert.assertTrue(notificationStates);
	}

	@Test(description = "Remote- Pressing the toggle all Email button in Remote toggles every Remote Email setting")
	public void togglingAllRemoteEmailSetsAllEmailsToSameState() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.toggleAllNotification("Remote", 2);

		MobileElement toggleAllXpath = driver.findElement(
				By.xpath("//XCUIElementTypeOther[@name='Remote']" + "//XCUIElementTypeButton[@name='empty box'][2]"));

		String toggleState = toggleAllXpath.getAttribute("enabled");
		empAppDevice.scrollDown();

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("Remote");

		boolean notificationStates = true;
		for (MobileElement notificationText : allLinks) {

			if (!empAppDevice.isElementDisplayed(notificationText)
					|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(), "email") != toggleState) {
				notificationStates = false;
				break;
			}
		}
		Assert.assertTrue(notificationStates);
	}

	@Test(description = "Remote- Pressing the toggle all App button in Remote toggles every Remote App setting")
	public void togglingAllRemoteAppSetsAllAppsToSameState() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.toggleAllNotification("Remote", 3);

		MobileElement toggleAllXpath = driver.findElement(
				By.xpath("//XCUIElementTypeOther[@name='Remote']" + "//XCUIElementTypeButton[@name='empty box'][3]"));

		String toggleState = toggleAllXpath.getAttribute("enabled");
		empAppDevice.scrollDown();

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("Remote");

		boolean notificationStates = true;
		for (MobileElement notificationText : allLinks) {

			if (!empAppDevice.isElementDisplayed(notificationText)
					|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(), "phone") != toggleState) {
				notificationStates = false;
				break;
			}
		}
		Assert.assertTrue(notificationStates);
	}

	@Test(description = "Remote- Verify panic Notification sms toggle changes sms setting")
	public void panicNotificationSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("PANIC NOTIFICATION", "text");
	}

	@Test(description = "Remote- Verify Alarm Notification sms toggle changes sms setting")
	public void alarmNotificationSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("ALARM NOTIFICATION", "text");
	}

	@Test(description = "Remote- Verify Horn And Lights sms toggle changes sms setting")
	public void hornAndLightsSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("HORN AND LIGHTS/LIGHTS ONLY", "text");
	}

	@Test(description = "Remote- Verify Remote Engine Start Stop sms toggle changes sms setting")
	public void remoteEngineStartStopSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("REMOTE ENGINE START/STOP", "text");
	}

	@Test(description = "Remote- Verify Remote Door Lock Unlock sms toggle changes sms setting")
	public void remoteDoorLockUnlockSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("REMOTE DOOR LOCK/UNLOCK", "text");
	}

	@Test(description = "Remote- Verify Valet Alert sms toggle changes sms setting")
	public void valetAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("VALET ALERT", "text");
	}

	@Test(description = "Remote- Verify Geo Fence Alert sms toggle changes sms setting")
	public void geoFenceAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("GEO-FENCE ALERT", "text");
	}

	@Test(description = "Remote- Verify Speed Alert sms toggle changes sms setting")
	public void speedAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("SPEED ALERT", "text");
	}

	@Test(description = "Remote- Verify Curfew Alert sms toggle changes sms setting")
	public void curfewAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("CURFEW ALERT", "text");
	}

	@Test(description = "Remote- Verify Head Unit Software Update sms toggle changes sms setting")
	public void headUnitSoftwareUpdateSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("HEAD UNIT SOFTWARE UPDATE", "text");
	}

	@Test(description = "Remote- Verify Engine Idle Alert sms toggle changes sms setting")
	public void engineIdleAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("ENGINE IDLE ALERT", "text");
	}

	@Test(description = "Remote- Verify panic Notification Email toggle changes Email setting")
	public void panicNotificationEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("PANIC NOTIFICATION", "email");
	}

	@Test(description = "Remote- Verify Alarm Notification Email toggle changes Email setting")
	public void alarmNotificationEmailToggleChangesEmailSetting() {
		System.out.println("Logged In using : " + strEmail);
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("ALARM NOTIFICATION", "email");
	}

	@Test(description = "Remote- Verify Horn And Lights Email toggle changes Email setting")
	public void hornAndLightsEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("HORN AND LIGHTS/LIGHTS ONLY", "email");
	}

	@Test(description = "Remote- Verify Remote Engine Start Stop Email toggle changes Email setting")
	public void remoteEngineStartStopEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("REMOTE ENGINE START/STOP", "email");
	}

	@Test(description = "Remote- Verify Remote Door Lock Unlock Email toggle changes Email setting")
	public void remoteDoorLockUnlockEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("REMOTE DOOR LOCK/UNLOCK", "email");
	}

	@Test(description = "Remote- Verify Valet Alert Email toggle changes Email setting")
	public void valetAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("VALET ALERT", "email");
	}

	@Test(description = "Remote- Verify Geo Fence Alert Email toggle changes Email setting")
	public void geoFenceAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("GEO-FENCE ALERT", "email");
	}

	@Test(description = "Remote- Verify Speed Alert Email toggle changes Email setting")
	public void speedAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("SPEED ALERT", "email");
	}

	@Test(description = "Remote- Verify Curfew Alert Email toggle changes Email setting")
	public void curfewAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("CURFEW ALERT", "email");
	}

	@Test(description = "Remote- Verify Head Unit Software Update Email toggle changes Email setting")
	public void headUnitSoftwareUpdateEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("HEAD UNIT SOFTWARE UPDATE", "email");
	}

	@Test(description = "Remote- Verify Engine Idle Alert Email toggle changes Email setting")
	public void engineIdleAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("ENGINE IDLE ALERT", "email");
	}

	@Test(description = "Remote- Verify panic Notification Phone toggle changes Phone setting")
	public void panicNotificationPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("PANIC NOTIFICATION", "phone");
	}

	@Test(description = "Remote- Verify Alarm Notification Phone toggle changes Phone setting")
	public void alarmNotificationPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("ALARM NOTIFICATION", "phone");
	}

	@Test(description = "Remote- Verify Horn And Lights Phone toggle changes Phone setting")
	public void hornAndLightsPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("HORN AND LIGHTS/LIGHTS ONLY", "phone");
	}

	@Test(description = "Remote- Verify Remote Engine Start Stop Phone toggle changes Phone setting")
	public void remoteEngineStartStopPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("REMOTE ENGINE START/STOP", "phone");
	}

	@Test(description = "Remote- Verify Remote Door Lock Unlock Phone toggle changes Phone setting")
	public void remoteDoorLockUnlockPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("REMOTE DOOR LOCK/UNLOCK", "phone");
	}

	@Test(description = "Remote- Verify Valet Alert Phone toggle changes Phone setting")
	public void valetAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("VALET ALERT", "phone");
	}

	@Test(description = "Remote- Verify Geo Fence Alert Phone toggle changes Phone setting")
	public void geoFenceAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("GEO-FENCE ALERT", "phone");
	}

	@Test(description = "Remote- Verify Speed Alert Phone toggle changes Phone setting")
	public void speedAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("SPEED ALERT", "phone");
	}

	@Test(description = "Remote- Verify Curfew Alert Phone toggle changes Phone setting")
	public void curfewAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("CURFEW ALERT", "phone");
	}

	@Test(description = "Remote- Verify Head Unit Software Update Phone toggle changes Phone setting")
	public void headUnitSoftwareUpdatePhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("HEAD UNIT SOFTWARE UPDATE", "phone");
	}

	@Test(description = "Remote- Verify Engine Idle Alert Phone toggle changes Phone setting")
	public void engineIdleAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();

		settingsPage.clickOnExpandButton("Remote");
		settingsPage.clickOnToggleButtonForNotification("ENGINE IDLE ALERT", "phone");
	}

}
