package com.spa.hyundai.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.DriverSettingPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.UserProfilePage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ProfileTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static UserProfilePage userProfilePage;
	private static DriverSettingPage driverSettingPage;
	private static LoginPage loginPage;
	private static DashboardPage dashboardPage;

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		userProfilePage = new UserProfilePage(empAppDevice);
		driverSettingPage = new DriverSettingPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);

	}

	@Test(description = "Test User Profile")
	public void testUserProfile() {
		String strUserName = "hata412@mailinator.com";
		String strPassword = "test1234";
		String strVIN = "KMHE54L28JA067125";

		userProfile(strUserName, strPassword, strVIN); 
		userProfilePage.navigateToInviteAdditionalDriver();

		userProfilePage.clickSelectFromContactsLink();

		Assert.assertTrue(userProfilePage.isMyHyundaiAlertDisplayed(), "MyHyundai Alert not displaying");

		dashboardPage.clickOnPopupOKButton();

	}

	private void userProfile(String strUserName, String strPassword, String strVIN) {
		loginPage.validLogin(strUserName, strPassword, strVIN);
		consoleLog("Log in with : " + strUserName + "/" + strPassword + "/" + strVIN);
		dashboardPage.openMenu();

		// Verify Menu option and User profile section
		Assert.assertTrue(dashboardPage.isMenuPanelDisplayed(), "Menu Panel is displaying");
		Assert.assertTrue(dashboardPage.isUserProfileMenuSectionDisplayed(),
				"User profile VIN section is not displaying at menu panel.");

		dashboardPage.navigateToUserProfile();
		Assert.assertTrue(userProfilePage.isUserProfilePageDisplayed(), "User profile page is not displaying");
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
