package com.spa.hyundai.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.ClimateSettingsPage;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.EnterPinPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.MapPage;
import com.spa.hyundai.pageObjects.MenuPage;
import com.spa.hyundai.pageObjects.RemoteActionsPage;
import com.spa.hyundai.pageObjects.SwitchVehiclePage;
import com.spa.hyundai.pageObjects.VehicleStatusPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class DashboardTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static ClimateSettingsPage climateSettingsPage;
	private static EnterPinPage enterPinPage;
	private static LoginPage loginPage;
	private static DashboardPage dashboardPage;
	private static VehicleStatusPage vehicleStatusPage;
	private static MapPage mapPage;
	private static RemoteActionsPage remoteActionsPage;
	private static MenuPage menuPage;
	private static SwitchVehiclePage switchVehiclePage;
	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		climateSettingsPage = new ClimateSettingsPage(empAppDevice);
		enterPinPage = new EnterPinPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);
		vehicleStatusPage = new VehicleStatusPage(empAppDevice);
		mapPage = new MapPage(empAppDevice);
		remoteActionsPage = new RemoteActionsPage(empAppDevice);
		menuPage = new MenuPage(empAppDevice);
		switchVehiclePage = new SwitchVehiclePage(empAppDevice);
	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "DashBoard");
	}

	/*****************************************************************************************
	 * @Description : Set Up Charge Management Schedule and go back without
	 *              editing/saving schedule
	 * @Steps : Login and go to the Charge Management screen. Go back to the
	 *        dashboard without saving any schedule.
	 *
	 * @author : Rashmi Sharma
	 * @throws IOException
	 *
	 * @Date : 09-July-2019
	 *
	 ******************************************************************************************/

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testElementsPresentOnDashboard(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strVehicleName) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();
		String strWelcomeMessage = dashboardPage.getWelcomeText();
		System.out.println("Alert Save Message:" + strWelcomeMessage);

		Assert.assertTrue(strWelcomeMessage.contains("Good afternoon") || strWelcomeMessage.contains("Good morning")
				|| strWelcomeMessage.contains("Good evening"), "Welcome messaage not displayed");

		Assert.assertTrue(dashboardPage.isWeatherIconDisplayed(), "Weather icon not displayed");
		Assert.assertTrue(dashboardPage.isVehicleNameDisplayed(strVehicleName), "Vehicle name not displayed");
	}

	@Test(description = "Remote Start Stop Climate with Front Defrost and Heated Features On Test", dataProvider = "TestLoginData")
	public void testDashboardremoteClimateStartStopWithTemperatureOnFrontDefrostOnHeatedAccessoriesOn(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strVehicleName)
			throws IOException {


		String REMOTE_COMMAND = "Remote Start";
		String REMOTE_COMMAND2 = "Remote Control Stop";
		String TEMERATURE_VALUE = "70";
		String FRONT_DEFROST_VALUE = "1";
		String HEATED_ACCESSORIES_VALUE = "1";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();

		// First Perform Remote Door Lock Action
		String REMOTE_COMMANDLock = "Remote Door Lock";
		dashboardPage.clickRemoteActionsOption();
		remoteActionsPage.clickOnRemoteLockButton();
		enterPinPage.enterPin(strPin.replace(".0", ""));
		dashboardPage.clickOnPopupOKButton();

		String REMOTE_COMMAND_MESSAGE_Stop = dashboardPage.getRemoteSuccessMessageText(REMOTE_COMMANDLock);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE_Stop);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE_Stop.contains("was successful")||REMOTE_COMMAND_MESSAGE_Stop.contains("was not processed"),
				"The Remote Door Lock command was not successful");

		empAppDevice.sleepFor(10000);
		System.out.println("Test1");
		dashboardPage.clickOnPopupOKButton();
		System.out.println("Test2");
		dashboardPage.clickRemoteActionsOption();
		empAppDevice.sleepFor(10000);
		remoteActionsPage.clickOnRemoteStartButton();
		System.out.println("Test3");
		empAppDevice.sleepFor(10000);
		
		climateSettingsPage.isRemoteStartTextDisplay();

		climateSettingsPage.clickStartVehicleWithoutPresets();
		climateSettingsPage.isViewPreConditionLabelDisplay();
		// climateSettingsPage.setTemperatureScrollValue(TEMERATURE_VALUE);
		climateSettingsPage.setStartSettingsStates(FRONT_DEFROST_VALUE, HEATED_ACCESSORIES_VALUE);

		enterPinPage.enterPin(strPin.replace(".0", ""));
		dashboardPage.clickOnPopupOKButton();

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getRemoteSuccessMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful")||REMOTE_COMMAND_MESSAGE.contains("was not processed"),
				"The Remote Start command was not successful");

		climateSettingsPage.clickOnRemoteCommandOKButton();

		dashboardPage.clickRemoteActionsOption();

		remoteActionsPage.clickOnRemoteStopButton();
		//enterPinPage.enterPin(strPin.replace(".0", ""));
		dashboardPage.clickOnPopupOKButton();

		String REMOTE_COMMAND_MESSAGE2 = dashboardPage.getRemoteSuccessMessageText(REMOTE_COMMAND2);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE2);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE2.contains("was successful")||REMOTE_COMMAND_MESSAGE.contains("Remote Stop for your vehicle cannot be processed."),
				"The Remote Stop command was not successful");

	}

	/*****************************************************************************************
	 * @Description :Search POI by passing zipCode as Search Parameter
	 * @Steps : Login and go to the POI Search Page. Enter the zipCode in the search
	 *        bar and get the POI results.
	 *
	 * @author : Rashmi Sharma
	 * @throws IOException
	 *
	 * @Date : 11-July-2019
	 *
	 ******************************************************************************************/
	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testSearchDashboardPOI(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strVehicleName) throws IOException

	{

		String str_SearchText = "20001";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();

		dashboardPage.enterSearchText(str_SearchText);
		System.out.println(mapPage.getMapLocationAddress());

		Assert.assertTrue(mapPage.getMapLocationAddress().contains(str_SearchText), "Address Not Matched");
	}

	@Test(description = "Remote Charge Start Test", dataProvider = "TestLoginData")
	public void remoteChargeStartSuccess(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String VehicleName) throws IOException {
		
		String REMOTE_COMMAND = "START REMOTE ELECTRIC CHARGE";
		
		System.out.println(strVehicleType);
		if (!strVehicleType.equalsIgnoreCase("HEV")) {

			loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();

		Assert.assertTrue(
				dashboardPage.getPluggedInStatus().equals("Plugged In")
						&& dashboardPage.getChargingStatus().equals("Not Charging"),
				"Vehicle is not plugged in or charging is full");

		dashboardPage.clickOnStartChargeButton();

		enterPinPage.enterPin(strPin.replace(".0", ""));
		dashboardPage.clickOnPopupOKButton();

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getRemoteSuccessMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Start Charge command was not successful");
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Remote Vehicle Status Test", dataProvider = "TestLoginData")
	public void remoteVehicleStatus(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strVehicleName) throws IOException {


		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.isVehicleStatusButtonDisplaye();

		dashboardPage.clickOnVehicleStatusButton();
		vehicleStatusPage.isPresent();

		String LAST_UPDATED_TEXT = vehicleStatusPage.getLastUpdatedText();

		System.out.println("Last Updated Date and Time:" + LAST_UPDATED_TEXT);

		Assert.assertTrue(vehicleStatusPage.isVehicleStatusDisplayed(), "Last Updated Vehicle Status is not displayed");

	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testMessageCenterPage(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String VehicleName) throws IOException {


		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickHomeNotificationButton();
		Assert.assertTrue(dashboardPage.labelMessageCenterDisplayed(), "Message Center label not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledPOIOnDashboard(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strVehicleName) throws IOException

	{

		String str_SearchText = "20001";
		
		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();

		dashboardPage.enterSearchText(str_SearchText);

		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledPOIOnDashboardBySwitchigVehicles(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strVehicleName) throws IOException

	{

		String str_SearchText = "20001";

		loginPage.validLoginSelectDenyPopup("AE222@hmausa.com","test1234","KMHC05LC1LU142222");
		dashboardPage.isVehicleStatusButtonDisplaye();

		dashboardPage.enterSearchText(str_SearchText);

		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");

		empAppDevice.sleepFor(10000);

		mapPage.clickCancelAlertButton();

		mapPage.clickOnBackButton();
		dashboardPage.openMenu();

		menuPage.navigateToSwitchVehicle();

		switchVehiclePage.selectVehicleByVIN("KMHC75LJ9LU049524");

		dashboardPage.isVehicleStatusButtonDisplaye();

		dashboardPage.enterSearchText(str_SearchText);

		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");

	}

	@Test(description = "Remote Charge Start Test", dataProvider = "TestLoginData")
	public void testRefreshIconUpdatesDashboard(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strVehicleName) throws IOException {
		
		System.out.println(strVehicleType);
		if (!strVehicleType.equalsIgnoreCase("HEV")) {


		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();

		String strLastUpdate = dashboardPage.getLatUpdateValue();
		System.out.println(strLastUpdate);

		empAppDevice.sleepFor(60000);
		dashboardPage.clickRefreshIcon();

		System.out.println(dashboardPage.getLatUpdateValue());
		Assert.assertTrue(!strLastUpdate.equalsIgnoreCase(dashboardPage.getLatUpdateValue()),
				"The Last Update status not changed after selecting the refresh button");
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}

	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testGen2HomePage(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strVehicleName) throws IOException {
		if (!strVehicleType.equalsIgnoreCase("HEV")) {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickHomeNotificationButton();
		Assert.assertTrue(dashboardPage.isbtnRefreshVehicleStatusDisplay(), "Refresh button displayed");
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
